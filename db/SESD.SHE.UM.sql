USE [master]
GO
/****** Object:  Database [SESD.SHE.UM]    Script Date: 04-Mar-19 1:38:54 PM ******/
CREATE DATABASE [SESD.SHE.UM]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'SESD.SHE.UMG', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\SESD.SHE.UMG.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'SESD.SHE.UMG_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\SESD.SHE.UMG_log.ldf' , SIZE = 2048KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [SESD.SHE.UM] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [SESD.SHE.UM].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [SESD.SHE.UM] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [SESD.SHE.UM] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [SESD.SHE.UM] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [SESD.SHE.UM] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [SESD.SHE.UM] SET ARITHABORT OFF 
GO
ALTER DATABASE [SESD.SHE.UM] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [SESD.SHE.UM] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [SESD.SHE.UM] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [SESD.SHE.UM] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [SESD.SHE.UM] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [SESD.SHE.UM] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [SESD.SHE.UM] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [SESD.SHE.UM] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [SESD.SHE.UM] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [SESD.SHE.UM] SET  DISABLE_BROKER 
GO
ALTER DATABASE [SESD.SHE.UM] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [SESD.SHE.UM] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [SESD.SHE.UM] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [SESD.SHE.UM] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [SESD.SHE.UM] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [SESD.SHE.UM] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [SESD.SHE.UM] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [SESD.SHE.UM] SET RECOVERY FULL 
GO
ALTER DATABASE [SESD.SHE.UM] SET  MULTI_USER 
GO
ALTER DATABASE [SESD.SHE.UM] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [SESD.SHE.UM] SET DB_CHAINING OFF 
GO
ALTER DATABASE [SESD.SHE.UM] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [SESD.SHE.UM] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [SESD.SHE.UM] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'SESD.SHE.UM', N'ON'
GO
USE [SESD.SHE.UM]
GO
/****** Object:  Table [dbo].[Actions]    Script Date: 04-Mar-19 1:38:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Actions](
	[ActionId] [uniqueidentifier] NOT NULL,
	[ActionName] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_Actions] PRIMARY KEY CLUSTERED 
(
	[ActionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ActionsInModules]    Script Date: 04-Mar-19 1:38:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ActionsInModules](
	[ActionId] [uniqueidentifier] NOT NULL,
	[ModuleId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_ActionsInModules] PRIMARY KEY CLUSTERED 
(
	[ActionId] ASC,
	[ModuleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ActionsInModulesChosen]    Script Date: 04-Mar-19 1:38:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ActionsInModulesChosen](
	[ModuleInRoleId] [uniqueidentifier] NOT NULL,
	[ActionId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_ActionsInModulesChosen] PRIMARY KEY CLUSTERED 
(
	[ModuleInRoleId] ASC,
	[ActionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Applications]    Script Date: 04-Mar-19 1:38:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Applications](
	[ApplicationName] [nvarchar](235) NOT NULL,
	[ApplicationId] [uniqueidentifier] NOT NULL,
	[Description] [nvarchar](256) NULL,
PRIMARY KEY CLUSTERED 
(
	[ApplicationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Memberships]    Script Date: 04-Mar-19 1:38:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Memberships](
	[ApplicationId] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[Password] [nvarchar](128) NOT NULL,
	[PasswordFormat] [int] NOT NULL,
	[PasswordSalt] [nvarchar](128) NOT NULL,
	[Email] [nvarchar](256) NULL,
	[PasswordQuestion] [nvarchar](256) NULL,
	[PasswordAnswer] [nvarchar](128) NULL,
	[IsApproved] [bit] NOT NULL,
	[IsLockedOut] [bit] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[LastLoginDate] [datetime] NOT NULL,
	[LastPasswordChangedDate] [datetime] NOT NULL,
	[LastLockoutDate] [datetime] NOT NULL,
	[FailedPasswordAttemptCount] [int] NOT NULL,
	[FailedPasswordAttemptWindowStart] [datetime] NOT NULL,
	[FailedPasswordAnswerAttemptCount] [int] NOT NULL,
	[FailedPasswordAnswerAttemptWindowsStart] [datetime] NOT NULL,
	[Comment] [nvarchar](256) NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Modules]    Script Date: 04-Mar-19 1:38:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Modules](
	[ModuleId] [uniqueidentifier] NOT NULL,
	[ModuleName] [nvarchar](256) NOT NULL,
	[ParentModuleId] [uniqueidentifier] NULL,
 CONSTRAINT [PK_Modules] PRIMARY KEY CLUSTERED 
(
	[ModuleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ModulesInRoles]    Script Date: 04-Mar-19 1:38:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ModulesInRoles](
	[Id] [uniqueidentifier] NOT NULL,
	[RoleId] [uniqueidentifier] NOT NULL,
	[ModuleId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_ModulesInRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Profiles]    Script Date: 04-Mar-19 1:38:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Profiles](
	[UserId] [uniqueidentifier] NOT NULL,
	[PropertyNames] [nvarchar](4000) NOT NULL,
	[PropertyValueStrings] [nvarchar](4000) NOT NULL,
	[PropertyValueBinary] [image] NOT NULL,
	[LastUpdatedDate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Roles]    Script Date: 04-Mar-19 1:38:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Roles](
	[ApplicationId] [uniqueidentifier] NOT NULL,
	[RoleId] [uniqueidentifier] NOT NULL,
	[RoleName] [nvarchar](256) NOT NULL,
	[Description] [nvarchar](256) NULL,
PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Users]    Script Date: 04-Mar-19 1:38:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[ApplicationId] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[IsAnonymous] [bit] NOT NULL,
	[LastActivityDate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UsersInRoles]    Script Date: 04-Mar-19 1:38:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UsersInRoles](
	[UserId] [uniqueidentifier] NOT NULL,
	[RoleId] [uniqueidentifier] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
INSERT [dbo].[Applications] ([ApplicationName], [ApplicationId], [Description]) VALUES (N'ProjectTemplate', N'495d2e92-85fd-4b5b-b21e-5d1908011b68', NULL)
INSERT [dbo].[Memberships] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [Email], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowsStart], [Comment]) VALUES (N'495d2e92-85fd-4b5b-b21e-5d1908011b68', N'0ff6ac18-b7cb-4d41-8c42-5ef5283b1f2c', N'c0qOZUJwa+4hVUKXmq5EskpbLHiiz9B2qbTuqBcKLZY=', 1, N'Mqf3mNv30kuMs5e+PstFXg==', N'superadmin@mail.com', NULL, NULL, 1, 0, CAST(N'2017-10-16 06:10:32.147' AS DateTime), CAST(N'2019-03-01 03:45:14.030' AS DateTime), CAST(N'2017-10-16 06:10:32.147' AS DateTime), CAST(N'1754-01-01 00:00:00.000' AS DateTime), 0, CAST(N'1754-01-01 00:00:00.000' AS DateTime), 0, CAST(N'1754-01-01 00:00:00.000' AS DateTime), NULL)
INSERT [dbo].[Memberships] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [Email], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowsStart], [Comment]) VALUES (N'495d2e92-85fd-4b5b-b21e-5d1908011b68', N'97ff22da-fd01-4e2c-9658-74f921272246', N'at7J41WCe/35v0Iu1eYlrXp8wG7hhYFaXZU9HAyWnYc=', 1, N'KMUZKVeXWVulrh31ab461g==', N'admin@mail.com', NULL, NULL, 1, 0, CAST(N'2018-04-11 15:55:43.557' AS DateTime), CAST(N'2018-04-11 18:12:29.530' AS DateTime), CAST(N'2018-04-11 15:55:43.557' AS DateTime), CAST(N'1754-01-01 00:00:00.000' AS DateTime), 0, CAST(N'1754-01-01 00:00:00.000' AS DateTime), 0, CAST(N'1754-01-01 00:00:00.000' AS DateTime), NULL)
INSERT [dbo].[Modules] ([ModuleId], [ModuleName], [ParentModuleId]) VALUES (N'40e58de3-f3f1-46f8-bf64-2f3e60497f70', N'Crew', NULL)
INSERT [dbo].[Modules] ([ModuleId], [ModuleName], [ParentModuleId]) VALUES (N'9de4e116-012a-4c42-8657-5ef2c7886c6d', N'Whitelist', NULL)
INSERT [dbo].[Modules] ([ModuleId], [ModuleName], [ParentModuleId]) VALUES (N'3cf8b68b-0ded-4206-8e31-711423f1ebb3', N'Company', N'2cdf85c5-2b85-4c01-8bca-bb41b41a40b9')
INSERT [dbo].[Modules] ([ModuleId], [ModuleName], [ParentModuleId]) VALUES (N'3a17863e-31b4-4b69-abe5-ba2a264b07be', N'User Management', NULL)
INSERT [dbo].[Modules] ([ModuleId], [ModuleName], [ParentModuleId]) VALUES (N'2cdf85c5-2b85-4c01-8bca-bb41b41a40b9', N'Data Master', NULL)
INSERT [dbo].[ModulesInRoles] ([Id], [RoleId], [ModuleId]) VALUES (N'b2ee7ec2-51af-47aa-a1f3-3339d8715745', N'f9dfbc89-6423-4c7c-9390-36d240a3f875', N'40e58de3-f3f1-46f8-bf64-2f3e60497f70')
INSERT [dbo].[ModulesInRoles] ([Id], [RoleId], [ModuleId]) VALUES (N'11cd99a5-d94d-4af9-aa2f-368ed8fddd6a', N'f9dfbc89-6423-4c7c-9390-36d240a3f875', N'9de4e116-012a-4c42-8657-5ef2c7886c6d')
INSERT [dbo].[ModulesInRoles] ([Id], [RoleId], [ModuleId]) VALUES (N'6eba1ede-4c87-4d9d-ac67-a5ca8e699138', N'f9dfbc89-6423-4c7c-9390-36d240a3f875', N'3a17863e-31b4-4b69-abe5-ba2a264b07be')
INSERT [dbo].[Roles] ([ApplicationId], [RoleId], [RoleName], [Description]) VALUES (N'495d2e92-85fd-4b5b-b21e-5d1908011b68', N'f9dfbc89-6423-4c7c-9390-36d240a3f875', N'Administrator', NULL)
INSERT [dbo].[Users] ([ApplicationId], [UserId], [UserName], [IsAnonymous], [LastActivityDate]) VALUES (N'495d2e92-85fd-4b5b-b21e-5d1908011b68', N'0ff6ac18-b7cb-4d41-8c42-5ef5283b1f2c', N'superadmin', 0, CAST(N'2019-03-01 03:45:14.030' AS DateTime))
INSERT [dbo].[Users] ([ApplicationId], [UserId], [UserName], [IsAnonymous], [LastActivityDate]) VALUES (N'495d2e92-85fd-4b5b-b21e-5d1908011b68', N'97ff22da-fd01-4e2c-9658-74f921272246', N'admin', 0, CAST(N'2018-04-11 18:12:29.530' AS DateTime))
INSERT [dbo].[UsersInRoles] ([UserId], [RoleId]) VALUES (N'0ff6ac18-b7cb-4d41-8c42-5ef5283b1f2c', N'f9dfbc89-6423-4c7c-9390-36d240a3f875')
SET ANSI_PADDING ON

GO
/****** Object:  Index [IDX_UserName]    Script Date: 04-Mar-19 1:38:55 PM ******/
CREATE NONCLUSTERED INDEX [IDX_UserName] ON [dbo].[Users]
(
	[UserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ActionsInModules]  WITH CHECK ADD  CONSTRAINT [ActionsInModulesActions] FOREIGN KEY([ActionId])
REFERENCES [dbo].[Actions] ([ActionId])
GO
ALTER TABLE [dbo].[ActionsInModules] CHECK CONSTRAINT [ActionsInModulesActions]
GO
ALTER TABLE [dbo].[ActionsInModules]  WITH CHECK ADD  CONSTRAINT [ActionsInModulesModules] FOREIGN KEY([ModuleId])
REFERENCES [dbo].[Modules] ([ModuleId])
GO
ALTER TABLE [dbo].[ActionsInModules] CHECK CONSTRAINT [ActionsInModulesModules]
GO
ALTER TABLE [dbo].[ActionsInModulesChosen]  WITH CHECK ADD  CONSTRAINT [ActionsInModulesChosenActions] FOREIGN KEY([ActionId])
REFERENCES [dbo].[Actions] ([ActionId])
GO
ALTER TABLE [dbo].[ActionsInModulesChosen] CHECK CONSTRAINT [ActionsInModulesChosenActions]
GO
ALTER TABLE [dbo].[ActionsInModulesChosen]  WITH CHECK ADD  CONSTRAINT [ActionsInModulesChosenModulesInRoles] FOREIGN KEY([ModuleInRoleId])
REFERENCES [dbo].[ModulesInRoles] ([Id])
GO
ALTER TABLE [dbo].[ActionsInModulesChosen] CHECK CONSTRAINT [ActionsInModulesChosenModulesInRoles]
GO
ALTER TABLE [dbo].[Memberships]  WITH CHECK ADD  CONSTRAINT [MembershipApplication] FOREIGN KEY([ApplicationId])
REFERENCES [dbo].[Applications] ([ApplicationId])
GO
ALTER TABLE [dbo].[Memberships] CHECK CONSTRAINT [MembershipApplication]
GO
ALTER TABLE [dbo].[Memberships]  WITH CHECK ADD  CONSTRAINT [MembershipUser] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[Memberships] CHECK CONSTRAINT [MembershipUser]
GO
ALTER TABLE [dbo].[Modules]  WITH CHECK ADD  CONSTRAINT [FK_Modules_Modules1] FOREIGN KEY([ParentModuleId])
REFERENCES [dbo].[Modules] ([ModuleId])
GO
ALTER TABLE [dbo].[Modules] CHECK CONSTRAINT [FK_Modules_Modules1]
GO
ALTER TABLE [dbo].[ModulesInRoles]  WITH CHECK ADD  CONSTRAINT [ModulesInRolesModules] FOREIGN KEY([ModuleId])
REFERENCES [dbo].[Modules] ([ModuleId])
GO
ALTER TABLE [dbo].[ModulesInRoles] CHECK CONSTRAINT [ModulesInRolesModules]
GO
ALTER TABLE [dbo].[ModulesInRoles]  WITH CHECK ADD  CONSTRAINT [ModulesInRolesRoles] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Roles] ([RoleId])
GO
ALTER TABLE [dbo].[ModulesInRoles] CHECK CONSTRAINT [ModulesInRolesRoles]
GO
ALTER TABLE [dbo].[Profiles]  WITH CHECK ADD  CONSTRAINT [UserProfile] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[Profiles] CHECK CONSTRAINT [UserProfile]
GO
ALTER TABLE [dbo].[Roles]  WITH CHECK ADD  CONSTRAINT [RoleApplication] FOREIGN KEY([ApplicationId])
REFERENCES [dbo].[Applications] ([ApplicationId])
GO
ALTER TABLE [dbo].[Roles] CHECK CONSTRAINT [RoleApplication]
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [UserApplication] FOREIGN KEY([ApplicationId])
REFERENCES [dbo].[Applications] ([ApplicationId])
GO
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [UserApplication]
GO
ALTER TABLE [dbo].[UsersInRoles]  WITH CHECK ADD  CONSTRAINT [UsersInRoleRole] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Roles] ([RoleId])
GO
ALTER TABLE [dbo].[UsersInRoles] CHECK CONSTRAINT [UsersInRoleRole]
GO
ALTER TABLE [dbo].[UsersInRoles]  WITH CHECK ADD  CONSTRAINT [UsersInRoleUser] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[UsersInRoles] CHECK CONSTRAINT [UsersInRoleUser]
GO
USE [master]
GO
ALTER DATABASE [SESD.SHE.UM] SET  READ_WRITE 
GO
