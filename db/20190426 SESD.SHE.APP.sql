USE [master]
GO
/****** Object:  Database [SESD.SHE.APP]    Script Date: 26-Apr-19 3:54:18 PM ******/
CREATE DATABASE [SESD.SHE.APP]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'SESD.SHE.APP', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\SESD.SHE.APP.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'SESD.SHE.APP_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\SESD.SHE.APP_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [SESD.SHE.APP] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [SESD.SHE.APP].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [SESD.SHE.APP] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [SESD.SHE.APP] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [SESD.SHE.APP] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [SESD.SHE.APP] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [SESD.SHE.APP] SET ARITHABORT OFF 
GO
ALTER DATABASE [SESD.SHE.APP] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [SESD.SHE.APP] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [SESD.SHE.APP] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [SESD.SHE.APP] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [SESD.SHE.APP] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [SESD.SHE.APP] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [SESD.SHE.APP] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [SESD.SHE.APP] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [SESD.SHE.APP] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [SESD.SHE.APP] SET  DISABLE_BROKER 
GO
ALTER DATABASE [SESD.SHE.APP] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [SESD.SHE.APP] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [SESD.SHE.APP] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [SESD.SHE.APP] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [SESD.SHE.APP] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [SESD.SHE.APP] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [SESD.SHE.APP] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [SESD.SHE.APP] SET RECOVERY FULL 
GO
ALTER DATABASE [SESD.SHE.APP] SET  MULTI_USER 
GO
ALTER DATABASE [SESD.SHE.APP] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [SESD.SHE.APP] SET DB_CHAINING OFF 
GO
ALTER DATABASE [SESD.SHE.APP] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [SESD.SHE.APP] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [SESD.SHE.APP] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'SESD.SHE.APP', N'ON'
GO
USE [SESD.SHE.APP]
GO
/****** Object:  Table [dbo].[MstCompany]    Script Date: 26-Apr-19 3:54:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MstCompany](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MstCompanyTypeId] [int] NOT NULL,
	[Name] [varchar](128) NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTimeUtc] [datetime] NOT NULL,
	[ModifiedBy] [varchar](50) NOT NULL,
	[ModifiedDateTimeUtc] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_MstVehicleOwner] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MstCompanyType]    Script Date: 26-Apr-19 3:54:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MstCompanyType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](128) NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTimeUtc] [datetime] NOT NULL,
	[ModifiedBy] [varchar](50) NOT NULL,
	[ModifiedDateTimeUtc] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_MstOwnershipType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MstDriver]    Script Date: 26-Apr-19 3:54:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MstDriver](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [varchar](50) NOT NULL,
	[IsProfessional] [bit] NULL,
	[DrivingStartDate] [datetime] NULL,
	[CompanyDrivingStartDate] [datetime] NULL,
	[MstCqMvcThirdPartyTypeId] [int] NULL,
	[IsOwnValidLicense] [bit] NULL,
	[IsAttendedDriverTraining] [bit] NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTimeUtc] [datetime] NOT NULL,
	[ModifiedBy] [varchar](50) NOT NULL,
	[ModifiedDateTimeUtc] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_MstDriver] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MstOrganization]    Script Date: 26-Apr-19 3:54:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MstOrganization](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ParentId] [int] NULL,
	[Name] [varchar](128) NOT NULL,
	[MstRegionId] [int] NULL,
	[MstSiteId] [int] NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTimeUtc] [datetime] NOT NULL,
	[ModifiedBy] [varchar](50) NOT NULL,
	[ModifiedDateTimeUtc] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_MstOrganization] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MstRegion]    Script Date: 26-Apr-19 3:54:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MstRegion](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](128) NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTimeUtc] [datetime] NOT NULL,
	[ModifiedBy] [varchar](50) NOT NULL,
	[ModifiedDateTimeUtc] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_MstRegion] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MstSite]    Script Date: 26-Apr-19 3:54:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MstSite](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MstRegionId] [int] NOT NULL,
	[Name] [varchar](128) NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTimeUtc] [datetime] NOT NULL,
	[ModifiedBy] [varchar](50) NOT NULL,
	[ModifiedDateTimeUtc] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_MstSite] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MstVehicle]    Script Date: 26-Apr-19 3:54:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MstVehicle](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MstVehicleTypeId] [int] NOT NULL,
	[LicensePlateHeader] [varchar](4) NULL,
	[LicensePlateNumber] [varchar](8) NULL,
	[LicensePlateTail] [varchar](4) NULL,
	[MstVehicleOwnershipId] [int] NULL,
	[OwnerMstCompanyId] [int] NULL,
	[MstVehicleBrandId] [int] NULL,
	[VehicleYear] [int] NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTimeUtc] [datetime] NOT NULL,
	[ModifiedBy] [varchar](50) NOT NULL,
	[ModifiedDateTimeUtc] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_MstVehicle] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MstVehicleBrand]    Script Date: 26-Apr-19 3:54:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MstVehicleBrand](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](128) NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTimeUtc] [datetime] NOT NULL,
	[ModifiedBy] [varchar](50) NOT NULL,
	[ModifiedDateTimeUtc] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_MstVehicleBrand] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MstVehicleOwnership]    Script Date: 26-Apr-19 3:54:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MstVehicleOwnership](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MstCompanyTypeId] [int] NOT NULL,
	[Name] [varchar](128) NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTimeUtc] [datetime] NOT NULL,
	[ModifiedBy] [varchar](50) NOT NULL,
	[ModifiedDateTimeUtc] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_MstVehicleOwnership] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MstVehicleType]    Script Date: 26-Apr-19 3:54:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MstVehicleType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](32) NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTimeUtc] [datetime] NOT NULL,
	[ModifiedBy] [varchar](50) NOT NULL,
	[ModifiedDateTimeUtc] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_MstVehicleType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[MstCompany] ON 

INSERT [dbo].[MstCompany] ([Id], [MstCompanyTypeId], [Name], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (2, 2, N'Star Energy', N'superadmin', CAST(N'2019-04-02T00:00:00.000' AS DateTime), N'superadmin', CAST(N'2019-04-02T00:00:00.000' AS DateTime), 0)
INSERT [dbo].[MstCompany] ([Id], [MstCompanyTypeId], [Name], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (3, 1, N'SOLIMAN SECURITY SERVICES, INC.', N'superadmin', CAST(N'2019-04-02T00:00:00.000' AS DateTime), N'superadmin', CAST(N'2019-04-02T00:00:00.000' AS DateTime), 0)
SET IDENTITY_INSERT [dbo].[MstCompany] OFF
SET IDENTITY_INSERT [dbo].[MstCompanyType] ON 

INSERT [dbo].[MstCompanyType] ([Id], [Name], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (1, N'Contractor', N'superadmin', CAST(N'2019-04-02T00:00:00.000' AS DateTime), N'superadmin', CAST(N'2019-04-02T00:00:00.000' AS DateTime), 0)
INSERT [dbo].[MstCompanyType] ([Id], [Name], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (2, N'Company', N'superadmin', CAST(N'2019-04-02T00:00:00.000' AS DateTime), N'superadmin', CAST(N'2019-04-02T00:00:00.000' AS DateTime), 0)
SET IDENTITY_INSERT [dbo].[MstCompanyType] OFF
SET IDENTITY_INSERT [dbo].[MstDriver] ON 

INSERT [dbo].[MstDriver] ([Id], [UserName], [IsProfessional], [DrivingStartDate], [CompanyDrivingStartDate], [MstCqMvcThirdPartyTypeId], [IsOwnValidLicense], [IsAttendedDriverTraining], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (1, N'admin', 1, CAST(N'2011-04-01T00:00:00.000' AS DateTime), CAST(N'2017-04-01T00:00:00.000' AS DateTime), NULL, 1, 0, N'superadmin', CAST(N'2019-04-08T07:44:52.013' AS DateTime), N'superadmin', CAST(N'2019-04-08T07:44:52.013' AS DateTime), 0)
INSERT [dbo].[MstDriver] ([Id], [UserName], [IsProfessional], [DrivingStartDate], [CompanyDrivingStartDate], [MstCqMvcThirdPartyTypeId], [IsOwnValidLicense], [IsAttendedDriverTraining], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (6, N'mk.myudistiro', 1, CAST(N'2007-04-01T00:00:00.000' AS DateTime), CAST(N'2016-04-01T00:00:00.000' AS DateTime), NULL, 1, 0, N'superadmin', CAST(N'2019-04-08T07:40:20.737' AS DateTime), N'superadmin', CAST(N'2019-04-08T07:40:20.737' AS DateTime), 0)
SET IDENTITY_INSERT [dbo].[MstDriver] OFF
SET IDENTITY_INSERT [dbo].[MstOrganization] ON 

INSERT [dbo].[MstOrganization] ([Id], [ParentId], [Name], [MstRegionId], [MstSiteId], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (1, NULL, N'Global', NULL, NULL, N'superadmin', CAST(N'2019-03-12T07:41:07.040' AS DateTime), N'superadmin', CAST(N'2019-03-12T07:41:07.040' AS DateTime), 0)
INSERT [dbo].[MstOrganization] ([Id], [ParentId], [Name], [MstRegionId], [MstSiteId], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (2, 1, N'Asia', NULL, NULL, N'superadmin', CAST(N'2019-03-12T07:41:12.790' AS DateTime), N'superadmin', CAST(N'2019-03-12T07:41:12.790' AS DateTime), 0)
INSERT [dbo].[MstOrganization] ([Id], [ParentId], [Name], [MstRegionId], [MstSiteId], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (3, 2, N'Indonesia', NULL, NULL, N'superadmin', CAST(N'2019-03-12T07:41:22.773' AS DateTime), N'superadmin', CAST(N'2019-03-12T07:41:22.773' AS DateTime), 0)
INSERT [dbo].[MstOrganization] ([Id], [ParentId], [Name], [MstRegionId], [MstSiteId], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (4, 3, N'Star Energy', NULL, NULL, N'superadmin', CAST(N'2019-03-12T07:41:29.503' AS DateTime), N'superadmin', CAST(N'2019-03-12T07:41:29.503' AS DateTime), 0)
INSERT [dbo].[MstOrganization] ([Id], [ParentId], [Name], [MstRegionId], [MstSiteId], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (5, 4, N'Operations', NULL, NULL, N'superadmin', CAST(N'2019-03-12T07:46:17.257' AS DateTime), N'superadmin', CAST(N'2019-03-12T07:46:17.257' AS DateTime), 0)
INSERT [dbo].[MstOrganization] ([Id], [ParentId], [Name], [MstRegionId], [MstSiteId], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (6, 5, N'Management & Staff', 1, 3, N'superadmin', CAST(N'2019-03-12T07:46:38.903' AS DateTime), N'superadmin', CAST(N'2019-03-12T07:46:38.903' AS DateTime), 0)
INSERT [dbo].[MstOrganization] ([Id], [ParentId], [Name], [MstRegionId], [MstSiteId], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (7, 5, N'Asset Darajat', 3, 1, N'superadmin', CAST(N'2019-03-12T07:48:43.240' AS DateTime), N'superadmin', CAST(N'2019-03-12T07:48:43.240' AS DateTime), 0)
INSERT [dbo].[MstOrganization] ([Id], [ParentId], [Name], [MstRegionId], [MstSiteId], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (8, 7, N'Operations & Maintenance', 3, 1, N'superadmin', CAST(N'2019-03-12T07:49:00.680' AS DateTime), N'superadmin', CAST(N'2019-03-12T07:49:00.680' AS DateTime), 0)
SET IDENTITY_INSERT [dbo].[MstOrganization] OFF
SET IDENTITY_INSERT [dbo].[MstRegion] ON 

INSERT [dbo].[MstRegion] ([Id], [Name], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (1, N'Jakarta', N'superadmin', CAST(N'2019-03-12T07:40:24.717' AS DateTime), N'superadmin', CAST(N'2019-03-12T07:40:24.717' AS DateTime), 0)
INSERT [dbo].[MstRegion] ([Id], [Name], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (2, N'Salak', N'superadmin', CAST(N'2019-03-12T07:40:28.243' AS DateTime), N'superadmin', CAST(N'2019-03-12T07:40:28.243' AS DateTime), 0)
INSERT [dbo].[MstRegion] ([Id], [Name], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (3, N'Darajat', N'superadmin', CAST(N'2019-03-12T07:40:31.187' AS DateTime), N'superadmin', CAST(N'2019-03-12T07:40:31.187' AS DateTime), 0)
SET IDENTITY_INSERT [dbo].[MstRegion] OFF
SET IDENTITY_INSERT [dbo].[MstSite] ON 

INSERT [dbo].[MstSite] ([Id], [MstRegionId], [Name], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (1, 3, N'Darajat Power Plant', N'superadmin', CAST(N'2019-03-12T07:40:44.337' AS DateTime), N'superadmin', CAST(N'2019-03-12T07:40:44.337' AS DateTime), 0)
INSERT [dbo].[MstSite] ([Id], [MstRegionId], [Name], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (2, 2, N'Salak Power Plant', N'superadmin', CAST(N'2019-03-12T07:40:51.940' AS DateTime), N'superadmin', CAST(N'2019-03-12T07:40:51.940' AS DateTime), 0)
INSERT [dbo].[MstSite] ([Id], [MstRegionId], [Name], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (3, 1, N'Sentral Senayan II', N'superadmin', CAST(N'2019-03-12T07:40:57.357' AS DateTime), N'superadmin', CAST(N'2019-03-12T07:40:57.357' AS DateTime), 0)
SET IDENTITY_INSERT [dbo].[MstSite] OFF
SET IDENTITY_INSERT [dbo].[MstVehicle] ON 

INSERT [dbo].[MstVehicle] ([Id], [MstVehicleTypeId], [LicensePlateHeader], [LicensePlateNumber], [LicensePlateTail], [MstVehicleOwnershipId], [OwnerMstCompanyId], [MstVehicleBrandId], [VehicleYear], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (2, 1, N'B', N'1234', N'ABC', 2, 2, 1, 2015, N'superadmin', CAST(N'2019-04-08T07:23:04.257' AS DateTime), N'superadmin', CAST(N'2019-04-08T07:23:04.257' AS DateTime), 0)
INSERT [dbo].[MstVehicle] ([Id], [MstVehicleTypeId], [LicensePlateHeader], [LicensePlateNumber], [LicensePlateTail], [MstVehicleOwnershipId], [OwnerMstCompanyId], [MstVehicleBrandId], [VehicleYear], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (3, 1, N'B', N'4321', N'ASD', 3, 2, 1, 2012, N'superadmin', CAST(N'2019-04-08T07:44:52.020' AS DateTime), N'superadmin', CAST(N'2019-04-08T07:44:52.020' AS DateTime), 0)
SET IDENTITY_INSERT [dbo].[MstVehicle] OFF
SET IDENTITY_INSERT [dbo].[MstVehicleBrand] ON 

INSERT [dbo].[MstVehicleBrand] ([Id], [Name], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (1, N'Samsung', N'superadmin', CAST(N'2019-04-10T00:00:00.000' AS DateTime), N'superadmin', CAST(N'2019-04-10T00:00:00.000' AS DateTime), 0)
SET IDENTITY_INSERT [dbo].[MstVehicleBrand] OFF
SET IDENTITY_INSERT [dbo].[MstVehicleOwnership] ON 

INSERT [dbo].[MstVehicleOwnership] ([Id], [MstCompanyTypeId], [Name], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (2, 1, N'Contract Owned', N'superadmin', CAST(N'2019-04-02T00:00:00.000' AS DateTime), N'superadmin', CAST(N'2019-04-02T00:00:00.000' AS DateTime), 0)
INSERT [dbo].[MstVehicleOwnership] ([Id], [MstCompanyTypeId], [Name], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (3, 2, N'Company Leased', N'superadmin', CAST(N'2019-04-02T00:00:00.000' AS DateTime), N'superadmin', CAST(N'2019-04-02T00:00:00.000' AS DateTime), 0)
SET IDENTITY_INSERT [dbo].[MstVehicleOwnership] OFF
SET IDENTITY_INSERT [dbo].[MstVehicleType] ON 

INSERT [dbo].[MstVehicleType] ([Id], [Name], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (1, N'Tractor', N'superadmin', CAST(N'2019-04-02T00:00:00.000' AS DateTime), N'superadmin', CAST(N'2019-04-02T00:00:00.000' AS DateTime), 0)
INSERT [dbo].[MstVehicleType] ([Id], [Name], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (2, N'Other', N'superadmin', CAST(N'2019-04-02T00:00:00.000' AS DateTime), N'superadmin', CAST(N'2019-04-02T00:00:00.000' AS DateTime), 0)
SET IDENTITY_INSERT [dbo].[MstVehicleType] OFF
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_MstDriver]    Script Date: 26-Apr-19 3:54:18 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_MstDriver] ON [dbo].[MstDriver]
(
	[UserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MstCompany] ADD  CONSTRAINT [DF_MstVehicleOwner_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[MstCompanyType] ADD  CONSTRAINT [DF_MstOwnershipType_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[MstDriver] ADD  CONSTRAINT [DF_MstDriver_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[MstOrganization] ADD  CONSTRAINT [DF_MstOrganization_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[MstRegion] ADD  CONSTRAINT [DF_MstRegion_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[MstSite] ADD  CONSTRAINT [DF_MstSite_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[MstVehicle] ADD  CONSTRAINT [DF_MstVehicle_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[MstVehicleBrand] ADD  CONSTRAINT [DF_MstVehicleBrand_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[MstVehicleOwnership] ADD  CONSTRAINT [DF_MstVehicleOwnership_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[MstVehicleType] ADD  CONSTRAINT [DF_MstVehicleType_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[MstCompany]  WITH CHECK ADD  CONSTRAINT [FK_MstVehicleOwner_MstVehicleOwnershipType] FOREIGN KEY([MstCompanyTypeId])
REFERENCES [dbo].[MstCompanyType] ([Id])
GO
ALTER TABLE [dbo].[MstCompany] CHECK CONSTRAINT [FK_MstVehicleOwner_MstVehicleOwnershipType]
GO
ALTER TABLE [dbo].[MstOrganization]  WITH CHECK ADD  CONSTRAINT [FK_MstOrganization_MstOrganization] FOREIGN KEY([ParentId])
REFERENCES [dbo].[MstOrganization] ([Id])
GO
ALTER TABLE [dbo].[MstOrganization] CHECK CONSTRAINT [FK_MstOrganization_MstOrganization]
GO
ALTER TABLE [dbo].[MstOrganization]  WITH CHECK ADD  CONSTRAINT [FK_MstOrganization_MstRegion] FOREIGN KEY([MstRegionId])
REFERENCES [dbo].[MstRegion] ([Id])
GO
ALTER TABLE [dbo].[MstOrganization] CHECK CONSTRAINT [FK_MstOrganization_MstRegion]
GO
ALTER TABLE [dbo].[MstOrganization]  WITH CHECK ADD  CONSTRAINT [FK_MstOrganization_MstSite] FOREIGN KEY([MstSiteId])
REFERENCES [dbo].[MstSite] ([Id])
GO
ALTER TABLE [dbo].[MstOrganization] CHECK CONSTRAINT [FK_MstOrganization_MstSite]
GO
ALTER TABLE [dbo].[MstSite]  WITH CHECK ADD  CONSTRAINT [FK_MstSite_MstRegion] FOREIGN KEY([MstRegionId])
REFERENCES [dbo].[MstRegion] ([Id])
GO
ALTER TABLE [dbo].[MstSite] CHECK CONSTRAINT [FK_MstSite_MstRegion]
GO
ALTER TABLE [dbo].[MstVehicle]  WITH CHECK ADD  CONSTRAINT [FK_MstVehicle_MstVehicleBrand] FOREIGN KEY([MstVehicleBrandId])
REFERENCES [dbo].[MstVehicleBrand] ([Id])
GO
ALTER TABLE [dbo].[MstVehicle] CHECK CONSTRAINT [FK_MstVehicle_MstVehicleBrand]
GO
ALTER TABLE [dbo].[MstVehicle]  WITH CHECK ADD  CONSTRAINT [FK_MstVehicle_MstVehicleOwner] FOREIGN KEY([OwnerMstCompanyId])
REFERENCES [dbo].[MstCompany] ([Id])
GO
ALTER TABLE [dbo].[MstVehicle] CHECK CONSTRAINT [FK_MstVehicle_MstVehicleOwner]
GO
ALTER TABLE [dbo].[MstVehicle]  WITH CHECK ADD  CONSTRAINT [FK_MstVehicle_MstVehicleOwnership] FOREIGN KEY([MstVehicleOwnershipId])
REFERENCES [dbo].[MstVehicleOwnership] ([Id])
GO
ALTER TABLE [dbo].[MstVehicle] CHECK CONSTRAINT [FK_MstVehicle_MstVehicleOwnership]
GO
ALTER TABLE [dbo].[MstVehicle]  WITH CHECK ADD  CONSTRAINT [FK_MstVehicle_MstVehicleType] FOREIGN KEY([MstVehicleTypeId])
REFERENCES [dbo].[MstVehicleType] ([Id])
GO
ALTER TABLE [dbo].[MstVehicle] CHECK CONSTRAINT [FK_MstVehicle_MstVehicleType]
GO
ALTER TABLE [dbo].[MstVehicleOwnership]  WITH CHECK ADD  CONSTRAINT [FK_MstVehicleOwnership_MstVehicleOwnershipType] FOREIGN KEY([MstCompanyTypeId])
REFERENCES [dbo].[MstCompanyType] ([Id])
GO
ALTER TABLE [dbo].[MstVehicleOwnership] CHECK CONSTRAINT [FK_MstVehicleOwnership_MstVehicleOwnershipType]
GO
USE [master]
GO
ALTER DATABASE [SESD.SHE.APP] SET  READ_WRITE 
GO
