USE [SESD.SHE.APP]
GO
/****** Object:  Table [dbo].[MstCompany]    Script Date: 23/05/2019 13:27:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MstCompany](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MstCompanyTypeId] [int] NOT NULL,
	[Name] [varchar](128) NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTimeUtc] [datetime] NOT NULL,
	[ModifiedBy] [varchar](50) NOT NULL,
	[ModifiedDateTimeUtc] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_MstVehicleOwner] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MstCompanyType]    Script Date: 23/05/2019 13:27:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MstCompanyType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](128) NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTimeUtc] [datetime] NOT NULL,
	[ModifiedBy] [varchar](50) NOT NULL,
	[ModifiedDateTimeUtc] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_MstOwnershipType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MstDriver]    Script Date: 23/05/2019 13:27:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MstDriver](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [varchar](50) NOT NULL,
	[IsProfessional] [bit] NULL,
	[DrivingStartDate] [datetime] NULL,
	[CompanyDrivingStartDate] [datetime] NULL,
	[MstCqMvcThirdPartyTypeId] [int] NULL,
	[IsOwnValidLicense] [bit] NULL,
	[IsAttendedDriverTraining] [bit] NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTimeUtc] [datetime] NOT NULL,
	[ModifiedBy] [varchar](50) NOT NULL,
	[ModifiedDateTimeUtc] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_MstDriver] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MstLocation]    Script Date: 23/05/2019 13:27:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MstLocation](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SiteId] [int] NOT NULL,
	[Name] [varchar](max) NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTimeUtc] [datetime] NOT NULL,
	[ModifiedBy] [varchar](50) NOT NULL,
	[ModifiedDateTimeUtc] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_MstLocation] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MstOrganization]    Script Date: 23/05/2019 13:27:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MstOrganization](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ParentId] [int] NULL,
	[Name] [varchar](128) NOT NULL,
	[MstRegionId] [int] NULL,
	[MstSiteId] [int] NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTimeUtc] [datetime] NOT NULL,
	[ModifiedBy] [varchar](50) NOT NULL,
	[ModifiedDateTimeUtc] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_MstOrganization] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MstRegion]    Script Date: 23/05/2019 13:27:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MstRegion](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](128) NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTimeUtc] [datetime] NOT NULL,
	[ModifiedBy] [varchar](50) NOT NULL,
	[ModifiedDateTimeUtc] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_MstRegion] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MstSite]    Script Date: 23/05/2019 13:27:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MstSite](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MstRegionId] [int] NOT NULL,
	[Name] [varchar](128) NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTimeUtc] [datetime] NOT NULL,
	[ModifiedBy] [varchar](50) NOT NULL,
	[ModifiedDateTimeUtc] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_MstSite] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MstVehicle]    Script Date: 23/05/2019 13:27:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MstVehicle](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MstVehicleTypeId] [int] NULL,
	[LicensePlateHeader] [varchar](4) NULL,
	[LicensePlateNumber] [varchar](8) NULL,
	[LicensePlateTail] [varchar](4) NULL,
	[MstVehicleOwnershipId] [int] NULL,
	[OwnerMstCompanyId] [int] NULL,
	[MstVehicleBrandId] [int] NULL,
	[VehicleYear] [int] NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTimeUtc] [datetime] NOT NULL,
	[ModifiedBy] [varchar](50) NOT NULL,
	[ModifiedDateTimeUtc] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_MstVehicle] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MstVehicleBrand]    Script Date: 23/05/2019 13:27:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MstVehicleBrand](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](128) NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTimeUtc] [datetime] NOT NULL,
	[ModifiedBy] [varchar](50) NOT NULL,
	[ModifiedDateTimeUtc] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_MstVehicleBrand] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MstVehicleOwnership]    Script Date: 23/05/2019 13:27:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MstVehicleOwnership](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MstCompanyTypeId] [int] NOT NULL,
	[Name] [varchar](128) NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTimeUtc] [datetime] NOT NULL,
	[ModifiedBy] [varchar](50) NOT NULL,
	[ModifiedDateTimeUtc] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_MstVehicleOwnership] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MstVehicleType]    Script Date: 23/05/2019 13:27:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MstVehicleType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](32) NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTimeUtc] [datetime] NOT NULL,
	[ModifiedBy] [varchar](50) NOT NULL,
	[ModifiedDateTimeUtc] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_MstVehicleType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[MstCompany] ON 

INSERT [dbo].[MstCompany] ([Id], [MstCompanyTypeId], [Name], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (2, 2, N'Star Energy', N'superadmin', CAST(0x0000AA2300000000 AS DateTime), N'superadmin', CAST(0x0000AA2300000000 AS DateTime), 0)
INSERT [dbo].[MstCompany] ([Id], [MstCompanyTypeId], [Name], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (3, 1, N'SOLIMAN SECURITY SERVICES, INC.', N'superadmin', CAST(0x0000AA2300000000 AS DateTime), N'superadmin', CAST(0x0000AA2300000000 AS DateTime), 0)
SET IDENTITY_INSERT [dbo].[MstCompany] OFF
SET IDENTITY_INSERT [dbo].[MstCompanyType] ON 

INSERT [dbo].[MstCompanyType] ([Id], [Name], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (1, N'Contractor', N'superadmin', CAST(0x0000AA2300000000 AS DateTime), N'superadmin', CAST(0x0000AA2300000000 AS DateTime), 0)
INSERT [dbo].[MstCompanyType] ([Id], [Name], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (2, N'Company', N'superadmin', CAST(0x0000AA2300000000 AS DateTime), N'superadmin', CAST(0x0000AA2300000000 AS DateTime), 0)
SET IDENTITY_INSERT [dbo].[MstCompanyType] OFF
SET IDENTITY_INSERT [dbo].[MstDriver] ON 

INSERT [dbo].[MstDriver] ([Id], [UserName], [IsProfessional], [DrivingStartDate], [CompanyDrivingStartDate], [MstCqMvcThirdPartyTypeId], [IsOwnValidLicense], [IsAttendedDriverTraining], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (1, N'admin', 1, CAST(0x00009EB800000000 AS DateTime), CAST(0x0000A74800000000 AS DateTime), NULL, 1, 0, N'superadmin', CAST(0x0000AA3B0094044F AS DateTime), N'superadmin', CAST(0x0000AA3B0094044F AS DateTime), 0)
INSERT [dbo].[MstDriver] ([Id], [UserName], [IsProfessional], [DrivingStartDate], [CompanyDrivingStartDate], [MstCqMvcThirdPartyTypeId], [IsOwnValidLicense], [IsAttendedDriverTraining], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (6, N'mk.myudistiro', 1, CAST(0x0000990300000000 AS DateTime), CAST(0x0000A5DB00000000 AS DateTime), NULL, 1, 0, N'superadmin', CAST(0x0000AA29007E700D AS DateTime), N'superadmin', CAST(0x0000AA29007E700D AS DateTime), 0)
SET IDENTITY_INSERT [dbo].[MstDriver] OFF
SET IDENTITY_INSERT [dbo].[MstLocation] ON 

INSERT [dbo].[MstLocation] ([Id], [SiteId], [Name], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (1, 1, N'Darajat Power Plant (Location - 1)', N'superadmin', CAST(0x0000AA560068BB7A AS DateTime), N'superadmin', CAST(0x0000AA560068BB7A AS DateTime), 0)
INSERT [dbo].[MstLocation] ([Id], [SiteId], [Name], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (2, 2, N'Salak Power Plant (Location 1)', N'superadmin', CAST(0x0000AA560068D354 AS DateTime), N'superadmin', CAST(0x0000AA560068D354 AS DateTime), 0)
INSERT [dbo].[MstLocation] ([Id], [SiteId], [Name], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (3, 1, N'dadadadadadadadadadadadadada', N'superadmin', CAST(0x0000AA5600691E11 AS DateTime), N'superadmin', CAST(0x0000AA5600691E11 AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[MstLocation] OFF
SET IDENTITY_INSERT [dbo].[MstOrganization] ON 

INSERT [dbo].[MstOrganization] ([Id], [ParentId], [Name], [MstRegionId], [MstSiteId], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (1, NULL, N'Global', NULL, NULL, N'superadmin', CAST(0x0000AA0E007EA650 AS DateTime), N'superadmin', CAST(0x0000AA0E007EA650 AS DateTime), 0)
INSERT [dbo].[MstOrganization] ([Id], [ParentId], [Name], [MstRegionId], [MstSiteId], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (2, 1, N'Asia', NULL, NULL, N'superadmin', CAST(0x0000AA0E007EAD0D AS DateTime), N'superadmin', CAST(0x0000AA0E007EAD0D AS DateTime), 0)
INSERT [dbo].[MstOrganization] ([Id], [ParentId], [Name], [MstRegionId], [MstSiteId], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (3, 2, N'Indonesia', NULL, NULL, N'superadmin', CAST(0x0000AA0E007EB8C0 AS DateTime), N'superadmin', CAST(0x0000AA0E007EB8C0 AS DateTime), 0)
INSERT [dbo].[MstOrganization] ([Id], [ParentId], [Name], [MstRegionId], [MstSiteId], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (4, 3, N'Star Energy', NULL, NULL, N'superadmin', CAST(0x0000AA0E007EC0A3 AS DateTime), N'superadmin', CAST(0x0000AA0E007EC0A3 AS DateTime), 0)
INSERT [dbo].[MstOrganization] ([Id], [ParentId], [Name], [MstRegionId], [MstSiteId], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (5, 4, N'Operations', NULL, NULL, N'superadmin', CAST(0x0000AA0E008011D9 AS DateTime), N'superadmin', CAST(0x0000AA0E008011D9 AS DateTime), 0)
INSERT [dbo].[MstOrganization] ([Id], [ParentId], [Name], [MstRegionId], [MstSiteId], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (6, 5, N'Management & Staff', 1, 3, N'superadmin', CAST(0x0000AA0E00802B37 AS DateTime), N'superadmin', CAST(0x0000AA0E00802B37 AS DateTime), 0)
INSERT [dbo].[MstOrganization] ([Id], [ParentId], [Name], [MstRegionId], [MstSiteId], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (7, 5, N'Asset Darajat', 3, 1, N'superadmin', CAST(0x0000AA0E0080BCEC AS DateTime), N'superadmin', CAST(0x0000AA0E0080BCEC AS DateTime), 0)
INSERT [dbo].[MstOrganization] ([Id], [ParentId], [Name], [MstRegionId], [MstSiteId], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (8, 7, N'Operations & Maintenance', 3, 1, N'superadmin', CAST(0x0000AA0E0080D15C AS DateTime), N'superadmin', CAST(0x0000AA0E0080D15C AS DateTime), 0)
SET IDENTITY_INSERT [dbo].[MstOrganization] OFF
SET IDENTITY_INSERT [dbo].[MstRegion] ON 

INSERT [dbo].[MstRegion] ([Id], [Name], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (1, N'Jakarta', N'superadmin', CAST(0x0000AA0E007E74B7 AS DateTime), N'superadmin', CAST(0x0000AA0E007E74B7 AS DateTime), 0)
INSERT [dbo].[MstRegion] ([Id], [Name], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (2, N'Salak', N'superadmin', CAST(0x0000AA0E007E78D9 AS DateTime), N'superadmin', CAST(0x0000AA0E007E78D9 AS DateTime), 0)
INSERT [dbo].[MstRegion] ([Id], [Name], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (3, N'Darajat', N'superadmin', CAST(0x0000AA0E007E7C4C AS DateTime), N'superadmin', CAST(0x0000AA0E007E7C4C AS DateTime), 0)
SET IDENTITY_INSERT [dbo].[MstRegion] OFF
SET IDENTITY_INSERT [dbo].[MstSite] ON 

INSERT [dbo].[MstSite] ([Id], [MstRegionId], [Name], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (1, 3, N'Darajat Power Plant', N'superadmin', CAST(0x0000AA0E007E8BB5 AS DateTime), N'superadmin', CAST(0x0000AA0E007E8BB5 AS DateTime), 0)
INSERT [dbo].[MstSite] ([Id], [MstRegionId], [Name], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (2, 2, N'Salak Power Plant', N'superadmin', CAST(0x0000AA0E007E949E AS DateTime), N'superadmin', CAST(0x0000AA0E007E949E AS DateTime), 0)
INSERT [dbo].[MstSite] ([Id], [MstRegionId], [Name], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (3, 1, N'Sentral Senayan II', N'superadmin', CAST(0x0000AA0E007E9AF7 AS DateTime), N'superadmin', CAST(0x0000AA0E007E9AF7 AS DateTime), 0)
SET IDENTITY_INSERT [dbo].[MstSite] OFF
SET IDENTITY_INSERT [dbo].[MstVehicle] ON 

INSERT [dbo].[MstVehicle] ([Id], [MstVehicleTypeId], [LicensePlateHeader], [LicensePlateNumber], [LicensePlateTail], [MstVehicleOwnershipId], [OwnerMstCompanyId], [MstVehicleBrandId], [VehicleYear], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (2, 1, N'B', N'1234', N'ABC', 2, 2, 1, 2015, N'superadmin', CAST(0x0000AA290079B16D AS DateTime), N'superadmin', CAST(0x0000AA290079B16D AS DateTime), 0)
INSERT [dbo].[MstVehicle] ([Id], [MstVehicleTypeId], [LicensePlateHeader], [LicensePlateNumber], [LicensePlateTail], [MstVehicleOwnershipId], [OwnerMstCompanyId], [MstVehicleBrandId], [VehicleYear], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (3, 1, N'B', N'4321', N'ASD', 3, 2, 1, 2012, N'superadmin', CAST(0x0000AA3B00940452 AS DateTime), N'superadmin', CAST(0x0000AA3B00940452 AS DateTime), 0)
SET IDENTITY_INSERT [dbo].[MstVehicle] OFF
SET IDENTITY_INSERT [dbo].[MstVehicleBrand] ON 

INSERT [dbo].[MstVehicleBrand] ([Id], [Name], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (1, N'Samsung', N'superadmin', CAST(0x0000AA2B00000000 AS DateTime), N'superadmin', CAST(0x0000AA2B00000000 AS DateTime), 0)
SET IDENTITY_INSERT [dbo].[MstVehicleBrand] OFF
SET IDENTITY_INSERT [dbo].[MstVehicleOwnership] ON 

INSERT [dbo].[MstVehicleOwnership] ([Id], [MstCompanyTypeId], [Name], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (2, 1, N'Contract Owned', N'superadmin', CAST(0x0000AA2300000000 AS DateTime), N'superadmin', CAST(0x0000AA2300000000 AS DateTime), 0)
INSERT [dbo].[MstVehicleOwnership] ([Id], [MstCompanyTypeId], [Name], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (3, 2, N'Company Leased', N'superadmin', CAST(0x0000AA2300000000 AS DateTime), N'superadmin', CAST(0x0000AA2300000000 AS DateTime), 0)
SET IDENTITY_INSERT [dbo].[MstVehicleOwnership] OFF
SET IDENTITY_INSERT [dbo].[MstVehicleType] ON 

INSERT [dbo].[MstVehicleType] ([Id], [Name], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (1, N'Tractor', N'superadmin', CAST(0x0000AA2300000000 AS DateTime), N'superadmin', CAST(0x0000AA2300000000 AS DateTime), 0)
INSERT [dbo].[MstVehicleType] ([Id], [Name], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (2, N'Other', N'superadmin', CAST(0x0000AA2300000000 AS DateTime), N'superadmin', CAST(0x0000AA2300000000 AS DateTime), 0)
SET IDENTITY_INSERT [dbo].[MstVehicleType] OFF
ALTER TABLE [dbo].[MstCompany] ADD  CONSTRAINT [DF_MstVehicleOwner_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[MstCompanyType] ADD  CONSTRAINT [DF_MstOwnershipType_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[MstDriver] ADD  CONSTRAINT [DF_MstDriver_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[MstLocation] ADD  CONSTRAINT [DF_MstLocation_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[MstOrganization] ADD  CONSTRAINT [DF_MstOrganization_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[MstRegion] ADD  CONSTRAINT [DF_MstRegion_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[MstSite] ADD  CONSTRAINT [DF_MstSite_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[MstVehicle] ADD  CONSTRAINT [DF_MstVehicle_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[MstVehicleBrand] ADD  CONSTRAINT [DF_MstVehicleBrand_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[MstVehicleOwnership] ADD  CONSTRAINT [DF_MstVehicleOwnership_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[MstVehicleType] ADD  CONSTRAINT [DF_MstVehicleType_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[MstCompany]  WITH CHECK ADD  CONSTRAINT [FK_MstVehicleOwner_MstVehicleOwnershipType] FOREIGN KEY([MstCompanyTypeId])
REFERENCES [dbo].[MstCompanyType] ([Id])
GO
ALTER TABLE [dbo].[MstCompany] CHECK CONSTRAINT [FK_MstVehicleOwner_MstVehicleOwnershipType]
GO
ALTER TABLE [dbo].[MstLocation]  WITH CHECK ADD  CONSTRAINT [FK_MstLocation_MstSite] FOREIGN KEY([SiteId])
REFERENCES [dbo].[MstSite] ([Id])
GO
ALTER TABLE [dbo].[MstLocation] CHECK CONSTRAINT [FK_MstLocation_MstSite]
GO
ALTER TABLE [dbo].[MstOrganization]  WITH CHECK ADD  CONSTRAINT [FK_MstOrganization_MstOrganization] FOREIGN KEY([ParentId])
REFERENCES [dbo].[MstOrganization] ([Id])
GO
ALTER TABLE [dbo].[MstOrganization] CHECK CONSTRAINT [FK_MstOrganization_MstOrganization]
GO
ALTER TABLE [dbo].[MstOrganization]  WITH CHECK ADD  CONSTRAINT [FK_MstOrganization_MstRegion] FOREIGN KEY([MstRegionId])
REFERENCES [dbo].[MstRegion] ([Id])
GO
ALTER TABLE [dbo].[MstOrganization] CHECK CONSTRAINT [FK_MstOrganization_MstRegion]
GO
ALTER TABLE [dbo].[MstOrganization]  WITH CHECK ADD  CONSTRAINT [FK_MstOrganization_MstSite] FOREIGN KEY([MstSiteId])
REFERENCES [dbo].[MstSite] ([Id])
GO
ALTER TABLE [dbo].[MstOrganization] CHECK CONSTRAINT [FK_MstOrganization_MstSite]
GO
ALTER TABLE [dbo].[MstSite]  WITH CHECK ADD  CONSTRAINT [FK_MstSite_MstRegion] FOREIGN KEY([MstRegionId])
REFERENCES [dbo].[MstRegion] ([Id])
GO
ALTER TABLE [dbo].[MstSite] CHECK CONSTRAINT [FK_MstSite_MstRegion]
GO
ALTER TABLE [dbo].[MstVehicle]  WITH CHECK ADD  CONSTRAINT [FK_MstVehicle_MstVehicleBrand] FOREIGN KEY([MstVehicleBrandId])
REFERENCES [dbo].[MstVehicleBrand] ([Id])
GO
ALTER TABLE [dbo].[MstVehicle] CHECK CONSTRAINT [FK_MstVehicle_MstVehicleBrand]
GO
ALTER TABLE [dbo].[MstVehicle]  WITH CHECK ADD  CONSTRAINT [FK_MstVehicle_MstVehicleOwner] FOREIGN KEY([OwnerMstCompanyId])
REFERENCES [dbo].[MstCompany] ([Id])
GO
ALTER TABLE [dbo].[MstVehicle] CHECK CONSTRAINT [FK_MstVehicle_MstVehicleOwner]
GO
ALTER TABLE [dbo].[MstVehicle]  WITH CHECK ADD  CONSTRAINT [FK_MstVehicle_MstVehicleOwnership] FOREIGN KEY([MstVehicleOwnershipId])
REFERENCES [dbo].[MstVehicleOwnership] ([Id])
GO
ALTER TABLE [dbo].[MstVehicle] CHECK CONSTRAINT [FK_MstVehicle_MstVehicleOwnership]
GO
ALTER TABLE [dbo].[MstVehicle]  WITH CHECK ADD  CONSTRAINT [FK_MstVehicle_MstVehicleType] FOREIGN KEY([MstVehicleTypeId])
REFERENCES [dbo].[MstVehicleType] ([Id])
GO
ALTER TABLE [dbo].[MstVehicle] CHECK CONSTRAINT [FK_MstVehicle_MstVehicleType]
GO
ALTER TABLE [dbo].[MstVehicleOwnership]  WITH CHECK ADD  CONSTRAINT [FK_MstVehicleOwnership_MstVehicleOwnershipType] FOREIGN KEY([MstCompanyTypeId])
REFERENCES [dbo].[MstCompanyType] ([Id])
GO
ALTER TABLE [dbo].[MstVehicleOwnership] CHECK CONSTRAINT [FK_MstVehicleOwnership_MstVehicleOwnershipType]
GO
