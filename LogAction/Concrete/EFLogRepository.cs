﻿using Business.Concrete;
using LogAction.Abstract;
using LogAction.Entities;

namespace LogAction.Concrete
{
    public class EFLogRepository : EFBaseRepository<Log>, ILogRepository
    {
        public void Truncate()
        {
            Context.Database.ExecuteSqlCommand("TRUNCATE TABLE log");
        }
    }
}
