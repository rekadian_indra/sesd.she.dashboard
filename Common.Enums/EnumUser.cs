﻿using System.ComponentModel;

namespace Common.Enums
{
    public enum UserModule
    {
        [Description("User Management")]
        UserManagement,
        [Description("Data Master")]
        DataMaster,
        [Description("Input Normalization")]
        InputNormalization,
        [Description("Whitelist")]
        Whitelist,
    }

    public enum UserRole
    {
        [Description("Administrator")]
        Administrator
    }
}
