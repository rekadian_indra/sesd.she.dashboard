﻿using System.ComponentModel;

namespace Common.Enums
{
    public enum Day
    {
        SUNDAY = 0,
        MONDAY = 1,
        TUESDAY = 2,
        WEDNESDAY = 3,
        THURSDAY = 4,
        FRIDAY = 5,
        SATURDAY = 6,
    }

    public enum Month
    {
        [Description("Jan")]
        JAN = 1,
        [Description("Feb")]
        FEB = 2,
        [Description("Mar")]
        MAR = 3,
        [Description("Apr")]
        APR = 4,
        [Description("Mei")]
        MEI = 5,
        [Description("Jun")]
        JUN = 6,
        [Description("Jul")]
        JUL = 7,
        [Description("Agt")]
        AUG = 8,
        [Description("Sep")]
        SEP = 9,
        [Description("Okt")]
        OKT = 10,
        [Description("Nov")]
        NOV = 11,
        [Description("Des")]
        DES = 12,
    }

    public enum OptionType
    {
        REGION,
        SITE,
        ORGANIZATION,
        AD_USERS,
        MST_REGION,
        APPLICATION,
        COMPANY_TYPE,
        COMPANY,
        VEHICLE_TYPE,
        VEHICLE_BRAND,
        VEHICLE_OWNERSHIP
    }

    public enum ActionType
    {
        [Description("Manage Data")]
        MANAGE,
        [Description("View Data")]
        VIEW
    }
}
