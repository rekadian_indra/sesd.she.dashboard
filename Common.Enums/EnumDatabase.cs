﻿namespace Common.Enums
{
    public enum Field
    {
        //reuseable field
        IsDeleted,
        CreatedBy,
        CreatedDateTime,
        CreatedDateTimeUtc,
        ModifiedBy,
        ModifiedDateTime,
        ModifiedDateTimeUtc,
        Text,
        Value,
        StartDate,
        EndDate,

        //user management field
        ApplicationName,
        ApplicationId,
        UserId,
        UserName,
        IsAnonymous,
        LastActivityDate,
        RoleId,
        RoleName,
        Description,
        PropertyNames,
        PropertyValueStrings,
        PropertyValueBinary,
        LastUpdatedDate,
        ModuleId,
        ModuleName,
        ParentModule,
        ActionId,
        ActionName,
        Password,
        PasswordFormat,
        PasswordSalt,
        Email,
        PasswordQuestion,
        PasswordAnswer,
        IsApproved,
        IsLockedOut,
        CreateDate,
        LastLoginDate,
        LastPasswordChangedDate,
        LastLockoutDate,
        FailedPasswordAttemptCount,
        FailedPasswordAttemptWindowStart,
        FailedPasswordAnswerAttemptCount,
        FailedPasswordAnswerAttemptWindowsStart,
        Comment,

        //app field
        Id,
        Name,
        MstRegionId,
        RegionName,
        FullName,
        MstSiteId,
        ParentId,
        ParentName,
        SiteName,
        MstRegionName,
        MstOrganizationName,
        MstOrganizationId,
        HasRelation,
        IsInRole,
        IsChoosen,
        MstCompanyTypeId,
        CompanyTypeName,
        VehicleTypeName,
        LicensePlateHeader,
        LicensePlateNumber,
        LicensePlateTail,
        VehicleBrandName,
        VehicleOwnershipName,
        CompanyName,
        MstVehicleTypeId,
        VehicleYear,
        MstVehicleOwnershipId,
        OwnerMstCompanyId,
        MstVehicleBrandId,
        SiteId
    }

    public enum Table
    {
        //user management table
        Actions,
        Application,
        Membership,
        Module,
        Profile,
        Role,
        User,

        //app table
        MstRegion,
        MstSite,
        MstVehicleBrand,
        MstVehicleOwnership,
        MstVehicleType,
        MstCompany,
        MstCompanyType,
        MstDriver
    }
}
