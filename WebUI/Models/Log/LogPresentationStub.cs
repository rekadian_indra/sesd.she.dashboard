using Business.Infrastructure;
using LogAction.Entities;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace WebUI.Models
{
    public class LogPresentationStub
    {
        #region Global variables

        #endregion

        #region Properties

        public long Id { get; set; }
		public System.DateTime Timestamp { get; set; }
		public string Application { get; set; }
		public string Ip { get; set; }
		public string User { get; set; }
		public string Action { get; set; }
		public string Data { get; set; }

        #endregion

        #region Constructor

        public LogPresentationStub()
        {
        }
        public LogPresentationStub(Log dbItem)
        {
            ObjectMapper.MapObject<Log, LogPresentationStub>(dbItem, this);
        }

        #endregion

        #region Helper

        public MemoryStream GenerateExcel(List<LogPresentationStub> items)
        {
            //kamus lokal
            int rowIndex = 0, colIndex;
            XSSFCellStyle styleHeader, styleDate; XSSFFont font;
            XSSFRow row; XSSFCell cell;

            //algoritma
            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            XSSFWorkbook workbook = new XSSFWorkbook();
            XSSFSheet sheet = (XSSFSheet)workbook.CreateSheet("activity log");

            //create row (header)
            row = (XSSFRow)sheet.CreateRow((short)rowIndex++);

            //header style
            styleHeader = (XSSFCellStyle)workbook.CreateCellStyle();
            font = (XSSFFont)workbook.CreateFont();
            font.Boldweight = (short)FontBoldWeight.Bold; ;
            styleHeader.SetFont(font);

            //header data
            List<string> colNames = new List<string> { "id", "timestamp", "application", "ip", "user", "action", "data" };
            colIndex = 0;
            foreach (string single in colNames)
            {
                cell = (XSSFCell)row.CreateCell(colIndex++);
                cell.SetCellValue(single);
                cell.CellStyle = styleHeader;
            }

            //body
            styleDate = (XSSFCellStyle)workbook.CreateCellStyle();
            styleDate.DataFormat = workbook.CreateDataFormat().GetFormat("yyyy-mm-dd HH:mm");
            foreach (LogPresentationStub single in items)
            {
                row = (XSSFRow)sheet.CreateRow((short)rowIndex++);
                colIndex = 0;

                cell = (XSSFCell)row.CreateCell(colIndex++);
                cell.SetCellValue(single.Id);

                cell = (XSSFCell)row.CreateCell(colIndex++);
                cell.SetCellValue(single.Timestamp);
                cell.CellStyle = styleDate;

                cell = (XSSFCell)row.CreateCell(colIndex++);
                cell.SetCellValue(single.Application);

                cell = (XSSFCell)row.CreateCell(colIndex++);
                cell.SetCellValue(single.Ip);

                cell = (XSSFCell)row.CreateCell(colIndex++);
                cell.SetCellValue(single.User);

                cell = (XSSFCell)row.CreateCell(colIndex++);
                cell.SetCellValue(single.Action);

                cell = (XSSFCell)row.CreateCell(colIndex++);
                cell.SetCellValue(single.Data);
            }

            //write to file
            MemoryStream ms = new MemoryStream();
            workbook.Write(ms);

            return ms;
        }

        #endregion
    }
}
