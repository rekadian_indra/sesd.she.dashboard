using Business.Infrastructure;
using LogAction.Entities;
using System.ComponentModel;

namespace WebUI.Models
{
    public class LogFormStub
    {
        #region Global variables

        #endregion

        #region Properties

        [DisplayName("Id")]
        //[Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Resources.MyGlobalErrors))]
		public long Id { get; set; }

		[DisplayName("Timestamp")]
        //[Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Resources.MyGlobalErrors))]
		public System.DateTime Timestamp { get; set; }

		[DisplayName("Application")]
        //[Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Resources.MyGlobalErrors))]
		public string Application { get; set; }

		[DisplayName("Ip")]
        //[Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Resources.MyGlobalErrors))]
		public string Ip { get; set; }

		[DisplayName("User")]
        //[Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Resources.MyGlobalErrors))]
		public string User { get; set; }

		[DisplayName("Action")]
        //[Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Resources.MyGlobalErrors))]
		public string Action { get; set; }

		[DisplayName("Data")]
        //[Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Resources.MyGlobalErrors))]
		public string Data { get; set; }

        #endregion

        #region Constructor

        public LogFormStub()
        {
        }

		public LogFormStub(Log dbItem) : this()
		{
            ObjectMapper.MapObject<Log, LogFormStub>(dbItem, this);
		}

        #endregion

        #region Helper

        public void MapDbObject(Log dbItem) {
            ObjectMapper.MapObject<LogFormStub, Log>(this, dbItem);
        }

        #endregion
    }
}

