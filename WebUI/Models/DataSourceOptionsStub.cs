﻿using System.Collections.Generic;

namespace WebUI.Models
{
    public class DataSourceOptionsStub
    {
        public string Controller { get; set; }
        public int? Page { get; set; }
        public int? PageSize { get; set; }
        public ICollection<SortQuery> Sorts { get; set; }
        public FilterQuery Filter { get; set; }

        public DataSourceOptionsStub()
        {
            Sorts = new HashSet<SortQuery>();
        }

        public class SortQuery
        {
            public string field { get; set; }
            public string dir { get; set; }
        }

        public class FilterQuery
        {
            public string logic { get; set; }
            public string field { get; set; }
            public string @operator { get; set; }
            public object value { get; set; }
            public ICollection<FilterQuery> filters { get; set; }

            public FilterQuery()
            {
                filters = new HashSet<FilterQuery>();
            }
        }
    }
}