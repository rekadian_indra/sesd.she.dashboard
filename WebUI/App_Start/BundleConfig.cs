﻿using System.Web.Optimization;

namespace WebUI
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            #region StyleBundle

            bundles.Add(new StyleBundle("~/Content/kendo/css").Include(
                "~/Content/kendo/kendo.common-bootstrap.min.css",
                "~/Content/kendo/kendo.bootstrap.min.css"));

            bundles.Add(new StyleBundle("~/Content/inspinia/css").Include(
                "~/Content/inspinia/bootstrap.min.css",
                "~/Content/inspinia/style.css",
                "~/Content/inspinia/animate.css",
                "~/Content/inspinia/font-awesome.min.css"));

            bundles.Add(new StyleBundle("~/Content/popup/css").Include(
                "~/Content/popup.css"));

            bundles.Add(new StyleBundle("~/Content/sweet-alert/css").Include(
                "~/Content/sweet-alert/sweet-alert.css"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/login/css").Include(
              "~/Content/login.css"));

            #endregion

            #region ScriptBundle
            // JQuery Script Vendor
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                    "~/Scripts/jquery-3.3.1.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jquerymigrate").Include(
                    "~/Scripts/jquery-migrate-3.0.0.min.js"));

            // jQuery Validation
            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                   "~/Scripts/jquery.unobtrusive-ajax.min.js",
                   "~/Scripts/jquery.validate.min.js",
                   "~/Scripts/jquery.validate.extension.js",
                   "~/Scripts/jquery.validate.unobtrusive.min.js"));

            // Bootstrap script
            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                    "~/Scripts/bootstrap.min.js"));

            // Inspinia script
            bundles.Add(new ScriptBundle("~/bundles/inspinia").Include(
                    "~/Scripts/Inspinia/app/inspinia.js"));

            // SlimScroll
            bundles.Add(new ScriptBundle("~/plugins/slimScroll").Include(
                    "~/Scripts/Inspinia/plugins/slimScroll/jquery.slimscroll.min.js"));

            // jQuery plugins
            bundles.Add(new ScriptBundle("~/plugins/metsiMenu").Include(
                    "~/Scripts/Inspinia/plugins/metisMenu/metisMenu.min.js"));

            bundles.Add(new ScriptBundle("~/plugins/pace").Include(
                    "~/Scripts/Inspinia/plugins/pace/pace.min.js"));

            // Sweetalert Script
            bundles.Add(new ScriptBundle("~/bundles/sweet-alert").Include(
                    "~/Scripts/sweet-alert/sweet-alert.min.js"));

            // Helper Script
            bundles.Add(new ScriptBundle("~/bundles/webapp").Include(
                    "~/Scripts/webapp.js"));

            // kendo Script
            bundles.Add(new ScriptBundle("~/bundles/kendo").Include(
                "~/Scripts/kendo/kendo.all.min.js",
                "~/Scripts/kendo/cultures/kendo.culture.id-ID.min.js"));

            // modernizr script
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                "~/Scripts/modernizr-2.8.3"));

            #endregion

            bundles.IgnoreList.Clear();
            BundleTable.EnableOptimizations = false;
        }
    }
}
