using Business.Abstract;
using Common.Enums;
using SecurityGuard.Interfaces;
using SecurityGuard.Services;
using WebUI.Areas.UserManagement.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Mvc;
using System.Web.Security;
using WebUI.Controllers;
using WebUI.Infrastructure;
using System.Linq;
using System.Threading.Tasks;
using Business.Entities;
using Business.Infrastructure;
using Newtonsoft.Json;
using MvcSiteMapProvider;
using Resources;
using WebUI.Extension;
using MvcSiteMapProvider.Web.Mvc.Filters;
using Business.Extension;
using WebUI.Models;

namespace WebUI.Areas.UserManagement.Controllers
{
    [AuthorizeUser(ModuleName = UserModule.UserManagement)]
    public partial class RoleController : BaseController<RoleFormStub>
    {
        private IRoleService RoleService { get; set; }

        public RoleController(
            IModuleRepository repoParam, 
            IActionRepository repoActionParam, 
            IRoleRepository repoRole, 
            IModulesInRoleRepository repoModulesInRole,
            IApplicationRepository applicationRepository
        )
        {
            RoleService = new RoleService(Roles.Provider);
            
            RepoModule = repoParam;
            RepoAction = repoActionParam;
            RepoRole = repoRole;
            RepoModulesInRole = repoModulesInRole;
            RepoApplication = applicationRepository;
        }

        [MvcSiteMapNode(Title = TitleSite.Role, Area = AreaSite.UserManagement, ParentKey = KeySite.DashboardUserManagement, Key = KeySite.IndexRole)]
        public override async Task<ActionResult> Index(object args = null)
        {
            //ManageRolesViewModel model = new ManageRolesViewModel();
            //model.Roles = new SelectList(RoleService.GetAllRoles());
            //model.RoleList = RoleService.GetAllRoles();

            //await Task.Delay(0);

            //return View(model);

            return await base.Index();
        }

        [MvcSiteMapNode(Title = TitleSite.Create, Area = AreaSite.UserManagement, ParentKey = KeySite.IndexRole, Key = KeySite.CreateRole)]
        public override async Task<ActionResult> Create()
        {
            RoleFormStub model = new RoleFormStub();

            await Task.Delay(0);

            return View("Form", model);
        }

        [HttpPost]
        public override async Task<ActionResult> Create(RoleFormStub model)
        {
            if (ModelState.IsValid)
            {
                Role dbObject = new Role();

                //map dbObject
                model.MapDbObject(dbObject);

                //save dbObject
                if(await RepoRole.SaveAsync(dbObject))
                {
                    //message
                    this.SetMessage(model.RoleName, AppGlobalMessage.CreateSuccess);
                }

                return RedirectToAction(ActionSite.Index);
            }
            else
            {
                return View("Form", model);
            }
        }

        [SiteMapTitle(TitleSite.Breadcrumb)]
        [MvcSiteMapNode(Title = TitleSite.Edit, Area = AreaSite.UserManagement, ParentKey = KeySite.IndexRole, Key = KeySite.EditRole)]
        public override async Task<ActionResult> Edit(params object[] id)
        {
            if (id == null)
                throw new ArgumentNullException();

            Guid primaryKey = id[0].ToGuid();

            Role dbObject = await RepoRole.FindByPrimaryKeyAsync(primaryKey);
            RoleFormStub model = new RoleFormStub(dbObject);

            ViewBag.Breadcrumb = model.RoleName ;

            return View("Form", model);
        }

        [HttpPost]
        public override async Task<ActionResult> Edit(RoleFormStub model)
        {
            if (ModelState.IsValid)
            {
                Role dbObject = await RepoRole.FindByPrimaryKeyAsync(model.RoleId);

                //map dbObject
                model.MapDbObject(dbObject);

                //save dbObject
                if (await RepoRole.SaveAsync(dbObject))
                {
                    //message
                    this.SetMessage(model.RoleName, AppGlobalMessage.EditSuccess);
                }

                return RedirectToAction(ActionSite.Index);
            }
            else
            {
                return View("Form", model);
            }
        }

        [HttpPost]
        public override async Task<ActionResult> Delete(params object[] id)
        {
            if (id == null)
                throw new ArgumentNullException();

            Guid primaryKey = id[0].ToGuid();
            Role dbObject = await RepoRole.FindByPrimaryKeyAsync(primaryKey);
            ResponseModel response = new ResponseModel(true);

            if (!(await RepoRole.DeleteAsync(dbObject)))
            {
                response.SetFail(AppGlobalMessage.DeleteFailed);
            }

            return Json(response);
        }

        [MvcSiteMapNode(Title = TitleSite.AssignRevokeModules, Area = AreaSite.UserManagement, ParentKey = KeySite.IndexRole, Key = KeySite.AssignRevokeModules)]
        public async Task<ActionResult> AssignRevokeModules(params object[] id)
        {
            if (id == null)
                throw new ArgumentNullException();

            Guid primaryKey = id[0].ToGuid();
            Role dbObject = await RepoRole.FindByPrimaryKeyAsync(primaryKey);

            AssignRevokeModulesFormStub model = new AssignRevokeModulesFormStub(dbObject);

            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> AssignRevokeModules(AssignRevokeModulesFormStub model)
        {
            List<ModulesInRole> modulesInRoles = RepoModulesInRole.FindAll(new FilterQuery(Field.RoleId,FilterOperator.Equals,model.RoleId));
            if(modulesInRoles != null && modulesInRoles.Any())
            {
                foreach(ModulesInRole m in modulesInRoles)
                {
                    List<Guid> g = new List<Guid>();
                    foreach(Business.Entities.Action a in m.Actions)
                    {
                        g.Add(a.ActionId);
                    }
                    if (g.Any())
                    {
                        RepoModulesInRole.RevokeChoosenActions(m, g);
                    }
                }
                RepoModulesInRole.DeleteAll(modulesInRoles);
            }

            if(model.MIRFormStubs != null && model.MIRFormStubs.Any())
            {
                foreach (ModulesInRolesFormStub m in model.MIRFormStubs)
                {
                    ModulesInRole dbObject = new ModulesInRole();
                    m.MapDbObject(dbObject);

                    if(m.ActionIds != null && m.ActionIds.Any())
                    {
                        RepoModulesInRole.Save(dbObject);
                        RepoModulesInRole.AssignChoosenActions(dbObject, m.ActionIds);
                    }
                }
            }

            //message
            this.SetMessage("Assing / Revoke Modules to Roles " + model.RoleName + " Success");

            await Task.Delay(0);

            return RedirectToAction(ActionSite.Index);
        }

        public override async Task<string> Binding(params object[] args)
        {
            //lib
            int count;
            List<Role> dbObjects;
            List<RolePresentationStub> models;
            Task<List<Role>> dbObjectsTask = null;
            Task<int> countTask = null;
            QueryRequestParameter param = QueryRequestParameter.Current;

            dbObjectsTask = RepoRole.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
            countTask = RepoRole.CountAsync(param.Filter);

            //get and count data from db in background thread
            await Task.WhenAll(dbObjectsTask, countTask);

            //get callback from task
            dbObjects = dbObjectsTask.Result;
            count = countTask.Result;

            models = ListMapper.MapList<Role, RolePresentationStub>(dbObjects);

            return JsonConvert.SerializeObject(new { total = count, data = models });
        }

        public async Task<string> BindingModules(params object[] args)
        {
            //lib
            List<Module> dbObjects;
            List<ModulePresentationStub> models;
            Task<List<Module>> dbObjectsTask = null;
            QueryRequestParameter param = QueryRequestParameter.Current;

            string roleId = null;

            if(param.Filter != null && param.Filter.Filters.Any())
            {
                FilterQuery filterQuery = param.Filter.Filters.FirstOrDefault(a => a.Field == Field.RoleId);
                if(filterQuery != null)
                {
                    roleId = filterQuery.Value.ToString();
                    param.Filter.Filters.Remove(filterQuery);
                }
            }

            dbObjectsTask = RepoModule.FindAllAsync(param.Sorts, param.Filter);

            //get and count data from db in background thread
            await Task.WhenAll(dbObjectsTask);

            //get callback from task
            dbObjects = dbObjectsTask.Result;

            models = ListMapper.MapList<Module, ModulePresentationStub>(dbObjects);

            if(roleId != null)
            {
                foreach (ModulePresentationStub m in models)
                {
                    Module module = dbObjects.FirstOrDefault(a => a.ModuleId == m.ModuleId);
                    if (module != null)
                    {
                        if(module.ModulesInRoles != null && module.ModulesInRoles.Any())
                        {
                            ModulesInRole modulesInRole = module.ModulesInRoles.FirstOrDefault(a => a.ModuleId == m.ModuleId && a.RoleId == roleId.ToGuid());
                            if(modulesInRole != null && modulesInRole.Actions != null && modulesInRole.Actions.Any())
                            {
                                m.IsInRole = true;
                            }
                        }
                    }
                }
            }

            return JsonConvert.SerializeObject(new { data = models });
        }

        public async Task<string> BindingActionsInModules(params object[] args)
        {
            QueryRequestParameter param = QueryRequestParameter.Current;

            FilterQuery filterQuery = null;
            SortQuery sortQuery = null;

            string roleId = null;

            if (param.Filter != null && param.Filter.Filters.Any())
            {
                filterQuery = param.Filter.Filters.FirstOrDefault(a => a.Field == Field.RoleId);
                if (filterQuery != null)
                {
                    roleId = filterQuery.Value.ToString();
                    param.Filter.Filters.Remove(filterQuery);
                }
            }

            if (param.Filter != null && param.Filter.Filters.Any())
            {
                filterQuery = param.Filter.Filters.FirstOrDefault(a => a.Field == Field.ActionName);
                if(filterQuery != null)
                {
                    param.Filter.Filters.Remove(filterQuery);
                }
            }

            if(param.Sorts != null && param.Sorts.Any())
            {
                sortQuery = param.Sorts.FirstOrDefault(a => a.Field == Field.ActionName);
                if(sortQuery != null)
                {
                    param.Sorts.Remove(sortQuery);
                }
            }

            Module module = await RepoModule.FindAsync(param.Sorts,param.Filter);
            List<ActionPresentationStub> models = new List<ActionPresentationStub>();
            if(module != null && module.Actions != null && module.Actions.Any())
            {
                models = ListMapper.MapList<Business.Entities.Action, ActionPresentationStub>(module.Actions);
            }

            if(filterQuery != null && models.Any())
            {
                models = models.FindAll(a => a.ActionName.ToLower().Contains(filterQuery.Value.ToString().ToLower()));
            }

            if (sortQuery != null && models.Any())
            {
                if(sortQuery.Direction.Value == SortOrder.Ascending)
                {
                    models = models.OrderBy(a => a.ActionName).ToList();
                }
                else
                {
                    models = models.OrderByDescending(a => a.ActionName).ToList();
                }
            }

            if(roleId != null && module != null)
            {
                if (module.ModulesInRoles != null && module.ModulesInRoles.Any())
                {
                    ModulesInRole modulesInRole = module.ModulesInRoles.FirstOrDefault(a => a.ModuleId == module.ModuleId && a.RoleId == roleId.ToGuid());
                    if (modulesInRole != null && modulesInRole.Actions != null && modulesInRole.Actions.Any())
                    {
                        foreach(ActionPresentationStub a in models)
                        {
                            if(modulesInRole.Actions.Any(b => b.ActionId == a.ActionId))
                            {
                                a.IsChoosen = true;
                            }
                        }
                    }
                }
            }

            return JsonConvert.SerializeObject(new { data = models });
        }

        public async Task<JsonResult> RoleNameExists(string RoleName, string CurrentRoleName, Guid ApplicationId)
        {
            //lib
            bool exist;

            //algorithm
            if (string.IsNullOrWhiteSpace(RoleName))
            {
                exist = true;
            }
            else if (RoleName.ToLower() == CurrentRoleName.ToLower())
            {
                exist = false;
            }
            else
            {
                Role dbObject;
                FilterQuery filter = new FilterQuery(
                    Field.RoleName,
                    FilterOperator.Equals,
                    RoleName
                );
                filter.AddFilter(Field.ApplicationId,FilterOperator.Equals,ApplicationId);
                dbObject = await RepoRole.FindAsync(filter);

                exist = dbObject != null;
            }

            return Json(!exist, JsonRequestBehavior.AllowGet);
        }
    }
}
