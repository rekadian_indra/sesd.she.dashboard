using Business.Abstract;
using Common.Enums;
using WebUI.Areas.UserManagement.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using WebUI.Controllers;
using WebUI.Infrastructure;

namespace WebUI.Areas.UserManagement.Controllers
{
    [AuthorizeUser(ModuleName = UserModule.UserManagement)]
    public partial class ActionController : BaseController<ManageActionsViewModel>
    {
        private IActionRepository RepoAction { get; set; }

        public ActionController(IActionRepository repoParam)
        {
            RepoAction = repoParam;
        }

        //[MvcSiteMapNode(Title = "Manage Actions", ParentKey = "IndexSecurityGuard", Key = "ManageActionsModule")]
        //[SiteMapTitle("Breadcrumb")]
        public virtual async Task<ActionResult> Index()
        {
            //kamus
            ManageActionsViewModel model = new ManageActionsViewModel();
            var actions = await RepoAction.FindAllAsync();
            string[] actionsArr;

            //algo
            actionsArr = new string[actions.Count];
            for(int i =0;i<actions.Count; ++i){
                actionsArr[i] = actions[i].ActionName;
            }
            model.Actions = new SelectList(actionsArr);
            model.ActionList = actionsArr;
            return View(model);
        }

        #region Create Actions Methods

        [HttpGet]
        //[MvcSiteMapNode(Title = "Manage Actions", ParentKey = "IndexSecurityGuard", Key = "ManageActions")]
        //[SiteMapTitle("Breadcrumb")]
        public virtual ActionResult CreateAction()
        {
            return View(new ActionViewModel());
        }

        [HttpPost]
        public virtual ActionResult CreateAction(string actionName)
        {
            JsonResponse response = new JsonResponse();

            if (string.IsNullOrEmpty(actionName))
            {
                response.Success = false;
                response.Message = "You must enter a action name.";
                response.CssClass = "red";

                return Json(response);
            }

            try
            {
                Business.Entities.Action a = new Business.Entities.Action();
                a.ActionId = Guid.NewGuid();
                a.ActionName = actionName;
                RepoAction.Save(a);
                
                if (Request.IsAjaxRequest())
                {
                    response.Success = true;
                    response.Message = "Role created successfully!";
                    response.CssClass = "green";

                    return Json(response);
                }

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                if (Request.IsAjaxRequest())
                {
                    response.Success = false;
                    response.Message = ex.InnerException.Message;
                    response.CssClass = "red";

                    return Json(response);
                }

                ModelState.AddModelError("", ex.InnerException.Message);
            }

            return RedirectToAction("Index");
        }

        #endregion

        #region Delete Roles Methods

        /// <summary>
        /// This is an Ajax method.
        /// </summary>
        /// <param name="actionName"></param>
        /// <returns></returns>
        [HttpPost]
        public virtual ActionResult DeleteAction(string actionName)
        {
            JsonResponse response = new JsonResponse();

            if (string.IsNullOrEmpty(actionName))
            {
                response.Success = false;
                response.Message = "You must select a Action Name to delete.";
                response.CssClass = "red";

                return Json(response);
            }
            try
            {
                RepoAction.Delete(actionName, true);

                response.Success = true;
                response.Message = actionName + " was deleted successfully!";
                response.CssClass = "green";

                return Json(response);
            }
            catch
            {      
                response.Success = false;
                response.Message = "Action was used in modules";
                response.CssClass = "red";

                return Json(response);
            }
        }

        [HttpPost]
        public ActionResult DeleteActions(string actions, bool throwOnPopulatedAction)
        {
            JsonResponse response = new JsonResponse
            {
                Messages = new List<ResponseItem>()
            };

            if (string.IsNullOrEmpty(actions))
            {
                response.Success = false;
                response.Message = "You must select at least one action.";
                return Json(response);
            }

            string[] actionNames = actions.Split(',');
            StringBuilder sb = new StringBuilder();

            ResponseItem item = null;

            foreach (var action in actionNames)
            {
                if (!string.IsNullOrEmpty(action))
                {
                    try
                    {
                        RepoAction.Delete(action, throwOnPopulatedAction);//module not yet deleted

                        item = new ResponseItem();
                        item.Success = true;
                        item.Message = "Deleted this action successfully - " + action;
                        item.CssClass = "green";
                        response.Messages.Add(item);

                        //sb.AppendLine("Deleted this role successfully - " + role + "<br />");
                    }
                    catch
                    {
                        //sb.AppendLine(role + " - " + ex.Message + "<br />");

                        item = new ResponseItem();
                        item.Success = false;
                        item.Message = "Action was used in modules";
                        item.CssClass = "yellow";
                        response.Messages.Add(item);
                    }
                }
            }

            response.Success = true;
            response.Message = sb.ToString();

            return Json(response);
        }

        #endregion

        #region Get Modules In Action methods

        /// <summary>
        /// This is an Ajax method that populates the 
        /// Actions drop down list.
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> GetAllAction()
        {
            var list = await RepoAction.FindAllAsync();

            List<SelectObject> selectList = new List<SelectObject>();

            foreach (Business.Entities.Action item in list)
            {
                selectList.Add(new SelectObject() { caption = item.ActionName, value = item.ActionName.ToString() });
            }

            return Json(selectList, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetModulesInAction(string actionName)
        {
            var list = RepoAction.GetModulesInAction(actionName);

            return Json(list, JsonRequestBehavior.AllowGet);
        }


        #endregion
    }
}
