using Business.Abstract;
using Business.Entities;
using Business.Extension;
using Business.Infrastructure;
using Common.Enums;
using MvcSiteMapProvider;
using Newtonsoft.Json;
using SecurityGuard.Interfaces;
using SecurityGuard.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Security;
using WebUI.Areas.DataMaster.Models.Organizations;
using WebUI.Areas.UserManagement.Models;
using WebUI.Controllers;
using WebUI.Extension;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Areas.UserManagement.Controllers
{
    [AuthorizeUser(ModuleName = UserModule.UserManagement)]
    public partial class UserController : BaseController<UserFormStub>
    {
        private IMembershipService MembershipService { get; set; }
        private IRoleService RoleService { get; set; }

        public UserController(
            IUserRepository repoUser,
            IRoleRepository repoRole,
            IMembershipRepository membershipRepository,
            IOrganizationRepository organizationRepository,
            IUsersInRolesRepository usersInRolesRepository
        )
        {
            RepoUser = repoUser;
            RepoRole = repoRole;
            RepoUsersInRoles = usersInRolesRepository;
            RepoMembership = membershipRepository;
            MembershipService = new MembershipService(System.Web.Security.Membership.Provider);
            RoleService = new RoleService(Roles.Provider);
            RepoOrganization = organizationRepository;
        }

        [MvcSiteMapNode(Title = TitleSite.User, Area = AreaSite.UserManagement, ParentKey = KeySite.DashboardUserManagement, Key = KeySite.IndexUser)]
        public override async Task<ActionResult> Index(object args = null)
        {
            return await base.Index();
        }

        [HttpPost]
        public async Task<ActionResult> Index(UserFormStub model)
        {
            if (ModelState.IsValid || !ModelState.IsValidField(Field.Password.ToString()))
            {
                string template;
                MembershipUser membership = MembershipService.CreateUser(model.UserName, "123456", model.Email);
                if (membership != null)
                {
                    ActiveDirectoryUtil user = ActiveDirectoryUtil.GetUsers(model.UserName).FirstOrDefault();
                    FilterQuery filter = new FilterQuery(Field.Email, FilterOperator.Equals, user.Email);
                    Business.Entities.Membership membershipUser = RepoMembership.Find(filter);

                    //membershipUser.FullName = user.FullName;

                    if (await RepoMembership.SaveAsync(membershipUser))
                    {
                        template = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.CreateSuccess).ToString();
                        this.SetMessage(model.UserName, template);
                    }
                }

                return RedirectToAction("Index");
            }
            else
            {
                return View(model);
            }
        }

        public override async Task<ActionResult> Create()
        {
            UserFormStub model = new UserFormStub
            {
                IsNewUser = true,
                RequireSecretQuestionAndAnswer = MembershipService.RequiresQuestionAndAnswer
            };

            await Task.Delay(0);

            return PartialView("_Form", model);
        }

        [HttpPost]
        public override async Task<ActionResult> Create(UserFormStub model)
        {
            ResponseModel response = new ResponseModel(true);

            if (ModelState.IsValid)
            {
                MembershipCreateStatus status;
                MembershipUser membership;

                model.Approve = true;
                membership = MembershipService.CreateUser(
                    model.UserName,
                    model.Password,
                    model.Email,
                    model.SecretQuestion,
                    model.SecretAnswer,
                    model.Approve,
                    out status
                );

                if (membership != null && status == MembershipCreateStatus.Success)
                {
                    string template = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.CreateSuccess).ToString();
                    string message = string.Format(template, membership.UserName);

                    if (!string.IsNullOrEmpty(model.FullName))
                    {
                        UserProfile profile = UserProfile.GetUserProfile(membership.UserName);

                        profile.FullName = model.FullName;
                        profile.Save();
                    }

                    response.Message = message;
                }
                else
                {
                    switch (status)
                    {
                        case MembershipCreateStatus.InvalidPassword:
                            response.SetFail("Password must be at least 6 characters.");

                            break;
                        case MembershipCreateStatus.InvalidEmail:
                            response.SetFail("The provided email is wrong.");

                            break;
                        case MembershipCreateStatus.DuplicateUserName:
                            response.SetFail("Username is already used.");

                            break;
                    }
                }
            }
            else
            {
                response.SetFail("Please fill required field.");
            }

            await Task.Delay(0);

            return Json(response);
        }

        public override async Task<ActionResult> Edit(params object[] id)
        {
            if (id == null)
                throw new ArgumentNullException();

            FilterQuery filter = new FilterQuery(Field.UserName, FilterOperator.Equals, id.ElementAt(0));
            User dbObject = await RepoUser.FindAsync(filter);
            UserFormStub model = new UserFormStub(dbObject);


            return PartialView("_Form", model);
        }

        [HttpPost]
        public override async Task<ActionResult> Edit(UserFormStub model)
        {
            ResponseModel response = new ResponseModel(true);

            if (ModelState.IsValid)
            {
                MembershipUser membership = MembershipService.GetUser(model.UserName);
                UserProfile profile = UserProfile.GetUserProfile(membership.UserName);
                string template = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.EditSuccess).ToString();
                string message = string.Format(template, model.UserName);

                //update user
                membership.Email = model.Email;
                MembershipService.UpdateUser(membership);

                FilterQuery filter = new FilterQuery(Field.UserName, FilterOperator.Equals, model.UserName);
                User user = await RepoUser.FindAsync(filter);

                //update profile
                if (string.IsNullOrEmpty(model.FullName))
                {
                    await RepoUser.RemoveProfileAsync(user);
                }
                else
                {
                    profile.FullName = model.FullName;
                    profile.Save();
                }

                user.Membership.MstOrganizationId = model.MstOrganizationId;

                await RepoUser.SaveAsync(user);

                //set message
                response.Message = message;
            }
            else
            {
                response.SetFail("Please fill required field.");
            }

            await Task.Delay(0);

            return Json(response);
        }

        [HttpPost]
        public override async Task<ActionResult> Delete(params object[] id)
        {
            if (id == null)
                throw new ArgumentNullException();

            string userName = id.ElementAt(0).ToString();
            ResponseModel response = new ResponseModel(true);

            if (!MembershipService.DeleteUser(userName))
            {
                string message = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.DeleteFailed).ToString();
                response.SetFail(message);
            }

            await Task.Delay(0);

            return Json(response);
        }

        //public async Task<ActionResult> AssignRoles(string id)
        //{
        //    if (string.IsNullOrEmpty(id))
        //        throw new ArgumentNullException();

        //    FilterQuery filter = new FilterQuery(Field.UserName, FilterOperator.Equals, id);
        //    User dbObject = await RepoUser.FindAsync(filter);
        //    UserFormStub model = new UserFormStub(dbObject);

        //    return PartialView("_AssignRoles", model);
        //}

        //[HttpPost]
        //public async Task<ActionResult> AssignRoles(AssignRoleStub model)
        //{
        //    ResponseModel response = new ResponseModel(true);
        //    User user = await RepoUser.FindByPrimaryKeyAsync(model.UserId);
        //    List<Role> roles = new List<Role>();

        //    foreach (RoleFormStub m in model.Roles)
        //    {
        //        Role role = new Role();

        //        m.MapDbObject(role);
        //        roles.Add(role);
        //    }

        //    await RepoUser.AssignRoleAsync(user, roles);

        //    return Json(response);
        //}

        //[HttpPost]
        //public async Task<ActionResult> RevokeRoles(AssignRoleStub model)
        //{
        //    ResponseModel response = new ResponseModel(true);
        //    User user = await RepoUser.FindByPrimaryKeyAsync(model.UserId);
        //    List<Role> roles = new List<Role>();

        //    foreach (RoleFormStub m in model.Roles)
        //    {
        //        Role role = new Role();

        //        m.MapDbObject(role);
        //        roles.Add(role);
        //    }

        //    await RepoUser.RevokeRoleAsync(user, roles);

        //    return Json(response);
        //}

        [MvcSiteMapNode(Title = TitleSite.AssignRevokeRoles, Area = AreaSite.UserManagement, ParentKey = KeySite.IndexUser, Key = KeySite.AssignRevokeRoles)]
        public async Task<ActionResult> AssignRevokeRoles(string userName)
        {
            if (string.IsNullOrEmpty(userName))
                throw new ArgumentNullException();

            FilterQuery filter = new FilterQuery(Field.UserName, FilterOperator.Equals, userName);
            User dbObject = await RepoUser.FindAsync(filter);
            AssignRevokeRolesFormStub model = new AssignRevokeRolesFormStub(dbObject);

            return View("AssignRevokeRoles", model);
        }

        [HttpPost]
        public async Task<ActionResult> AssignRevokeRoles(AssignRevokeRolesFormStub model)
        {
            List<UsersInRole> usersInRoles = await RepoUsersInRoles.FindAllAsync(new FilterQuery(Field.UserId,FilterOperator.Equals, model.UserId));
            List<UsersInRole> dbObjects = new List<UsersInRole>();

            if (model.UIRFormStubs.HasValue())
            {
                foreach(UsersInRolesFormStub usersInRolesFormStub in model.UIRFormStubs)
                {
                    UsersInRole usersInRole = usersInRoles.FirstOrDefault(a => a.UserId == usersInRolesFormStub.UserId && a.RoleId == usersInRolesFormStub.RoleId && a.MstOrganizationId == usersInRolesFormStub.MstOrganizationId);
                    if(usersInRole != null)
                    {
                        usersInRoles.Remove(usersInRole);
                    }
                    else
                    {
                        usersInRole = new UsersInRole();
                        usersInRolesFormStub.MapDbObject(usersInRole);
                        dbObjects.Add(usersInRole);
                    }
                }
            }

            if (usersInRoles.Any())
            {
                await RepoUsersInRoles.DeleteAllAsync(usersInRoles);
            }

            if (dbObjects.Any())
            {
                await RepoUsersInRoles.SaveAllAsync(dbObjects);
            }

            return RedirectToAction(ActionSite.Index);
        }

        public async Task<JsonResult> UserNameExists(string UserName, string CurrentUserName)
        {
            //lib
            bool exist;

            //algorithm
            if (string.IsNullOrWhiteSpace(UserName))
            {
                exist = true;
            }
            else if (UserName.ToLower() == CurrentUserName.ToLower())
            {
                exist = false;
            }
            else
            {
                FilterQuery filter = new FilterQuery(
                    Field.UserName,
                    FilterOperator.Equals,
                    UserName
                );
                User user = await RepoUser.FindAsync(filter);

                exist = user != null;
            }

            return Json(!exist, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> EmailExists(string Email, string CurrentEmail)
        {
            //lib
            bool exist;

            //algorithm
            if (string.IsNullOrWhiteSpace(Email))
            {
                exist = true;
            }
            else if (Email.ToLower() == CurrentEmail.ToLower())
            {
                exist = false;
            }
            else
            {
                FilterQuery filter = new FilterQuery(
                    new FieldHelper(Table.Membership, Field.Email),
                    FilterOperator.Equals,
                    Email
                );
                User user = await RepoUser.FindAsync(filter);

                exist = user != null;
            }

            return Json(!exist, JsonRequestBehavior.AllowGet);
        }

        #region Binding

        public override async Task<string> Binding(params object[] args)
        {
            //lib
            int count;
            List<User> dbObjects;
            List<UserPresentationStub> models;
            Task<List<User>> dbObjectsTask = null;
            Task<int> countTask = null;
            QueryRequestParameter param = QueryRequestParameter.Current;

            SortQuery sortQuery = null;

            if(param.Sorts != null && param.Sorts.Any())
            {
                sortQuery = param.Sorts.FirstOrDefault(a => a.Field == Field.MstOrganizationName);
                if(sortQuery != null)
                {
                    param.Sorts.Remove(sortQuery);
                }
            }

            //algorithm
            dbObjectsTask = RepoUser.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
            countTask = RepoUser.CountAsync(param.Filter);

            //get and count data from db in background thread
            await Task.WhenAll(dbObjectsTask, countTask);

            //get callback from task
            dbObjects = dbObjectsTask.Result;
            count = countTask.Result;

            //map db data to model
            models = ListMapper.MapList<User, UserPresentationStub>(dbObjects);

            List<MstOrganization> mstOrganizations = await RepoOrganization.FindAllAsync();

            if(mstOrganizations != null && mstOrganizations.Any())
            {
                foreach (UserPresentationStub m in models)
                {
                    MstOrganization organization = mstOrganizations.FirstOrDefault(a => a.Id == m.MstOrganizationId);
                    if(organization != null)
                    {
                        m.MstOrganizationName = organization.Name;
                    }
                    else
                    {
                        m.MstOrganizationName = "-";
                    }
                }
            }

            if(sortQuery != null)
            {
                if(sortQuery.Direction == SortOrder.Ascending)
                {
                    models = models.OrderBy(a => a.MstOrganizationName).ToList();
                }
                else
                {
                    models = models.OrderByDescending(a => a.MstOrganizationName).ToList();
                }
            }

            return JsonConvert.SerializeObject(new { total = count, data = models });
        }

        //public async Task<string> BindingRoles()
        //{
        //    //lib
        //    List<Role> dbObjects;
        //    List<RolePresentationStub> models;
        //    QueryRequestParameter param = QueryRequestParameter.Current;

        //    //algorithm
        //    dbObjects = await RepoRole.FindAllAsync(param.Sorts, param.Filter);
        //    models = ListMapper.MapList<Role, RolePresentationStub>(dbObjects);

        //    return JsonConvert.SerializeObject(new { data = models });
        //}

        public async Task<string> BindingRoles(params object[] args)
        {
            //lib
            List<Role> dbObjects;
            List<RolePresentationStub> models;
            QueryRequestParameter param = QueryRequestParameter.Current;

            string userId = null;

            if (param.Filter != null && param.Filter.Filters.Any())
            {
                FilterQuery filterQuery = param.Filter.Filters.FirstOrDefault(a => a.Field == Field.UserId);
                if (filterQuery != null)
                {
                    userId = filterQuery.Value.ToString();
                    param.Filter.Filters.Remove(filterQuery);
                }
            }

            dbObjects = await RepoRole.FindAllAsync(param.Sorts, param.Filter);

            models = ListMapper.MapList<Role, RolePresentationStub>(dbObjects);

            if (!string.IsNullOrEmpty(userId))
            {
                foreach (RolePresentationStub r in models)
                {
                    Role role = dbObjects.FirstOrDefault(a => a.RoleId == r.RoleId);
                    if (role != null)
                    {
                        if (role.UsersInRoles != null && role.UsersInRoles.Any())
                        {
                            if (role.UsersInRoles.Any(a => a.RoleId == r.RoleId && a.UserId == userId.ToGuid()))
                            {
                                r.HasRelation = true;
                            }
                            else
                            {
                                r.HasRelation = false;
                            }
                        }
                    }
                }
            }

            return JsonConvert.SerializeObject(new { data = models });
        }

        public async Task<string> BindingMstOrganizationInUsersInRoles(params object[] args)
        {
            List<UsersInRole> usersInRoles = new List<UsersInRole>();
            List<MstOrganization> dbObjects;
            List<OrganizationPresentationStub> models;
            QueryRequestParameter param = QueryRequestParameter.Current;

            if (param.Filter != null && param.Filter.Filters.Any())
            {
                FilterQuery filterQuery = new FilterQuery();
                FilterQuery f = param.Filter.Filters.FirstOrDefault(a => a.Field == Field.UserId);
                if (f != null)
                {
                    filterQuery.AddFilter(f);
                    param.Filter.Filters.Remove(f);
                }
                f = param.Filter.Filters.FirstOrDefault(a => a.Field == Field.RoleId);
                if (f != null)
                {
                    filterQuery.AddFilter(f);
                    param.Filter.Filters.Remove(f);
                }

                usersInRoles = await RepoUsersInRoles.FindAllAsync(filterQuery);
            }

            dbObjects = await RepoOrganization.FindAllAsync(param.Sorts, param.Filter);

            models = ListMapper.MapList<MstOrganization, OrganizationPresentationStub>(dbObjects);

            if (usersInRoles.Any() && models.Any())
            {
                foreach (OrganizationPresentationStub o in models)
                {
                    if(usersInRoles.Exists(a => a.MstOrganizationId == o.Id))
                    {
                        o.HasRelation = true;
                    }
                    else
                    {
                        o.HasRelation = false;
                    }
                }
            }

            return JsonConvert.SerializeObject(new { data = models });
        }

        #endregion
    }
}
