using Business.Abstract;
using Common.Enums;
using MvcSiteMapProvider;
using System.Threading.Tasks;
using System.Web.Mvc;
using WebUI.Areas.UserManagement.Models;
using WebUI.Controllers;
using WebUI.Infrastructure;

namespace WebUI.Areas.UserManagement.Controllers
{
    [AuthorizeUser(ModuleName = UserModule.UserManagement)]
    public partial class DashboardController : BaseController
    {
        private IUserRepository RepoUser { get; set; }
        private IRoleRepository RepoRole { get; set; }

        public DashboardController(IUserRepository repoUser, IRoleRepository repoRole)
        {
            RepoUser = repoUser;
            RepoRole = repoRole;
        }

        [MvcSiteMapNode(Title = TitleSite.UserManagement, Area = AreaSite.UserManagement, ParentKey = KeySite.Dashboard, Key = KeySite.DashboardUserManagement)]
        public override async Task<ActionResult> Index(object args = null)
        {
            //lib
            DashboardModel model = new DashboardModel();
            Task<int> totalUserTask = RepoUser.CountAsync();
            Task<int> totalRoleTask = RepoRole.CountAsync();

            //algorithm
            await Task.WhenAll(totalUserTask, totalRoleTask);

            model.TotalUser = totalUserTask.Result;
            model.TotalRole = totalRoleTask.Result;

            return await base.Index(model);
        }

    }
}
