using Business.Abstract;
using Business.Entities;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using WebUI.Areas.UserManagement.Models;
using WebUI.Controllers;
using WebUI.Infrastructure;

namespace WebUI.Areas.UserManagement.Controllers
{
    [AuthorizeUser(ModuleName = UserModule.UserManagement)]
    public partial class ModuleController : BaseController<ManageModulesViewModel>
    {
        private IModuleRepository RepoModule { get; set; }
        private IActionRepository RepoAction { get; set; }
        private IModulesInRoleRepository RepoModulesInRole { get; set; }

        #region ctors

        public ModuleController(
            IModuleRepository repoParam, 
            IActionRepository repoActionParam, 
            IModulesInRoleRepository repoModulesInRole
        )
        {
            RepoModule = repoParam;
            RepoAction = repoActionParam;
            RepoModulesInRole = repoModulesInRole;
        }

        #endregion

        //[MvcSiteMapNode(Title = "Manage Modules", ParentKey = "IndexSecurityGuard", Key = "ManageModules")]
        //[SiteMapTitle("Breadcrumb")]
        public virtual async Task<ActionResult> Index()
        {
            //kamus
            ManageModulesViewModel model = new ManageModulesViewModel();
            var modules = await RepoModule.FindAllAsync();
            string[] actionsArr = new string[100];

            //algo
            actionsArr = new string[modules.Count];
            for(int i =0;i<modules.Count; ++i){
                actionsArr[i] = modules[i].ModuleName;
            }
            model.Modules = new SelectList(actionsArr);
            model.ModuleList = actionsArr;
            return View(model);
        }

        #region Create Actions Methods

        [HttpGet]
        public virtual ActionResult CreateModule()
        {
            return View(new ModuleViewModel());
        }

        [HttpPost]
        public virtual ActionResult CreateModule(string moduleName, Guid? parentModuleId)
        {
            JsonResponse response = new JsonResponse();

            if (string.IsNullOrEmpty(moduleName))
            {
                response.Success = false;
                response.Message = "You must enter a module name.";
                response.CssClass = "red";

                return Json(response);
            }

            try
            {
                Module a = new Module
                {
                    ModuleId = Guid.NewGuid(),
                    ModuleName = moduleName
                };

                a.ParentModuleId = parentModuleId;
                RepoModule.Save(a);
                
                if (Request.IsAjaxRequest())
                {
                    response.Success = true;
                    response.Message = "Module created successfully!";
                    response.CssClass = "green";

                    return Json(response);
                }

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                if (Request.IsAjaxRequest())
                {
                    response.Success = false;
                    response.Message = ex.InnerException.Message;
                    response.CssClass = "red";

                    return Json(response);
                }

                ModelState.AddModelError("", ex.InnerException.Message);
            }

            return RedirectToAction("Index");
        }

        #endregion

        #region Get treeview binding
        [HttpGet]
        public virtual async Task<ActionResult> GetModuleBinding(string moduleName){
            List<Module> tempModules;
            List<Module> modules = await RepoModule.FindAllAsync();
            List<ModuleViewModel> response = new List<ModuleViewModel>();
            bool hasChildren = false;

            tempModules = modules;
            //if (moduleName == null)
            //{
            //    modules = modules.FindAll(x => x.ParentModule == null);
            //}
            //else {
            //    modules = modules.FindAll(x => x.ParentModule == moduleName);
            //}

            //for (int i=0; i < modules.Count; ++i) {
            //    hasChildren = false;
            //    int countChildren = tempModules.FindAll(x => x.ParentModule == modules[i].ModuleName).Count;
            //    if(countChildren > 0){
            //        hasChildren = true;
            //    }

            //    ModuleViewModel m = new ModuleViewModel()
            //    {
            //        ModuleName = modules[i].ModuleName,
            //        ParentModule = modules[i].ParentModule,
            //        HasChildren = hasChildren
            //    };
            //    response.Add(m);
            //}

            return Json(response.ToArray(), JsonRequestBehavior.AllowGet);
        } 
        

        #endregion

        #region add Action to Module

        /// <summary>
        /// Return two lists:
        ///   1)  a list of Actions not in module
        ///   2)  a list of Actions in module
        /// </summary>
        /// <param name="moduleName"></param>
        /// <returns></returns>
        /// 
        //[MvcSiteMapNode(Title = "Add Action to Modules", ParentKey = "ManageModules", Key = "AddActiontoModules")]
        //[SiteMapTitle("Breadcrumb")]
        [HttpGet]
        public virtual async Task<ActionResult> AddAction(string moduleName)
        {
            if (string.IsNullOrEmpty(moduleName))
            {
                return RedirectToAction("Index");
            }

            AddActionsToModuleViewModel model = new AddActionsToModuleViewModel
            {
                ModuleName = moduleName,
                GUID = RepoModule.FindByName(moduleName).ModuleId.ToString()
            };

            var taskActionRepo = RepoAction.FindAllAsync();
            var taskModuleRepo = RepoModule.GetActionsInModuleAsync(moduleName);
            await Task.WhenAll(taskActionRepo, taskModuleRepo);
            List<Business.Entities.Action> availableActions = taskActionRepo.Result;
            List<Business.Entities.Action> usedActions = taskModuleRepo.Result;

            //used action
            foreach (Business.Entities.Action a in usedActions)
            {
                availableActions.RemoveAll(x => x.ActionName == a.ActionName);
            }

            model.AvailableActions = new SelectList(availableActions,"Id","ActionName");
            model.AddedActions = new SelectList(usedActions, "Id", "ActionName");

            return View(model);
        }

        public virtual ActionResult GrantActionsToModule(string moduleId, string actions) {
            JsonResponse response = new JsonResponse
            {
                Messages = new List<ResponseItem>()
            };

            string[] actionIds = actions.Split(',');
            StringBuilder sb = new StringBuilder();

            ResponseItem item = null;

            foreach (string s in actionIds)
            {
                if (!string.IsNullOrWhiteSpace(s))
                {
                    try
                    {
                        RepoModule.addAction(new Guid(moduleId), new Guid(s));

                        item = new ResponseItem
                        {
                            Success = true
                        };
                        response.Message = RepoAction.FindByPrimaryKey(new Guid(s)).ActionName + " was added successfully!";
                        response.CssClass = "green";
                        response.Messages.Add(item);
                    }
                    catch (Exception ex)
                    {
                        item = new ResponseItem
                        {
                            Success = false
                        };
                        response.Success = false;
                        response.Message = ex.Message;
                        response.CssClass = "red";
                        response.Messages.Add(item);
                    }
                }
            }

            return Json(response);
        }

        public virtual ActionResult ViewModuleAction()
        {
            ViewBag.modules = RepoModule.Find();
            ViewBag.actions = RepoAction.Find();
            return PartialView();
        }

        public virtual ActionResult RevokeActionsForModule(string moduleId, string actions)
        {
            JsonResponse response = new JsonResponse
            {
                Messages = new List<ResponseItem>()
            };

            string[] actionIds = actions.Split(',');
            StringBuilder sb = new StringBuilder();

            ResponseItem item = null;

            foreach (string s in actionIds)
            {
                if (!string.IsNullOrWhiteSpace(s))
                {
                    try
                    {
                        //remove action in repo
                        RepoModule.removeAction(new Guid(moduleId), new Guid(s));

                        //remove action in ModulesInRole
                        RepoModulesInRole.RemoveAction(new Guid(moduleId), new Guid(s));

                        //remove all empty actions in ModulesInRole
                        RepoModulesInRole.DeleteByModule(new Guid(moduleId));

                        item = new ResponseItem
                        {
                            Success = true
                        };
                        response.Message = RepoAction.FindByPrimaryKey(new Guid(s)).ActionName + " was removed successfully!";
                        response.CssClass = "green";
                        response.Messages.Add(item);
                    }
                    catch (Exception ex)
                    {
                        item = new ResponseItem
                        {
                            Success = false
                        };
                        response.Success = false;
                        response.Message = ex.Message;
                        response.CssClass = "red";
                        response.Messages.Add(item);
                    }
                }
            }

            return Json(response);
        }

        #endregion

        #region Delete Roles Methods

        /// <summary>
        /// This is an Ajax method.
        /// </summary>
        /// <param name="moduleName"></param>
        /// <returns></returns>
        [HttpPost]
        public virtual ActionResult DeleteModule(string moduleName)
        {
            JsonResponse response = new JsonResponse();

            if (string.IsNullOrEmpty(moduleName))
            {
                response.Success = false;
                response.Message = "You must select a Action Module to delete.";
                response.CssClass = "red";

                return Json(response);
            }
            try
            {
                RepoModule.Delete(moduleName, false);

                response.Success = true;
                response.Message = moduleName + " was deleted successfully!";
                response.CssClass = "green";

                return Json(response);
            }
            catch (Exception ex)
            {      
                response.Success = false;
                response.Message = ex.InnerException.Message;
                response.CssClass = "red";

                return Json(response);
            }
        }

        [HttpPost]
        public ActionResult DeleteModules(string moduleName, bool throwOnPopulatedModule)
        {
            JsonResponse response = new JsonResponse
            {
                Messages = new List<ResponseItem>()
            };

            if (string.IsNullOrEmpty(moduleName))
            {
                response.Success = false;
                response.Message = "You must select at least one module.";
                return Json(response);
            }

            StringBuilder sb = new StringBuilder();

            ResponseItem item = null;

            
            if (!string.IsNullOrEmpty(moduleName))
            {
                try
                {
                    RepoModule.Delete(moduleName, throwOnPopulatedModule);//roles not yet deleted

                    item = new ResponseItem
                    {
                        Success = true,
                        Message = "Deleted this module successfully - " + moduleName,
                        CssClass = "green"
                    };
                    response.Messages.Add(item);

                    //sb.AppendLine("Deleted this role successfully - " + role + "<br />");
                }
                catch (SqlException ex)
                {
                    //sb.AppendLine(role + " - " + ex.Message + "<br />");

                    item = new ResponseItem
                    {
                        Success = false,
                        Message = ex.InnerException.Message,
                        CssClass = "yellow"
                    };
                    response.Messages.Add(item);
                }
            }
            
            response.Success = true;
            response.Message = sb.ToString();

            return Json(response);
        }

        #endregion

        #region Get Actions In Module methods

        /// <summary>
        /// This is an Ajax method that populates the 
        /// Actions drop down list.
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> GetAllModule()
        {
            var list = await RepoModule.FindAllAsync();

            List<SelectObject> selectList = new List<SelectObject>
            {
                new SelectObject() { caption = "[None]", value = "[None]" }
            };
            foreach (Module item in list)
            {
                selectList.Add(new SelectObject() { caption = item.ModuleName, value = item.ModuleName.ToString() });
            }
            
            return Json(selectList, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> GetListModule()
        {
            List<Module> tempModules = new List<Module>();
            List<ModuleViewModel> result = new List<ModuleViewModel>();
            List<Module> modules = await RepoModule.FindAllAsync();
            //modules = modules.FindAll(x => x.ParentModule == null);

            //foreach (Module m in modules)
            //{
            //    tempModules.Add(m);
            //    moduleRepo.GetAllChildrenInModule(m.ModuleName, ref tempModules, 0);
            //}

            //foreach (Module m in tempModules)
            //{
            //    result.Add(new ModuleViewModel(m));
            //}

            result = new ModuleViewModel().MapRecursive(modules);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetActionsInModule(string moduleName)
        {
            var list = RepoModule.GetActionsInModule(moduleName);

            return Json(list, JsonRequestBehavior.AllowGet);
        }


        #endregion
    }
}
