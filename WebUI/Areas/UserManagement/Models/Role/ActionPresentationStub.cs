﻿using Business.Entities;
using System;
using System.Linq;
using WebUI.Models;

namespace WebUI.Areas.UserManagement.Models
{
    public class ActionPresentationStub : BasePresentationStub<Business.Entities.Action, ActionPresentationStub>
    {
        public Guid ActionId { get; set; }
        public string ActionName { get; set; }
        public bool IsChoosen { get; set; }

        public ActionPresentationStub() : base()
        {
        }

        public ActionPresentationStub(Business.Entities.Action dbObject) : base(dbObject)
        {
        }

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
        }
    }
}
