﻿using Business.Entities;
using Business.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Areas.UserManagement.Models
{
    public class AssignRevokeModulesFormStub
    {
        public Guid ApplicationId { get; set; }

        [Display(Name = "Application Name")]
        public string ApplicationName { get; set; }

        public Guid RoleId { get; set; }
		
        [Display(Name = "Role Name")]
        public string RoleName { get; set; }

        public List<ModulesInRolesFormStub> MIRFormStubs { get; set; }

        public AssignRevokeModulesFormStub()
        {

        }

        public AssignRevokeModulesFormStub(Role dbObject) : this()
        {
            ApplicationId = dbObject.ApplicationId;
            ApplicationName = dbObject.Application.ApplicationName;
            RoleId = dbObject.RoleId;
            RoleName = dbObject.RoleName;
        }
    }
}
