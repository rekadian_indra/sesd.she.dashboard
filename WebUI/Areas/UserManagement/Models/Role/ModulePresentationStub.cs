﻿using Business.Entities;
using System;
using System.Linq;
using WebUI.Models;

namespace WebUI.Areas.UserManagement.Models
{
    public class ModulePresentationStub : BasePresentationStub<Module, ModulePresentationStub>
    {
        public Guid ModuleId { get; set; }
        public string ModuleName { get; set; }
        public bool IsInRole { get; set; }

        public ModulePresentationStub() : base()
        {
        }

        public ModulePresentationStub(Module dbObject) : base(dbObject)
        {
            IsInRole = false;
        }

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
        }
    }
}
