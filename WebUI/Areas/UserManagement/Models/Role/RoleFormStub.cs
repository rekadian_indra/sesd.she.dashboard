﻿using Business.Entities;
using Business.Infrastructure;
using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Areas.UserManagement.Models
{
    public class RoleFormStub : BaseFormStub<Role, RoleFormStub>
    {
        [Display(Name = "Application Name")]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public Guid ApplicationId { get; set; }

        public Guid RoleId { get; set; }
		
        [Display(Name = "Role Name")]
        [RegularExpression(@"([a-zA-Z-._0-9]*)", ErrorMessageResourceName = GlobalErrorField.Symbol, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Remote(ActionSite.RoleNameExists, ControllerSite.Role, AreaSite.UserManagement, AdditionalFields = "CurrentRoleName,ApplicationId", ErrorMessageResourceName = GlobalErrorField.Unique, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string RoleName { get; set; }
		
        public string Description { get; set; }

        public string CurrentRoleName { get; set; }

        public RoleFormStub() : base()
        {
            RoleId = Guid.NewGuid();
        }

        public RoleFormStub(Role dbObject) : base(dbObject)
        {
            //TODO: Manual mapping object
            CurrentRoleName = dbObject.RoleName;
        }

        public override void MapDbObject(Role dbObject)
        {
            base.MapDbObject(dbObject);

            //TODO: Manual mapping object here           
        }

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
        }
    }
}
