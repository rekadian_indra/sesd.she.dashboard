﻿using Business.Entities;
using Business.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Areas.UserManagement.Models
{
    public class ModulesInRolesFormStub : BaseFormStub<ModulesInRole, ModulesInRolesFormStub>
    {
        public Guid Id { get; set; }
        public Guid RoleId { get; set; }
        public Guid ModuleId { get; set; }
        public List<Guid> ActionIds { get; set; }

        public ModulesInRolesFormStub() : base()
        {
        }

        public ModulesInRolesFormStub(ModulesInRole dbObject) : base(dbObject)
        {
            //TODO: Manual mapping object
        }

        public override void MapDbObject(ModulesInRole dbObject)
        {
            base.MapDbObject(dbObject);
            //TODO: Manual mapping object here
        }

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
        }
    }
}
