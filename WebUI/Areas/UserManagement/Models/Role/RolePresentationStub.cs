﻿using Business.Entities;
using System;
using System.Linq;
using WebUI.Models;

namespace WebUI.Areas.UserManagement.Models
{
    public class RolePresentationStub : BasePresentationStub<Role, RolePresentationStub>
    {
        public Guid ApplicationId { get; set; }
        public string ApplicationName { get; set; }
        public Guid RoleId { get; set; }
        public string RoleName { get; set; }
        public string Description { get; set; }
        public bool HasRelation { get; set; }

        public RolePresentationStub() : base()
        {
        }

        public RolePresentationStub(Role dbObject) : base(dbObject)
        {
            ApplicationName = dbObject.Application.ApplicationName;
            //HasRelation = dbObject.Users.Any();
            HasRelation = dbObject.UsersInRoles.Any();
        }

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
        }
    }
}
