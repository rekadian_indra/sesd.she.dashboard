﻿using System.Web.Mvc;

namespace WebUI.Areas.UserManagement.Models
{
    public class ManageActionsViewModel
    {
        public SelectList Actions { get; set; }
        public string[] ActionList { get; set; }
    }
}
