﻿using System.Collections.Generic;

namespace WebUI.Areas.UserManagement.Models
{
    public class FingerPrintParam
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public List<FingerPrintParam> listUser { get; set; }
    }
}