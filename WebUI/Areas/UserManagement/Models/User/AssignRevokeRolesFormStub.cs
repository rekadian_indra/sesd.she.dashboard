﻿using Business.Entities;
using Business.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Areas.UserManagement.Models
{
    public class AssignRevokeRolesFormStub
    {
        public Guid UserId { get; set; }

        [Display(Name = "User Name")]
        public string UserName { get; set; }

        public List<UsersInRolesFormStub> UIRFormStubs { get; set; }

        public AssignRevokeRolesFormStub()
        {

        }

        public AssignRevokeRolesFormStub(User dbObject) : this()
        {
            UserId = dbObject.UserId;
            UserName = dbObject.UserName;
        }
    }
}
