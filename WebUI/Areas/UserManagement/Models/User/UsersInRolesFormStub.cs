﻿using Business.Entities;
using Business.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Areas.UserManagement.Models
{
    public class UsersInRolesFormStub : BaseFormStub<UsersInRole, UsersInRolesFormStub>
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public Guid RoleId { get; set; }
        public int MstOrganizationId { get; set; }

        public UsersInRolesFormStub() : base()
        {
        }

        public UsersInRolesFormStub(UsersInRole dbObject) : base(dbObject)
        {
            //TODO: Manual mapping object
        }

        public override void MapDbObject(UsersInRole dbObject)
        {
            Id = Guid.NewGuid();
            base.MapDbObject(dbObject);
            //TODO: Manual mapping object here
        }

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
        }
    }
}
