﻿using Business.Entities;
using Business.Extension;
using System;
using WebUI.Models;

namespace WebUI.Areas.UserManagement.Models
{
    public class UserPresentationStub : BasePresentationStub<User, UserPresentationStub>
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime LastLoginDate { get; set; }
        public int? MstOrganizationId { get; set; }
        public string MstOrganizationName { get; set; }

        public UserPresentationStub() : base()
        {
        }

        public UserPresentationStub(User dbObject) : base(dbObject)
        {
            //TODO: Manual mapping object here
            Membership membership = dbObject.Membership;

            if (membership != null)
            {
                Email = membership.Email;
                CreateDate = membership.CreateDate.ToLocalDateTime();
                LastLoginDate = membership.LastLoginDate.ToLocalDateTime();
                MstOrganizationId = membership.MstOrganizationId;
            }
        }

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
        }
    }
}