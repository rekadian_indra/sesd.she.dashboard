using Business.Entities;
using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Areas.UserManagement.Models
{
    public class UserFormStub
    {
        public Guid UserId { get; set; }

        [Display(Name = "User ID")]
        //[RegularExpression(@"([a-zA-Z-._0-9]*)", ErrorMessageResourceName = GlobalErrorField.Symbol, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        //[Remote(ActionSite.UserNameExists, ControllerSite.User, AreaSite.UserManagement, AdditionalFields = "CurrentUserName", ErrorMessageResourceName = GlobalErrorField.Unique, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string UserName { get; set; }

        [EmailAddress(ErrorMessageResourceName = GlobalErrorField.EmailType, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Remote(ActionSite.EmailExists, ControllerSite.User, AreaSite.UserManagement, AdditionalFields = "CurrentEmail", ErrorMessageResourceName = GlobalErrorField.Unique, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string Email { get; set; }

        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        [StringLength(100, MinimumLength = 6, ErrorMessageResourceName = GlobalErrorField.MinStringLength, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [RequiredIf("IsNewUser", true, ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string Password { get; set; }

        [Display(Name = "Confirm Password")]
        [DataType(DataType.Password)]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessageResourceName = GlobalErrorField.Compare, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string ConfirmPassword { get; set; }

        [Display(Name = "Full Name")]
        [StringLength(100, ErrorMessageResourceName = GlobalErrorField.MaxStringLength, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string FullName { get; set; }

        public string CurrentUserName { get; set; }
        
        public string CurrentEmail { get; set; }

        public string SecretQuestion { get; set; }

        public string SecretAnswer { get; set; }

        public bool Approve { get; set; }

        public bool RequireSecretQuestionAndAnswer { get; set; }

        public bool IsNewUser { get; set; }

        [Display(Name = "Organization")]
        public int? MstOrganizationId { get; set; }

        public UserFormStub()
        {
        }

        public UserFormStub(User dbObject)
        {
            Membership membership = dbObject.Membership;

            if (membership != null)
            {
                UserProfile profile = UserProfile.GetUserProfile(dbObject.UserName);

                UserId = dbObject.UserId;
                UserName = dbObject.UserName;
                Email = membership.Email;
                CurrentUserName = dbObject.UserName;
                CurrentEmail = membership.Email;
                SecretQuestion = membership.PasswordQuestion;
                SecretAnswer = membership.PasswordAnswer;
                Approve = membership.IsApproved;
                FullName = profile?.FullName;
                MstOrganizationId = membership.MstOrganizationId;
            }
        }
    }
}
