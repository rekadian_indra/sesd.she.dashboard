using System.Web.Security;

namespace WebUI.Areas.UserManagement.Models
{
    public class UserViewModel
    {
        public MembershipUser User { get; set; }
        public bool RequiresSecretQuestionAndAnswer { get; set; }
        public string[] Roles { get; set; }
        public string userName { get; set; }
    }
}
