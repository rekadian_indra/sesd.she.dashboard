﻿namespace WebUI.Areas.UserManagement.Models
{
    public class SelectObject
    {
        public string value { get; set; }
        public string caption { get; set; }
    }
}
