﻿using Business.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebUI.Models;

namespace WebUI.Areas.DataMaster.Models.Locations
{
    public class LocationPresentationStub : BasePresentationStub<MstLocation, LocationPresentationStub>
    {
        #region "Properties"
        public int Id { get; set; }
        public int SiteId { get; set; }
        public string Name { get; set; }
        #endregion

        #region "Constructor"
        public LocationPresentationStub() : base()
        {

        }

        public LocationPresentationStub(MstLocation dbObject) : base(dbObject)
        {

        }
        #endregion

        #region "Helper"
        protected override void Init()
        {
        }
        #endregion
    }
}