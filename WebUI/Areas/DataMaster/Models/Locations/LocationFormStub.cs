﻿using Business.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Areas.DataMaster.Models.Locations
{
    public class LocationFormStub : BaseFormStub<MstLocation, LocationFormStub>
    {
        #region "Properties"
        public int Id { get; set; }

        public int SiteId { get; set; }

        [DisplayName("Location Name")]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string Name { get; set; }

        [DisplayName("Site")]
        public string SiteName { get; set; }
        #endregion

        #region "Constructor"
        public LocationFormStub() : base()
        {

        }

        public LocationFormStub(MstLocation dbObject) : base(dbObject)
        {
            SiteName = dbObject.MstSite.Name;
        }
        #endregion

        #region "Helper"
        public override void MapDbObject(MstLocation dbObject)
        {
            base.MapDbObject(dbObject);
        }

        protected override void Init()
        {
        }
        #endregion
    }
}