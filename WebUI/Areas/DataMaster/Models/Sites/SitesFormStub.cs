﻿using Business.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using WebUI.Infrastructure;
using WebUI.Models;


namespace WebUI.Areas.DataMaster.Models.Sites
{
    public class SitesFormStub : BaseFormStub<MstSite, SitesFormStub>
    {
        #region "Properties"
        public int Id { get; set; }

        [DisplayName("Region ")]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public int MstRegionId { get; set; }

        [DisplayName("Name")]
        [StringLength(250, ErrorMessageResourceName = GlobalErrorField.MaxStringLength, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string Name { get; set; }


        #endregion

        #region Constructor

        public SitesFormStub() : base()
        {
        }

        public SitesFormStub(MstSite dbObject) : base(dbObject)
        {
            //TODO: Manual mapping object
        }

        #endregion

        #region Helper

        public override void MapDbObject(MstSite dbObject)
        {
            base.MapDbObject(dbObject);

            //TODO: Manual mapping object here           
        }

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
        }

        #endregion

    }
}