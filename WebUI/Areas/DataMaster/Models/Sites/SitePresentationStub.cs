﻿using Business.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebUI.Models;

namespace WebUI.Areas.DataMaster.Models.Sites
{
    public class SitePresentationStub : BasePresentationStub<MstSite, SitePresentationStub>
    {
        #region Properties

        public int Id { get; set; }
        public int MstRegionId { get; set; }
        public string RegionName { get; set; }
        public string Name { get; set; }
        public bool HasRelation { get; set; }
        #endregion

        #region Constructor

        public SitePresentationStub() : base()
        {
        }

        public SitePresentationStub(MstSite dbObject) : base(dbObject)
        {
            //TODO: Manual mapping object here
            RegionName = dbObject.MstRegion.Name;
            if (dbObject.MstOrganizations.Count > 0)
            {
                HasRelation = true;
            }
            else
            {
                HasRelation = false;
            }
        }

        #endregion

        #region Helper

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
        }

        #endregion
    }
}