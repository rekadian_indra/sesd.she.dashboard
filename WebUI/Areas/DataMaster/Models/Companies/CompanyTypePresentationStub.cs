﻿using Business.Entities;
using System.Linq;
using WebUI.Models;

namespace WebUI.Areas.DataMaster.Models.Companies
{
    public class CompanyTypePresentationStub : BasePresentationStub<MstCompanyType, CompanyTypePresentationStub>
    {
        #region "Properties"
        public int Id { get; set; }
        public string Name { get; set; }
        public bool HasRelation { get; set; }


        #endregion

        #region "Consturctor"
        public CompanyTypePresentationStub() : base()
        {

        }

        public CompanyTypePresentationStub(MstCompanyType dbObject) : base(dbObject)
        {
            if (dbObject.MstCompanies.HasValue() || dbObject.MstVehicleOwnerships.HasValue())
                HasRelation = true;
        }
        #endregion

        #region "Helper"
        protected override void Init()
        {
        }
        #endregion
    }
}