﻿using Business.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebUI.Models;

namespace WebUI.Areas.DataMaster.Models.Companies
{
    public class CompanyPresentationStub : BasePresentationStub<MstCompany, CompanyPresentationStub>
    {
        #region "Properties"
        public int Id { get; set; }
        public int MstCompanyTypeId { get; set; }
        public string Name { get; set; }
        public string CompanyTypeName { get; set; }
        public bool HasRelation { get; set; }
        #endregion

        #region "Consturctor"
        public CompanyPresentationStub() : base()
        {

        }

        public CompanyPresentationStub(MstCompany dbObject) : base(dbObject)
        {
            CompanyTypeName = dbObject.MstCompanyType.Name;
            if (dbObject.MstVehicles.HasValue())
                HasRelation = true;
        }
        #endregion

        #region "Helper"
        protected override void Init()
        {
        }
        #endregion
    }
}