﻿using Business.Entities;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Areas.DataMaster.Models.Companies
{
    public class CompanyTypeFormStub : BaseFormStub<MstCompanyType, CompanyTypeFormStub>
    {
        #region "Properties"
        public int Id { get; set; }

        [DisplayName("Type Name")]
        [StringLength(128, ErrorMessageResourceName = GlobalErrorField.MaxStringLength, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Remote(ActionSite.NameExists, ControllerSite.CompanyType, AdditionalFields = nameof(CurrentName), ErrorMessageResourceName = GlobalErrorField.Unique, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string Name { get; set; }

        public string CurrentName { get; set; }
        #endregion

        #region "Consturctor"
        public CompanyTypeFormStub() : base()
        {

        }

        public CompanyTypeFormStub(MstCompanyType dbObject) : base(dbObject)
        {
            CurrentName = dbObject.Name;
        }
        #endregion

        #region "Helper"
        public override void MapDbObject(MstCompanyType dbObject)
        {
            base.MapDbObject(dbObject);
        }

        protected override void Init()
        {
        }
        #endregion
    }
}