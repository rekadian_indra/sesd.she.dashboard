﻿using Business.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Areas.DataMaster.Models.Regions
{
    public class RegionFormStub : BaseFormStub<MstRegion, RegionFormStub>
    {
        #region "Properties"
        public int Id { get; set; }              

        [DisplayName("Name")]
        [StringLength(250, ErrorMessageResourceName = GlobalErrorField.MaxStringLength, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string Name { get; set; }

       
        #endregion

        #region Constructor

        public RegionFormStub() : base()
        {
        }

        public RegionFormStub(MstRegion dbObject) : base(dbObject)
        {
            //TODO: Manual mapping object
        }

        #endregion

        #region Helper

        public override void MapDbObject(MstRegion dbObject)
        {
            base.MapDbObject(dbObject);

            //TODO: Manual mapping object here           
        }

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
        }

        #endregion

    }
}