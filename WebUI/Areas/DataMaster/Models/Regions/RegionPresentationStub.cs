﻿using Business.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebUI.Models;

namespace WebUI.Areas.DataMaster.Models.Regions
{
    public class RegionPresentationStub : BasePresentationStub<MstRegion, RegionPresentationStub>
    {
        #region Properties

        public int Id { get; set; }
        public string Name { get; set; }
        public bool HasRelation { get; set; }
        #endregion

        #region Constructor

        public RegionPresentationStub() : base()
        {
        }

        public RegionPresentationStub(MstRegion dbObject) : base(dbObject)
        {
            //TODO: Manual mapping object here
           
            if (dbObject.MstSites.Count > 0)
            {
                HasRelation = true;
            }
            else
            {
                HasRelation = false;
            }
        }

        #endregion

        #region Helper

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
        }

        #endregion
    }
}