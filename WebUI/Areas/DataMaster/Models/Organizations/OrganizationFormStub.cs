﻿using Business.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Areas.DataMaster.Models.Organizations
{
    public class OrganizationFormStub : BaseFormStub<MstOrganization, OrganizationFormStub>
    {
        #region "Properties"       
        public int Id { get; set; }

        [DisplayName("Region ")]
        public Nullable<int> MstRegionId { get; set; }

        [DisplayName("Site ")]
        public Nullable<int> MstSiteId { get; set; }

        [DisplayName("Parent ")]
        
        public Nullable<int> ParentId { get; set; }

        [DisplayName("Name")]
        [StringLength(250, ErrorMessageResourceName = GlobalErrorField.MaxStringLength, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string Name { get; set; }


        #endregion

        #region Constructor

        public OrganizationFormStub() : base()
        {
        }

        public OrganizationFormStub(MstOrganization dbObject) : base(dbObject)
        {
            //TODO: Manual mapping object
            if (dbObject.MstSite != null)
            {
                MstRegionId = dbObject.MstSite.MstRegionId;
            }
            
        }

        #endregion

        #region Helper

        public override void MapDbObject(MstOrganization dbObject)
        {
            base.MapDbObject(dbObject);

            //TODO: Manual mapping object here           
        }

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
        }

        #endregion

    }
}