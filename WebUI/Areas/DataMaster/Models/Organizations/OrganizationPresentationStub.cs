﻿using Business.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebUI.Models;

namespace WebUI.Areas.DataMaster.Models.Organizations
{
    public class OrganizationPresentationStub : BasePresentationStub<MstOrganization, OrganizationPresentationStub>
    {
        #region Properties

        public int Id { get; set; }
        public string Name { get; set; }
        public Nullable<int> ParentId { get; set; }
        public string ParentName { get; set; }
        public Nullable<int> MstRegionId { get; set; }
        public Nullable<int> MstSiteId { get; set; }
        public string SiteName { get; set; }
        public bool HasRelation { get; set; }
        #endregion

        #region Constructor

        public OrganizationPresentationStub() : base()
        {
        }

        public OrganizationPresentationStub(MstOrganization dbObject) : base(dbObject)
        {
            //TODO: Manual mapping object here
           
            if (dbObject.MstSite != null)
            {
                MstRegionId = dbObject.MstSite.MstRegionId;
                SiteName = dbObject.MstSite.Name;
            }
           
            if (dbObject.MstOrganization2 != null)
            {
                ParentName = dbObject.MstOrganization2.Name;
            }
            else
            {
                ParentName = " - ";
            }
            
            HasRelation = false;
        }

        #endregion

        #region Helper

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
        }

        #endregion
    }
}