﻿using Business.Entities;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Areas.DataMaster.Models.Vehicles
{
    public class VehicleFormStub : BaseFormStub<MstVehicle, VehicleFormStub>
    {
        #region "Properties"
        public int Id { get; set; }
        public int MstVehicleTypeId { get; set; }

        [DisplayName("License Plate Header")]
        [StringLength(4, ErrorMessageResourceName = GlobalErrorField.MaxStringLength, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        //[Remote(ActionSite.LicensePlateHeaderExists, ControllerSite.Vehicle, AdditionalFields = nameof(CurrentLicensePlateHeader), ErrorMessageResourceName = GlobalErrorField.Unique, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string LicensePlateHeader { get; set; }

        [DisplayName("License Plate Number")]
        [StringLength(8, ErrorMessageResourceName = GlobalErrorField.MaxStringLength, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Remote(ActionSite.LicensePlateNumberExists, ControllerSite.Vehicle, AdditionalFields = nameof(CurrentLicensePlateNumber), ErrorMessageResourceName = GlobalErrorField.Unique, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string LicensePlateNumber { get; set; }

        [DisplayName("License Plate Tail")]
        [StringLength(4, ErrorMessageResourceName = GlobalErrorField.MaxStringLength, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Remote(ActionSite.LicensePlateTailExists, ControllerSite.Vehicle, AdditionalFields = nameof(CurrentLicensePlateTail), ErrorMessageResourceName = GlobalErrorField.Unique, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string LicensePlateTail { get; set; }

        [DisplayName("Ownership")]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public int MstVehicleOwnershipId { get; set; }

        [DisplayName("Company")]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public int OwnerMstCompanyId { get; set; }

        [DisplayName("Brand")]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public int MstVehicleBrandId { get; set; }

        [DisplayName("Year")]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public int VehicleYear { get; set; }

        [DisplayName("Vehicle Type")]
        public string VehicleTypeName { get; set; }

        //public string CurrentLicensePlateHeader { get; set; }
        public string CurrentLicensePlateNumber { get; set; }
        public string CurrentLicensePlateTail { get; set; }
        #endregion

        #region "Contructor"
        public VehicleFormStub() : base()
        {

        }

        public VehicleFormStub(MstVehicle dbObject) : base(dbObject)
        {
            VehicleTypeName = dbObject.MstVehicleType.Name;
            //CurrentLicensePlateHeader = dbObject.LicensePlateHeader;
            CurrentLicensePlateNumber = dbObject.LicensePlateNumber;
            CurrentLicensePlateTail = dbObject.LicensePlateTail;
        }
        #endregion

        #region "Helper"
        public override void MapDbObject(MstVehicle dbObject)
        {
            base.MapDbObject(dbObject);
        }

        protected override void Init()
        {
        }
        #endregion
    }
}