﻿using Business.Entities;
using WebUI.Models;

namespace WebUI.Areas.DataMaster.Models.Vehicles
{
    public class VehiclePresentationStub : BasePresentationStub<MstVehicle, VehiclePresentationStub>
    {
        #region "Properties"
        public int Id { get; set; }
        public int MstVehicleTypeId { get; set; }
        public string LicensePlateHeader { get; set; }
        public string LicensePlateNumber { get; set; }
        public string LicensePlateTail { get; set; }
        public int MstVehicleOwnershipId { get; set; }
        public int OwnerMstCompanyId { get; set; }
        public int MstVehicleBrandId { get; set; }
        public int VehicleYear { get; set; }

        public string VehicleTypeName { get; set; }
        public string VehicleBrandName { get; set; }
        public string VehicleOwnershipName { get; set; }
        public string CompanyName { get; set; }
        #endregion

        #region "Contructor"
        public VehiclePresentationStub() : base()
        {

        }

        public VehiclePresentationStub(MstVehicle dbObject) : base(dbObject)
        {
            VehicleTypeName = dbObject.MstVehicleType.Name;
            VehicleBrandName = dbObject.MstVehicleBrand.Name;
            VehicleOwnershipName = dbObject.MstVehicleOwnership.Name;
            CompanyName = dbObject.MstCompany.Name;
        }
        #endregion

        #region "Helper"
        protected override void Init()
        {
        }
        #endregion
    }
}