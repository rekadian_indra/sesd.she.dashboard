﻿using Business.Entities;
using System.Linq;
using WebUI.Models;

namespace WebUI.Areas.DataMaster.Models.Vehicles
{
    public class VehicleOwnershipPresentationStub : BasePresentationStub<MstVehicleOwnership, VehicleOwnershipPresentationStub>
    {
        #region "Properties"
        public int Id { get; set; }
        public int MstCompanyTypeId { get; set; }
        public string Name { get; set; }
        public string CompanyTypeName { get; set; }
        public bool HasRelation { get; set; }
        #endregion

        #region "Contructor"
        public VehicleOwnershipPresentationStub() : base()
        {

        }

        public VehicleOwnershipPresentationStub(MstVehicleOwnership dbObject) : base(dbObject)
        {
            CompanyTypeName = dbObject.MstCompanyType.Name;
            if (dbObject.MstVehicles.HasValue())
                HasRelation = true;
        }
        #endregion

        #region "Helper"
        protected override void Init()
        {
        }
        #endregion
    }
}