﻿using Business.Entities;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Areas.DataMaster.Models.Vehicles
{
    public class VehicleTypeFormStub : BaseFormStub<MstVehicleType, VehicleTypeFormStub>
    {
        #region "Properties"
        public int Id { get; set; }

        [DisplayName("Type Name")]
        [StringLength(32, ErrorMessageResourceName = GlobalErrorField.MaxStringLength, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Remote(ActionSite.NameExists, ControllerSite.VehicleType, AdditionalFields = nameof(CurrentName), ErrorMessageResourceName = GlobalErrorField.Unique, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string Name { get; set; }

        public string CurrentName { get; set; }
        #endregion

        #region "Contructor"
        public VehicleTypeFormStub() : base()
        {

        }

        public VehicleTypeFormStub(MstVehicleType dbObject) : base(dbObject)
        {
            CurrentName = dbObject.Name;
        }
        #endregion

        #region "Helper"
        public override void MapDbObject(MstVehicleType dbObject)
        {
            base.MapDbObject(dbObject);
        }

        protected override void Init()
        {
        }
        #endregion
    }
}