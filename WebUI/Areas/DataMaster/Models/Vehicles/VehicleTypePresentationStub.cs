﻿using Business.Entities;
using System.Linq;
using WebUI.Models;

namespace WebUI.Areas.DataMaster.Models.Vehicles
{
    public class VehicleTypePresentationStub : BasePresentationStub<MstVehicleType, VehicleTypePresentationStub>
    {
        #region "Properties"
        public int Id { get; set; }
        public string Name { get; set; }
        public bool HasRelation { get; set; }
        #endregion

        #region "Contructor"
        public VehicleTypePresentationStub() : base()
        {

        }

        public VehicleTypePresentationStub(MstVehicleType dbObject) : base(dbObject)
        {
            if (dbObject.MstVehicles.HasValue())
                HasRelation = true;
        }
        #endregion

        #region "Helper"
        protected override void Init()
        {
        }
        #endregion
    }
}