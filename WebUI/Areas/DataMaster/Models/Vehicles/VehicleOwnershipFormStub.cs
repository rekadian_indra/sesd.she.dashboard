﻿using Business.Entities;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Areas.DataMaster.Models.Vehicles
{
    public class VehicleOwnershipFormStub : BaseFormStub<MstVehicleOwnership, VehicleOwnershipFormStub>
    {
        #region "Properties"
        public int Id { get; set; }
        public int MstCompanyTypeId { get; set; }

        [DisplayName("Ownership Name")]
        [StringLength(128, ErrorMessageResourceName = GlobalErrorField.MaxStringLength, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Remote(ActionSite.NameExists, ControllerSite.VehicleOwnership, AdditionalFields = nameof(CurrentName), ErrorMessageResourceName = GlobalErrorField.Unique, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string Name { get; set; }

        [DisplayName("Company Type")]
        public string CompanyTypeName { get; set; }
        public string CurrentName { get; set; }
        #endregion

        #region "Contructor"
        public VehicleOwnershipFormStub() : base()
        {

        }

        public VehicleOwnershipFormStub(MstVehicleOwnership dbObject) : base(dbObject)
        {
            CompanyTypeName = dbObject.MstCompanyType.Name;
            CurrentName = dbObject.Name;
        }
        #endregion

        #region "Helper"
        public override void MapDbObject(MstVehicleOwnership dbObject)
        {
            base.MapDbObject(dbObject);
        }

        protected override void Init()
        {
        }
        #endregion
    }
}