﻿using Business.Entities;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Areas.DataMaster.Models.Vehicles
{
    public class VehicleBrandFormStub : BaseFormStub<MstVehicleBrand, VehicleBrandFormStub>
    {
        #region "Properties"
        public int Id { get; set; }

        [DisplayName("Brand Name")]
        [StringLength(128, ErrorMessageResourceName = GlobalErrorField.MaxStringLength, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Remote(ActionSite.NameExists, ControllerSite.VehicleBrand, AdditionalFields = nameof(CurrentName), ErrorMessageResourceName = GlobalErrorField.Unique, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string Name { get; set; }

        public string CurrentName { get; set; }
        #endregion

        #region "Contructor"
        public VehicleBrandFormStub() : base()
        {

        }

        public VehicleBrandFormStub(MstVehicleBrand dbObject) : base(dbObject)
        {
            CurrentName = dbObject.Name;
        }
        #endregion

        #region "Helper"
        public override void MapDbObject(MstVehicleBrand dbObject)
        {
            base.MapDbObject(dbObject);
        }

        protected override void Init()
        {
        }
        #endregion
    }
}