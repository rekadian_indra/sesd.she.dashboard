﻿using Business.Abstract;
using Business.Entities;
using Business.Infrastructure;
using Common.Enums;
using MvcSiteMapProvider;
using MvcSiteMapProvider.Web.Mvc.Filters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebUI.Areas.DataMaster.Models.Organizations;
using WebUI.Controllers;
using WebUI.Extension;
using WebUI.Infrastructure;
using WebUI.Models;


namespace WebUI.Areas.DataMaster.Controllers
{
    [AuthorizeUser(Roles = new object[] { UserRole.Administrator })]
    public class OrganizationController : BaseController<OrganizationFormStub>
    {
        public OrganizationController
        (
            IRegionRepository repoRegion,
            ISiteRepository repoSite,
            IOrganizationRepository repoOrganization
        )
        {
            RepoRegion = repoRegion;
            RepoSite = repoSite;
            RepoOrganization = repoOrganization;
        }

        #region "Main Action"
        [MvcSiteMapNode(Title = TitleSite.Site, Area = AreaSite.DataMaster, ParentKey = KeySite.Dashboard, Key = KeySite.IndexOrganization)]
        public override async Task<ActionResult> Index(object args = null)
        {
            return await base.Index();
        }

        [MvcSiteMapNode(Title = TitleSite.Create, ParentKey = KeySite.IndexOrganization, Key = KeySite.CreateOrganization)]
        public override async Task<ActionResult> Create()
        {
            OrganizationFormStub model = new OrganizationFormStub();

            await Task.Delay(0);

            return View("Form", model);
        }

        [HttpPost]
        public override async Task<ActionResult> Create(OrganizationFormStub model)
        {
            if (ModelState.IsValid)
            {
                MstOrganization dbObject = new MstOrganization();

                model.MapDbObject(dbObject);
                await RepoOrganization.SaveAsync(dbObject);

                //message
                string template = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.CreateSuccess).ToString();
                this.SetMessage(model.Name, template);
                return RedirectToAction(ActionSite.Index);
            }
            else
            {
                return View("Form", model);
            }
        }

        [SiteMapTitle(TitleSite.Breadcrumb)]
        [MvcSiteMapNode(Title = TitleSite.Edit, ParentKey = KeySite.IndexOrganization, Key = KeySite.EditOrganization)]
        public override async Task<ActionResult> Edit(params object[] id)
        {
            int primaryKey = int.Parse(id.FirstOrDefault().ToString());
            MstOrganization dbObject = await RepoOrganization.FindByPrimaryKeyAsync(primaryKey);
            OrganizationFormStub model = new OrganizationFormStub(dbObject);

            ViewBag.Breadcrumb = model.Name;

            return View("Form", model);
        }

        [HttpPost]
        public override async Task<ActionResult> Edit(OrganizationFormStub model)
        {
            if (ModelState.IsValid)
            {
                MstOrganization dbObject = await RepoOrganization.FindByPrimaryKeyAsync(model.Id);

                model.MapDbObject(dbObject);

                await RepoOrganization.SaveAsync(dbObject);

                //message
                string template = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.CreateSuccess).ToString();
                this.SetMessage(model.Name, template);

                return RedirectToAction(ActionSite.Index);
            }
            else
            {
                ViewBag.Breadcrumb = model.Name;

                return View("Form", model);
            }
        }

        [HttpPost]
        public override async Task<ActionResult> Delete(params object[] id)
        {
            ResponseModel response = new ResponseModel(true);
            int primaryKey = int.Parse(id.FirstOrDefault().ToString());
            MstOrganization dbObject = await RepoOrganization.FindByPrimaryKeyAsync(primaryKey);

            if (!(await RepoOrganization.DeleteAsync(dbObject)))
            {
                string message = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.DeleteFailed).ToString();
                response.SetFail(message);
            }

            return Json(response);
        }
        #endregion

        #region "Binding"
        public override async Task<string> Binding(params object[] args)
        {
            //lib
            int count;
            List<MstOrganization> dbObjects;
            List<OrganizationPresentationStub> models;
            Task<List<MstOrganization>> dbObjectsTask = null;
            Task<int> countTask = null;
            QueryRequestParameter param = QueryRequestParameter.Current;
            FilterQuery filterRegion = null;

            if (param.Filter != null)
            {
                if (param.Filter.Filters.Any())
                {

                    if (param.Filter.Filters.Exists(f => f.Field == Field.SiteName))
                    {
                        param.InstanceFilter();
                        FilterQuery filterFmn = param.Filter.Filters.FirstOrDefault(f => f.Field == Field.SiteName);
                        param.Filter.AddFilter(new FilterQuery(new FieldHelper(Table.MstSite, Field.Name), FilterOperator.Contains, filterFmn.Value));
                        param.Filter.Filters.Remove(filterFmn);
                    }
                    if (param.Filter.Filters.Exists(f => f.Field == Field.MstSiteId))
                    {
                        param.InstanceFilter();
                        FilterQuery filterFmn = param.Filter.Filters.FirstOrDefault(f => f.Field == Field.MstSiteId);
                        if (filterFmn.Value == "")
                        {
                            param.Filter.Filters.Remove(filterFmn);
                        }

                    }

                    if (param.Filter.Filters.Exists(f => f.Field == Field.MstRegionId))
                    {
                        param.InstanceFilter();
                        FilterQuery filterFmn = param.Filter.Filters.FirstOrDefault(f => f.Field == Field.MstRegionId);
                        if (filterFmn.Value == "")
                        {
                            param.Filter.Filters.Remove(filterFmn);
                        }
                        else
                        {
                            filterRegion = param.Filter.Filters.FirstOrDefault(f => f.Field == Field.MstRegionId);
                            param.Filter.Filters.Remove(filterRegion);
                        }
                        
                    }
                }
            }

            if (param.Sorts != null)
            {
                if (param.Sorts.Exists(s => s.Field == Field.SiteName))
                {
                    SortQuery sortFmn = param.Sorts.FirstOrDefault(s => s.Field == Field.SiteName);
                    param.Sorts.Add(new SortQuery(new FieldHelper(Table.MstSite, Field.Name), sortFmn.Direction));
                    param.Sorts.Remove(sortFmn);
                }

            }

            if (param.Filter == null)
                param.InstanceFilter();

            //algorithm
            dbObjectsTask = RepoOrganization.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
            countTask = RepoOrganization.CountAsync(param.Filter);

            //get and count data from db in background thread
            await Task.WhenAll(dbObjectsTask, countTask);

            //get callback from task
            dbObjects = dbObjectsTask.Result;
            count = countTask.Result;

            //map db data to model
            models = ListMapper.MapList<MstOrganization, OrganizationPresentationStub>(dbObjects);

            if (filterRegion != null)
            {
                models = models.Where(m => m.MstRegionId.ToString() == filterRegion.Value.ToString()).ToList();
                count = models.Count();
                models = models.Skip(param.Skip).Take(param.Take).ToList();
            }
            
            return JsonConvert.SerializeObject(new { total = count, data = models });
        }
        #endregion

        #region "Helper"

        #endregion
    }
}