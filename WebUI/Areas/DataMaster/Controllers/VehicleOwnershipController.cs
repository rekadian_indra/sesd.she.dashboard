﻿using Business.Abstract;
using Business.Entities;
using Business.Infrastructure;
using Common.Enums;
using MvcSiteMapProvider;
using MvcSiteMapProvider.Web.Mvc.Filters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebUI.Areas.DataMaster.Models.Vehicles;
using WebUI.Controllers;
using WebUI.Extension;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Areas.DataMaster.Controllers
{
    public class VehicleOwnershipController : BaseController<VehicleOwnershipFormStub>
    {
        #region "Constructor"
        public VehicleOwnershipController(IMstVehicleOwnershipRepository repoMasterVehicleOwnership, IMstCompanyTypeRepository repoMasterCompanyType)
        {
            RepoMasterVehicleOwnership = repoMasterVehicleOwnership;
            RepoMasterCompanyType = repoMasterCompanyType;
        }
        #endregion

        #region "Main Action"
        [MvcSiteMapNode(Title = TitleSite.VehicleOwnership, ParentKey = KeySite.Dashboard, Key = KeySite.IndexVehicleOwnership)]
        public override async Task<ActionResult> Index(object args = null)
        {
            return await base.Index();
        }

        [MvcSiteMapNode(Title = TitleSite.Create, ParentKey = KeySite.IndexVehicleOwnership, Key = KeySite.CreateVehicleOwnership)]
        [RequiredParameter(QueryStringValue.CompanyType, IncludePost = false, Mode = MatchMode.Any)]
        public async Task<ActionResult> Create(int companyType)
        {
            MstCompanyType dbObject = await RepoMasterCompanyType.FindByPrimaryKeyAsync(companyType);

            VehicleOwnershipFormStub model = new VehicleOwnershipFormStub
            {
                MstCompanyTypeId = companyType,
                CompanyTypeName = dbObject != null ? dbObject.Name : ""
            };

            return View("Form", model);
        }

        [HttpPost]
        public override async Task<ActionResult> Create(VehicleOwnershipFormStub model)
        {
            if (ModelState.IsValid)
            {
                MstVehicleOwnership dbObject = new MstVehicleOwnership();

                model.MapDbObject(dbObject);
                await RepoMasterVehicleOwnership.SaveAsync(dbObject);

                //message
                string template = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.CreateSuccess).ToString();
                string msg = $"Vehicle Ownership: {model.Name}";
                this.SetMessage(msg, template);

                return RedirectToAction(ActionSite.Index);
            }
            else
            {
                return View("Form", model);
            }
        }

        [SiteMapTitle(TitleSite.Breadcrumb)]
        [MvcSiteMapNode(Title = TitleSite.Edit, ParentKey = KeySite.IndexVehicleOwnership, Key = KeySite.EditVehicleOwnership)]
        public override async Task<ActionResult> Edit(params object[] id)
        {
            int primaryKey = int.Parse(id.FirstOrDefault().ToString());
            MstVehicleOwnership dbObject = await RepoMasterVehicleOwnership.FindByPrimaryKeyAsync(primaryKey);
            VehicleOwnershipFormStub model = new VehicleOwnershipFormStub(dbObject);

            ViewBag.Breadcrumb = model.Name;
            ViewBag.CompanyTypeName = model.CompanyTypeName;

            return View("Form", model);
        }

        [HttpPost]
        public override async Task<ActionResult> Edit(VehicleOwnershipFormStub model)
        {
            if (ModelState.IsValid)
            {
                MstVehicleOwnership dbObject = await RepoMasterVehicleOwnership.FindByPrimaryKeyAsync(model.Id);

                model.MapDbObject(dbObject);
                await RepoMasterVehicleOwnership.SaveAsync(dbObject);

                //message
                string template = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.CreateSuccess).ToString();
                string msg = $"Vehicle Ownership: {model.Name}";
                this.SetMessage(msg, template);

                return RedirectToAction(ActionSite.Index);
            }
            else
            {
                ViewBag.Breadcrumb = model.Name;
                ViewBag.CompanyTypeName = model.CompanyTypeName;

                return View("Form", model);
            }
        }

        [HttpPost]
        public override async Task<ActionResult> Delete(params object[] id)
        {
            int primaryKey = int.Parse(id.FirstOrDefault().ToString());
            MstVehicleOwnership dbObject = await RepoMasterVehicleOwnership.FindByPrimaryKeyAsync(primaryKey);
            ResponseModel response = new ResponseModel(true);

            if (!(await RepoMasterVehicleOwnership.DeleteAsync(dbObject)))
            {
                string message = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.DeleteFailed).ToString();
                response.SetFail(message);
            }

            return Json(response);
        }
        #endregion

        #region "Binding"
        public override async Task<string> Binding(params object[] args)
        {
            //lib
            int count;
            List<MstVehicleOwnership> dbObjects;
            List<VehicleOwnershipPresentationStub> models;
            Task<List<MstVehicleOwnership>> dbObjectsTask = null;
            Task<int> countTask = null;
            QueryRequestParameter param = QueryRequestParameter.Current;

            //algorithm
            if (param.Sorts.Count == 0)
            {
                param.Sorts.Add(new SortQuery(Field.Name, SortOrder.Ascending));
            }

            dbObjectsTask = RepoMasterVehicleOwnership.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
            countTask = RepoMasterVehicleOwnership.CountAsync(param.Filter);

            //get and count data from db in background thread
            await Task.WhenAll(dbObjectsTask, countTask);

            //get callback from task
            dbObjects = dbObjectsTask.Result;
            count = countTask.Result;

            //map db data to model
            models = ListMapper.MapList<MstVehicleOwnership, VehicleOwnershipPresentationStub>(dbObjects);

            return JsonConvert.SerializeObject(new { total = count, data = models });
        }
        #endregion

        #region "Helper"
        public async Task<JsonResult> NameExists(string Name, string CurrentName)
        {
            //lib
            bool exist;

            //algorithm
            if (string.IsNullOrWhiteSpace(Name))
            {
                exist = true;
            }
            else if (Name.ToLower() == CurrentName.ToLower())
            {
                exist = false;
            }
            else
            {
                MstVehicleOwnership dbObject;
                FilterQuery filter = new FilterQuery(
                    Field.Name,
                    FilterOperator.Equals,
                    Name
                );

                dbObject = await RepoMasterVehicleOwnership.FindAsync(filter);

                exist = dbObject != null;
            }

            return Json(!exist, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}