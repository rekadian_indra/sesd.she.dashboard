﻿using Business.Abstract;
using Business.Entities;
using Business.Infrastructure;
using Common.Enums;
using MvcSiteMapProvider;
using MvcSiteMapProvider.Web.Mvc.Filters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebUI.Areas.DataMaster.Models.Companies;
using WebUI.Areas.DataMaster.Models.Locations;
using WebUI.Areas.DataMaster.Models.Vehicles;
using WebUI.Controllers;
using WebUI.Extension;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Areas.DataMaster.Controllers
{
    public class LocationController : BaseController<LocationFormStub>
    {
        #region "Contructor"
        public LocationController
        (
            ISiteRepository repoSite,
            ILocationRepository repoLocation
        )
        {
            RepoSite = repoSite;
            RepoLocation = repoLocation;
        }
        #endregion

        #region "Main Action"
        [MvcSiteMapNode(Title = TitleSite.Location, ParentKey = KeySite.Dashboard, Key = KeySite.IndexLocation)]
        public override async Task<ActionResult> Index(object args = null)
        {
            return await base.Index();
        }

        [MvcSiteMapNode(Title = TitleSite.Create, ParentKey = KeySite.IndexLocation, Key = KeySite.CreateLocation)]
        [RequiredParameter(QueryStringValue.Site, IncludePost = false, Mode = MatchMode.Any)]
        public async Task<ActionResult> Create(int site)
        {
            MstSite dbSite = await RepoSite.FindByPrimaryKeyAsync(site);

            LocationFormStub model = new LocationFormStub
            {
                SiteId = dbSite.Id,
                SiteName = dbSite.Name
            };

            return View("Form", model);
        }

        [HttpPost]
        public override async Task<ActionResult> Create(LocationFormStub model)
        {
            if (ModelState.IsValid)
            {
                MstLocation dbObject = new MstLocation();

                model.MapDbObject(dbObject);
                await RepoLocation.SaveAsync(dbObject);

                //message
                string template = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.CreateSuccess).ToString();
                string msg = $"Location: {model.Name}";
                this.SetMessage(msg, template);

                return RedirectToAction(ActionSite.Index);
            }
            else
            {
                return View("Form", model);
            }
        }

        [SiteMapTitle(TitleSite.Breadcrumb)]
        [MvcSiteMapNode(Title = TitleSite.Edit, ParentKey = KeySite.IndexLocation, Key = KeySite.EditLocation)]
        public override async Task<ActionResult> Edit(params object[] id)
        {
            int primaryKey = int.Parse(id.FirstOrDefault().ToString());
            MstLocation dbObject = await RepoLocation.FindByPrimaryKeyAsync(primaryKey);
            LocationFormStub model = new LocationFormStub(dbObject);

            ViewBag.Breadcrumb = model.Name;
            ViewBag.SiteName = model.SiteName;

            return View("Form", model);
        }

        [HttpPost]
        public override async Task<ActionResult> Edit(LocationFormStub model)
        {
            if (ModelState.IsValid)
            {
                MstLocation dbObject = await RepoLocation.FindByPrimaryKeyAsync(model.Id);

                model.MapDbObject(dbObject);
                await RepoLocation.SaveAsync(dbObject);

                //message
                string template = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.CreateSuccess).ToString();
                string msg = $"Location: {model.Name}";
                this.SetMessage(msg, template);

                return RedirectToAction(ActionSite.Index);
            }
            else
            {
                ViewBag.Breadcrumb = model.Name;
                ViewBag.SiteName = model.SiteName;

                return View("Form", model);
            }
        }

        [HttpPost]
        public override async Task<ActionResult> Delete(params object[] id)
        {
            int primaryKey = int.Parse(id.FirstOrDefault().ToString());
            MstLocation dbObject = await RepoLocation.FindByPrimaryKeyAsync(primaryKey);
            ResponseModel response = new ResponseModel(true);

            if (!(await RepoLocation.DeleteAsync(dbObject)))
            {
                string message = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.DeleteFailed).ToString();
                response.SetFail(message);
            }

            return Json(response);
        }
        #endregion

        #region "Binding"
        public override async Task<string> Binding(params object[] args)
        {
            //lib
            int count;
            List<MstLocation> dbObjects;
            List<LocationPresentationStub> models;
            Task<List<MstLocation>> dbObjectsTask = null;
            Task<int> countTask = null;
            QueryRequestParameter param = QueryRequestParameter.Current;

            //algorithm
            if (param.Sorts.Count == 0)
            {
                param.Sorts.Add(new SortQuery(Field.Name, SortOrder.Ascending));
            }

            dbObjectsTask = RepoLocation.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
            countTask = RepoLocation.CountAsync(param.Filter);

            //get and count data from db in background thread
            await Task.WhenAll(dbObjectsTask, countTask);

            //get callback from task
            dbObjects = dbObjectsTask.Result;
            count = countTask.Result;

            //map db data to model
            models = ListMapper.MapList<MstLocation, LocationPresentationStub>(dbObjects);

            return JsonConvert.SerializeObject(new { total = count, data = models });
        }
        #endregion

        #region "Helper"

        #endregion
    }
}