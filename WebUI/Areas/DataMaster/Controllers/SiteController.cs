﻿using Business.Abstract;
using Business.Entities;
using Business.Infrastructure;
using Common.Enums;
using MvcSiteMapProvider;
using MvcSiteMapProvider.Web.Mvc.Filters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebUI.Areas.DataMaster.Models.Sites;
using WebUI.Controllers;
using WebUI.Extension;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Areas.DataMaster.Controllers
{
    [AuthorizeUser(Roles = new object[] { UserRole.Administrator })]
    public class SiteController : BaseController<SitesFormStub>
    {
        public SiteController
        (
            IRegionRepository repoRegion,
            ISiteRepository repoSite
        )
        {
            RepoRegion = repoRegion;
            RepoSite = repoSite;
        }

        #region "Main Action"
        [MvcSiteMapNode(Title = TitleSite.Site, Area = AreaSite.DataMaster, ParentKey = KeySite.Dashboard, Key = KeySite.IndexSite)]
        public override async Task<ActionResult> Index(object args = null)
        {
            return await base.Index();
        }

        [MvcSiteMapNode(Title = TitleSite.Create, ParentKey = KeySite.IndexSite, Key = KeySite.CreateSite)]
        public override async Task<ActionResult> Create()
        {
            SitesFormStub model = new SitesFormStub();
          
            await Task.Delay(0);

            return View("Form", model);
        }

        [HttpPost]
        public override async Task<ActionResult> Create(SitesFormStub model)
        {
            if (ModelState.IsValid)
            {
                MstSite dbObject = new MstSite();

                model.MapDbObject(dbObject);
                await RepoSite.SaveAsync(dbObject);

                //message
                string template = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.CreateSuccess).ToString();
                this.SetMessage(model.Name, template);
                return RedirectToAction(ActionSite.Index);
            }
            else
            {
                return View("Form", model);
            }
        }

        [SiteMapTitle(TitleSite.Breadcrumb)]
        [MvcSiteMapNode(Title = TitleSite.Edit, ParentKey = KeySite.IndexSite, Key = KeySite.EditSite)]
        public override async Task<ActionResult> Edit(params object[] id)
        {
            int primaryKey = int.Parse(id.FirstOrDefault().ToString());
            MstSite dbObject = await RepoSite.FindByPrimaryKeyAsync(primaryKey);
            SitesFormStub model = new SitesFormStub(dbObject);

            ViewBag.Breadcrumb = model.Name;

            return View("Form", model);
        }

        [HttpPost]
        public override async Task<ActionResult> Edit(SitesFormStub model)
        {
            if (ModelState.IsValid)
            {
                MstSite dbObject = await RepoSite.FindByPrimaryKeyAsync(model.Id);

                model.MapDbObject(dbObject);

                await RepoSite.SaveAsync(dbObject);

                //message
                string template = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.CreateSuccess).ToString();
                this.SetMessage(model.Name, template);

                return RedirectToAction(ActionSite.Index);
            }
            else
            {
                ViewBag.Breadcrumb = model.Name;

                return View("Form", model);
            }
        }

        [HttpPost]
        public override async Task<ActionResult> Delete(params object[] id)
        {
            ResponseModel response = new ResponseModel(true);
            int primaryKey = int.Parse(id.FirstOrDefault().ToString());
            MstSite dbObject = await RepoSite.FindByPrimaryKeyAsync(primaryKey);

            if (!(await RepoSite.DeleteAsync(dbObject)))
            {
                string message = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.DeleteFailed).ToString();
                response.SetFail(message);
            }

            return Json(response);
        }
        #endregion

        #region "Binding"
        public override async Task<string> Binding(params object[] args)
        {
            //lib
            int count;
            List<MstSite> dbObjects;
            List<SitePresentationStub> models;
            Task<List<MstSite>> dbObjectsTask = null;
            Task<int> countTask = null;
            QueryRequestParameter param = QueryRequestParameter.Current;

            if (param.Filter != null)
            {
                if (param.Filter.Filters.Any())
                {

                    if (param.Filter.Filters.Exists(f => f.Field == Field.RegionName))
                    {
                        param.InstanceFilter();
                        FilterQuery filterFmn = param.Filter.Filters.FirstOrDefault(f => f.Field == Field.RegionName);
                        param.Filter.AddFilter(new FilterQuery(new FieldHelper(Table.MstRegion, Field.Name), FilterOperator.Contains, filterFmn.Value));
                        param.Filter.Filters.Remove(filterFmn);
                    }
                    if (param.Filter.Filters.Exists(f => f.Field == Field.MstRegionId))
                    {
                        param.InstanceFilter();
                        FilterQuery filterFmn = param.Filter.Filters.FirstOrDefault(f => f.Field == Field.MstRegionId);
                        if (filterFmn.Value == "")
                        {
                            param.Filter.Filters.Remove(filterFmn);
                        }
                       
                    }
                }
            }

            if (param.Sorts != null)
            {
                if (param.Sorts.Exists(s => s.Field == Field.RegionName))
                {
                    SortQuery sortFmn = param.Sorts.FirstOrDefault(s => s.Field == Field.RegionName);
                    param.Sorts.Add(new SortQuery(new FieldHelper(Table.MstRegion, Field.Name), sortFmn.Direction));
                    param.Sorts.Remove(sortFmn);
                }
                
            }

            if (param.Filter == null)
                param.InstanceFilter();           

            //algorithm
            dbObjectsTask = RepoSite.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
            countTask = RepoSite.CountAsync(param.Filter);

            //get and count data from db in background thread
            await Task.WhenAll(dbObjectsTask, countTask);

            //get callback from task
            dbObjects = dbObjectsTask.Result;
            count = countTask.Result;

            //map db data to model
            models = ListMapper.MapList<MstSite, SitePresentationStub>(dbObjects);
           
            return JsonConvert.SerializeObject(new { total = count, data = models });
        }
        #endregion

        #region "Helper"

        #endregion
    }
}