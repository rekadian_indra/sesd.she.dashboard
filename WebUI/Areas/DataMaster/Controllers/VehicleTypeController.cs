﻿using Business.Abstract;
using Business.Entities;
using Business.Infrastructure;
using Common.Enums;
using MvcSiteMapProvider;
using MvcSiteMapProvider.Web.Mvc.Filters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebUI.Areas.DataMaster.Models.Vehicles;
using WebUI.Controllers;
using WebUI.Extension;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Areas.DataMaster.Controllers
{
    public class VehicleTypeController : BaseController<VehicleTypeFormStub>
    {
        #region "Constructor"
        public VehicleTypeController(IMstVehicleTypeRepository repoMasterVehicleType)
        {
            RepoMasterVehicleType = repoMasterVehicleType;
        }
        #endregion

        #region "Main Action"
        [MvcSiteMapNode(Title = TitleSite.VehicleType, ParentKey = KeySite.Dashboard, Key = KeySite.IndexVehicleType)]
        public override async Task<ActionResult> Index(object args = null)
        {
            return await base.Index();
        }

        [MvcSiteMapNode(Title = TitleSite.Create, ParentKey = KeySite.IndexVehicleType, Key = KeySite.CreateVehicleType)]
        public override async Task<ActionResult> Create()
        {
            VehicleTypeFormStub model = new VehicleTypeFormStub();

            await Task.Delay(0);

            return View("Form", model);
        }

        [HttpPost]
        public override async Task<ActionResult> Create(VehicleTypeFormStub model)
        {
            if (ModelState.IsValid)
            {
                MstVehicleType dbObject = new MstVehicleType();

                model.MapDbObject(dbObject);
                await RepoMasterVehicleType.SaveAsync(dbObject);

                //message
                string template = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.CreateSuccess).ToString();
                string msg = $"Vehicle type: {model.Name}";
                this.SetMessage(msg, template);

                return RedirectToAction(ActionSite.Index);
            }
            else
            {
                return View("Form", model);
            }
        }

        [SiteMapTitle(TitleSite.Breadcrumb)]
        [MvcSiteMapNode(Title = TitleSite.Edit, ParentKey = KeySite.IndexVehicleType, Key = KeySite.EditVehicleType)]
        public override async Task<ActionResult> Edit(params object[] id)
        {
            int primaryKey = int.Parse(id.FirstOrDefault().ToString());
            MstVehicleType dbObject = await RepoMasterVehicleType.FindByPrimaryKeyAsync(primaryKey);
            VehicleTypeFormStub model = new VehicleTypeFormStub(dbObject);

            ViewBag.Breadcrumb = model.Name;

            return View("Form", model);
        }

        [HttpPost]
        public override async Task<ActionResult> Edit(VehicleTypeFormStub model)
        {
            if (ModelState.IsValid)
            {
                MstVehicleType dbObject = await RepoMasterVehicleType.FindByPrimaryKeyAsync(model.Id);

                model.MapDbObject(dbObject);
                await RepoMasterVehicleType.SaveAsync(dbObject);

                //message
                string template = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.CreateSuccess).ToString();
                string msg = $"Vehicle type: {model.Name}";
                this.SetMessage(msg, template);

                return RedirectToAction(ActionSite.Index);
            }
            else
            {
                ViewBag.Breadcrumb = model.Name;

                return View("Form", model);
            }
        }

        [HttpPost]
        public override async Task<ActionResult> Delete(params object[] id)
        {
            int primaryKey = int.Parse(id.FirstOrDefault().ToString());
            MstVehicleType dbObject = await RepoMasterVehicleType.FindByPrimaryKeyAsync(primaryKey);
            ResponseModel response = new ResponseModel(true);

            if (!(await RepoMasterVehicleType.DeleteAsync(dbObject)))
            {
                string message = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.DeleteFailed).ToString();
                response.SetFail(message);
            }

            return Json(response);
        }
        #endregion

        #region "Binding"
        public override async Task<string> Binding(params object[] args)
        {
            //lib
            int count;
            List<MstVehicleType> dbObjects;
            List<VehicleTypePresentationStub> models;
            Task<List<MstVehicleType>> dbObjectsTask = null;
            Task<int> countTask = null;
            QueryRequestParameter param = QueryRequestParameter.Current;

            //algorithm
            if (param.Sorts.Count == 0)
            {
                param.Sorts.Add(new SortQuery(Field.Name, SortOrder.Ascending));
            }

            dbObjectsTask = RepoMasterVehicleType.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
            countTask = RepoMasterVehicleType.CountAsync(param.Filter);

            //get and count data from db in background thread
            await Task.WhenAll(dbObjectsTask, countTask);

            //get callback from task
            dbObjects = dbObjectsTask.Result;
            count = countTask.Result;

            //map db data to model
            models = ListMapper.MapList<MstVehicleType, VehicleTypePresentationStub>(dbObjects);

            return JsonConvert.SerializeObject(new { total = count, data = models });
        }
        #endregion
        
        #region "Helper"
        public async Task<JsonResult> NameExists(string Name, string CurrentName)
        {
            //lib
            bool exist;

            //algorithm
            if (string.IsNullOrWhiteSpace(Name))
            {
                exist = true;
            }
            else if (Name.ToLower() == CurrentName.ToLower())
            {
                exist = false;
            }
            else
            {
                MstVehicleType dbObject;
                FilterQuery filter = new FilterQuery(
                    Field.Name,
                    FilterOperator.Equals,
                    Name
                );

                dbObject = await RepoMasterVehicleType.FindAsync(filter);

                exist = dbObject != null;
            }

            return Json(!exist, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}