﻿using Business.Abstract;
using Business.Entities;
using Business.Infrastructure;
using Common.Enums;
using MvcSiteMapProvider;
using MvcSiteMapProvider.Web.Mvc.Filters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebUI.Areas.DataMaster.Models.Companies;
using WebUI.Areas.DataMaster.Models.Vehicles;
using WebUI.Controllers;
using WebUI.Extension;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Areas.DataMaster.Controllers
{
    public class VehicleController : BaseController<VehicleFormStub>
    {
        #region "Contructor"
        public VehicleController
        (
            IMstVehicleTypeRepository repoMasterVehicleType,
            IMstVehicleBrandRepository repoMasterVehicleBrand,
            IMstVehicleOwnershipRepository repoMasterVehicleOwnership,
            IMstCompanyRepository repoMasterCompany,
            IMstVehicleRepository repoMasterVehicle
        )
        {
            RepoMasterVehicleType = repoMasterVehicleType;
            RepoMasterVehicleBrand = repoMasterVehicleBrand;
            RepoMasterVehicleOwnership = repoMasterVehicleOwnership;
            RepoMasterCompany = repoMasterCompany;
            RepoMasterVehicle = repoMasterVehicle;
        }
        #endregion

        #region "Main Action"
        [MvcSiteMapNode(Title = TitleSite.Vehicle, ParentKey = KeySite.Dashboard, Key = KeySite.IndexVehicle)]
        public override async Task<ActionResult> Index(object args = null)
        {
            return await base.Index();
        }

        [MvcSiteMapNode(Title = TitleSite.Create, ParentKey = KeySite.IndexVehicle, Key = KeySite.CreateVehicle)]
        [RequiredParameter(QueryStringValue.VehicleType, IncludePost = false, Mode = MatchMode.Any)]
        public async Task<ActionResult> Create(int vehicleType)
        {
            int year = DateTime.Now.Year;
            MstVehicleType dbObject = await RepoMasterVehicleType.FindByPrimaryKeyAsync(vehicleType);

            VehicleFormStub model = new VehicleFormStub
            {
                MstVehicleTypeId = vehicleType,
                VehicleTypeName = dbObject != null ? dbObject.Name : "",
                VehicleYear = year
            };

            return View("Form", model);
        }

        [HttpPost]
        public override async Task<ActionResult> Create(VehicleFormStub model)
        {
            if (ModelState.IsValid)
            {
                MstVehicle dbObject = new MstVehicle();

                model.MapDbObject(dbObject);
                await RepoMasterVehicle.SaveAsync(dbObject);

                //message
                string template = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.CreateSuccess).ToString();
                string msg = $"Vehicle: {model.LicensePlateHeader} {model.LicensePlateNumber} {model.LicensePlateTail}";
                this.SetMessage(msg, template);

                return RedirectToAction(ActionSite.Index);
            }
            else
            {
                return View("Form", model);
            }
        }

        [SiteMapTitle(TitleSite.Breadcrumb)]
        [MvcSiteMapNode(Title = TitleSite.Edit, ParentKey = KeySite.IndexVehicle, Key = KeySite.EditVehicle)]
        public override async Task<ActionResult> Edit(params object[] id)
        {
            int primaryKey = int.Parse(id.FirstOrDefault().ToString());
            MstVehicle dbObject = await RepoMasterVehicle.FindByPrimaryKeyAsync(primaryKey);
            VehicleFormStub model = new VehicleFormStub(dbObject);

            ViewBag.Breadcrumb = $"Vehicle: {model.LicensePlateHeader} {model.LicensePlateNumber} {model.LicensePlateTail}";
            ViewBag.CompanyTypeName = model.VehicleTypeName;

            return View("Form", model);
        }

        [HttpPost]
        public override async Task<ActionResult> Edit(VehicleFormStub model)
        {
            if (ModelState.IsValid)
            {
                MstVehicle dbObject = await RepoMasterVehicle.FindByPrimaryKeyAsync(model.Id);

                model.MapDbObject(dbObject);
                await RepoMasterVehicle.SaveAsync(dbObject);

                //message
                string template = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.CreateSuccess).ToString();
                string msg = $"Vehicle: {model.LicensePlateHeader} {model.LicensePlateNumber} {model.LicensePlateTail}";
                this.SetMessage(msg, template);

                return RedirectToAction(ActionSite.Index);
            }
            else
            {
                ViewBag.Breadcrumb = $"Vehicle: {model.LicensePlateHeader} {model.LicensePlateNumber} {model.LicensePlateTail}";
                ViewBag.CompanyTypeName = model.VehicleTypeName;

                return View("Form", model);
            }
        }

        [HttpPost]
        public override async Task<ActionResult> Delete(params object[] id)
        {
            int primaryKey = int.Parse(id.FirstOrDefault().ToString());
            MstVehicle dbObject = await RepoMasterVehicle.FindByPrimaryKeyAsync(primaryKey);
            ResponseModel response = new ResponseModel(true);

            if (!(await RepoMasterVehicle.DeleteAsync(dbObject)))
            {
                string message = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.DeleteFailed).ToString();
                response.SetFail(message);
            }

            return Json(response);
        }
        #endregion

        #region "Binding"
        public override async Task<string> Binding(params object[] args)
        {
            //lib
            int count;
            List<MstVehicle> dbObjects;
            List<VehiclePresentationStub> models;
            Task<List<MstVehicle>> dbObjectsTask = null;
            Task<int> countTask = null;
            QueryRequestParameter param = QueryRequestParameter.Current;

            //algorithm
            if (param.Sorts.Count == 0)
            {
                param.Sorts.Add(new SortQuery(Field.Id, SortOrder.Ascending));
            }

            dbObjectsTask = RepoMasterVehicle.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
            countTask = RepoMasterVehicle.CountAsync(param.Filter);

            //get and count data from db in background thread
            await Task.WhenAll(dbObjectsTask, countTask);

            //get callback from task
            dbObjects = dbObjectsTask.Result;
            count = countTask.Result;

            //map db data to model
            models = ListMapper.MapList<MstVehicle, VehiclePresentationStub>(dbObjects);

            return JsonConvert.SerializeObject(new { total = count, data = models });
            #endregion
        }

        public async Task<string> BindingOwnhershipOption()
        {
            //lib
            List<MstVehicleOwnership> dbObjects;
            List<VehicleOwnershipPresentationStub> models;

            dbObjects = await RepoMasterVehicleOwnership.FindAllAsync();

            //map db data to model
            models = ListMapper.MapList<MstVehicleOwnership, VehicleOwnershipPresentationStub>(dbObjects);

            return JsonConvert.SerializeObject(new { data = models });
        }

        #region "Helper"
        public async Task<JsonResult> LicensePlateHeaderExists(string LicensePlateHeader, string CurrentLicensePlateHeader)
        {
            //lib
            bool exist;

            //algorithm
            if (string.IsNullOrWhiteSpace(LicensePlateHeader))
            {
                exist = true;
            }
            else if (LicensePlateHeader.ToLower() == CurrentLicensePlateHeader.ToLower())
            {
                exist = false;
            }
            else
            {
                MstVehicle dbObject;
                FilterQuery filter = new FilterQuery(
                    Field.LicensePlateHeader,
                    FilterOperator.Equals,
                    LicensePlateHeader
                );

                dbObject = await RepoMasterVehicle.FindAsync(filter);

                exist = dbObject != null;
            }

            return Json(!exist, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> LicensePlateNumberExists(string LicensePlateNumber, string CurrentLicensePlateNumber)
        {
            //lib
            bool exist;

            //algorithm
            if (string.IsNullOrWhiteSpace(LicensePlateNumber))
            {
                exist = true;
            }
            else if (LicensePlateNumber.ToLower() == CurrentLicensePlateNumber.ToLower())
            {
                exist = false;
            }
            else
            {
                MstVehicle dbObject;
                FilterQuery filter = new FilterQuery(
                    Field.LicensePlateNumber,
                    FilterOperator.Equals,
                    LicensePlateNumber
                );

                dbObject = await RepoMasterVehicle.FindAsync(filter);

                exist = dbObject != null;
            }

            return Json(!exist, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> LicensePlateTailExists(string LicensePlateTail, string CurrentLicensePlateTail)
        {
            //lib
            bool exist;

            //algorithm
            if (string.IsNullOrWhiteSpace(LicensePlateTail))
            {
                exist = true;
            }
            else if (LicensePlateTail.ToLower() == CurrentLicensePlateTail.ToLower())
            {
                exist = false;
            }
            else
            {
                MstVehicle dbObject;
                FilterQuery filter = new FilterQuery(
                    Field.LicensePlateTail,
                    FilterOperator.Equals,
                    LicensePlateTail
                );

                dbObject = await RepoMasterVehicle.FindAsync(filter);

                exist = dbObject != null;
            }

            return Json(!exist, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}