﻿using Business.Abstract;
using Business.Entities;
using Business.Infrastructure;
using Common.Enums;
using MvcSiteMapProvider;
using MvcSiteMapProvider.Web.Mvc.Filters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebUI.Areas.DataMaster.Models.Vehicles;
using WebUI.Controllers;
using WebUI.Extension;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Areas.DataMaster.Controllers
{
    public class VehicleBrandController : BaseController<VehicleBrandFormStub>
    {
        #region "Constructor"
        public VehicleBrandController(IMstVehicleBrandRepository repoMasterVehicleBrand)
        {
            RepoMasterVehicleBrand = repoMasterVehicleBrand;
        }
        #endregion

        #region "Main Action"
        [MvcSiteMapNode(Title = TitleSite.VehicleBrand, ParentKey = KeySite.Dashboard, Key = KeySite.IndexVehicleBrand)]
        public override async Task<ActionResult> Index(object args = null)
        {
            return await base.Index();
        }

        [MvcSiteMapNode(Title = TitleSite.Create, ParentKey = KeySite.IndexVehicleBrand, Key = KeySite.CreateVehicleBrand)]
        public override async Task<ActionResult> Create()
        {
            VehicleBrandFormStub model = new VehicleBrandFormStub();

            await Task.Delay(0);

            return View("Form", model);
        }

        [HttpPost]
        public override async Task<ActionResult> Create(VehicleBrandFormStub model)
        {
            if (ModelState.IsValid)
            {
                MstVehicleBrand dbObject = new MstVehicleBrand();

                model.MapDbObject(dbObject);
                await RepoMasterVehicleBrand.SaveAsync(dbObject);

                //message
                string template = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.CreateSuccess).ToString();
                string msg = $"Vehicle brand: {model.Name}";
                this.SetMessage(msg, template);

                return RedirectToAction(ActionSite.Index);
            }
            else
            {
                return View("Form", model);
            }
        }

        [SiteMapTitle(TitleSite.Breadcrumb)]
        [MvcSiteMapNode(Title = TitleSite.Edit, ParentKey = KeySite.IndexVehicleBrand, Key = KeySite.EditVehicleBrand)]
        public override async Task<ActionResult> Edit(params object[] id)
        {
            int primaryKey = int.Parse(id.FirstOrDefault().ToString());
            MstVehicleBrand dbObject = await RepoMasterVehicleBrand.FindByPrimaryKeyAsync(primaryKey);
            VehicleBrandFormStub model = new VehicleBrandFormStub(dbObject);

            ViewBag.Breadcrumb = model.Name;

            return View("Form", model);
        }

        [HttpPost]
        public override async Task<ActionResult> Edit(VehicleBrandFormStub model)
        {
            if (ModelState.IsValid)
            {
                MstVehicleBrand dbObject = await RepoMasterVehicleBrand.FindByPrimaryKeyAsync(model.Id);

                model.MapDbObject(dbObject);
                await RepoMasterVehicleBrand.SaveAsync(dbObject);

                //message
                string template = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.CreateSuccess).ToString();
                string msg = $"Vehicle brand: {model.Name}";
                this.SetMessage(msg, template);

                return RedirectToAction(ActionSite.Index);
            }
            else
            {
                ViewBag.Breadcrumb = model.Name;

                return View("Form", model);
            }
        }

        [HttpPost]
        public override async Task<ActionResult> Delete(params object[] id)
        {
            int primaryKey = int.Parse(id.FirstOrDefault().ToString());
            MstVehicleBrand dbObject = await RepoMasterVehicleBrand.FindByPrimaryKeyAsync(primaryKey);
            ResponseModel response = new ResponseModel(true);

            if (!(await RepoMasterVehicleBrand.DeleteAsync(dbObject)))
            {
                string message = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.DeleteFailed).ToString();
                response.SetFail(message);
            }

            return Json(response);
        }
        #endregion

        #region "Binding"
        public override async Task<string> Binding(params object[] args)
        {
            //lib
            int count;
            List<MstVehicleBrand> dbObjects;
            List<VehicleBrandPresentationStub> models;
            Task<List<MstVehicleBrand>> dbObjectsTask = null;
            Task<int> countTask = null;
            QueryRequestParameter param = QueryRequestParameter.Current;

            //algorithm
            if (param.Sorts.Count == 0)
            {
                param.Sorts.Add(new SortQuery(Field.Name, SortOrder.Ascending));
            }

            dbObjectsTask = RepoMasterVehicleBrand.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
            countTask = RepoMasterVehicleBrand.CountAsync(param.Filter);

            //get and count data from db in background thread
            await Task.WhenAll(dbObjectsTask, countTask);

            //get callback from task
            dbObjects = dbObjectsTask.Result;
            count = countTask.Result;

            //map db data to model
            models = ListMapper.MapList<MstVehicleBrand, VehicleBrandPresentationStub>(dbObjects);

            return JsonConvert.SerializeObject(new { total = count, data = models });
        }
        #endregion
        
        #region "Helper"
        public async Task<JsonResult> NameExists(string Name, string CurrentName)
        {
            //lib
            bool exist;

            //algorithm
            if (string.IsNullOrWhiteSpace(Name))
            {
                exist = true;
            }
            else if (Name.ToLower() == CurrentName.ToLower())
            {
                exist = false;
            }
            else
            {
                MstVehicleBrand dbObject;
                FilterQuery filter = new FilterQuery(
                    Field.Name,
                    FilterOperator.Equals,
                    Name
                );

                dbObject = await RepoMasterVehicleBrand.FindAsync(filter);

                exist = dbObject != null;
            }

            return Json(!exist, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}