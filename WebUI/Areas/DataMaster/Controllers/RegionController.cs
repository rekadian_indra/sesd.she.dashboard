﻿using Business.Abstract;
using Business.Entities;
using Business.Infrastructure;
using Common.Enums;
using MvcSiteMapProvider;
using MvcSiteMapProvider.Web.Mvc.Filters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebUI.Areas.DataMaster.Models.Regions;
using WebUI.Controllers;
using WebUI.Extension;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Areas.DataMaster.Controllers
{
    [AuthorizeUser(Roles = new object[] { UserRole.Administrator })]
    public class RegionController : BaseController<RegionFormStub>
    {
        public RegionController
        (
               IRegionRepository repoRegion
        )
        {
            RepoRegion = repoRegion;
        }

        #region "Main Action"
        [MvcSiteMapNode(Title = TitleSite.Region, Area = AreaSite.DataMaster, ParentKey = KeySite.Dashboard, Key = KeySite.IndexRegion)]
        public override async Task<ActionResult> Index(object args = null)
        {
            return await base.Index();
        }

        [MvcSiteMapNode(Title = TitleSite.Create, ParentKey = KeySite.IndexRegion, Key = KeySite.CreateRegion)]
        public override async Task<ActionResult> Create()
        {
            RegionFormStub model = new RegionFormStub();
            
            await Task.Delay(0);

            return View("Form", model);
        }

        [HttpPost]
        public override async Task<ActionResult> Create(RegionFormStub model)
        {
            if (ModelState.IsValid)
            {
                MstRegion dbObject = new MstRegion();

                model.MapDbObject(dbObject);
                await RepoRegion.SaveAsync(dbObject);

                
                //message
                string template = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.CreateSuccess).ToString();
                this.SetMessage(model.Name, template);

                return RedirectToAction(ActionSite.Index);
            }
            else
            {
                return View("Form", model);
            }
        }

        [SiteMapTitle(TitleSite.Breadcrumb)]
        [MvcSiteMapNode(Title = TitleSite.Edit, ParentKey = KeySite.IndexRegion, Key = KeySite.EditRegion)]
        public override async Task<ActionResult> Edit(params object[] id)
        {
            int primaryKey = int.Parse(id.FirstOrDefault().ToString());
            MstRegion dbObject = await RepoRegion.FindByPrimaryKeyAsync(primaryKey);
            RegionFormStub model = new RegionFormStub(dbObject);

            ViewBag.Breadcrumb = model.Name;

            return View("Form", model);
        }

        [HttpPost]
        public override async Task<ActionResult> Edit(RegionFormStub model)
        {
            if (ModelState.IsValid)
            {
                MstRegion dbObject = await RepoRegion.FindByPrimaryKeyAsync(model.Id);

                model.MapDbObject(dbObject);

                await RepoRegion.SaveAsync(dbObject);

                //message
                string template = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.CreateSuccess).ToString();
                this.SetMessage(model.Name, template);

                return RedirectToAction(ActionSite.Index);
            }
            else
            {
                ViewBag.Breadcrumb = model.Name;

                return View("Form", model);
            }
        }

        [HttpPost]
        public override async Task<ActionResult> Delete(params object[] id)
        {
            ResponseModel response = new ResponseModel(true);
            int primaryKey = int.Parse(id.FirstOrDefault().ToString());
            MstRegion dbObject = await RepoRegion.FindByPrimaryKeyAsync(primaryKey);

            if (!(await RepoRegion.DeleteAsync(dbObject)))
            {
                string message = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.DeleteFailed).ToString();
                response.SetFail(message);
            }

            return Json(response);
        }
        #endregion

        #region "Binding"
        public override async Task<string> Binding(params object[] args)
        {
            //lib
            int count;
            List<MstRegion> dbObjects;
            List<RegionPresentationStub> models;
            Task<List<MstRegion>> dbObjectsTask = null;
            Task<int> countTask = null;
            QueryRequestParameter param = QueryRequestParameter.Current;

            
            //algorithm
            dbObjectsTask = RepoRegion.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
            countTask = RepoRegion.CountAsync(param.Filter);

            //get and count data from db in background thread
            await Task.WhenAll(dbObjectsTask, countTask);

            //get callback from task
            dbObjects = dbObjectsTask.Result;
            count = countTask.Result;

            //map db data to model
            models = ListMapper.MapList<MstRegion, RegionPresentationStub>(dbObjects);

            return JsonConvert.SerializeObject(new { total = count, data = models });
        }
        #endregion

        #region "Helper"

        #endregion
    }
}