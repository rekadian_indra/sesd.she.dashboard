﻿$(function () {
    $.validator.methods.range = function (value, element, param) {
        var globalizedValue = value.replace(",", ".");
        return this.optional(element) || (globalizedValue >= param[0] && globalizedValue <= param[1]);
    }

    $.validator.methods.number = function (value, element) {
        return this.optional(element) || /^-?(?:\d+|\d{1,3}(?:[\s\.,]\d{3})+)(?:[\.,]\d+)?$/.test(value);
    }

    /**
    * init code by pazto @2018-04-27
    * topaz@rekadia.co.id
    */
    $.validator.addMethod("requiredif",
        function (value, element, parameters) {
            var id = "#" + parameters["dependentproperty"];

            // get the target value (as a string, 
            // as that's what actual value will be)
            var targetvalue = parameters["targetvalue"];
            targetvalue = (targetvalue === null ? "" : targetvalue).toString().toLowerCase();

            // get operator
            var operator = parameters["operator"];

            // get the actual value of the target control
            // note - this probably needs to cater for more 
            // control types, e.g. radios
            var $control = $(id);
            var controltype = $control.attr("type");
            var actualvalue = $control.val().toString().toLowerCase();

            if (controltype === "checkbox") {
                actualvalue = $control.attr("checked").toString().toLowerCase()
            }

            if (controltype === "radio") {
                actualvalue = $control.parentsUntil("form")
                    .find("input[type=\"" + controltype + "\"]:checked")
                    .val().toLowerCase();
            }

            // if the condition is true, reuse the existing 
            // required field validator functionality
            var needvalidation = ($.trim(targetvalue) === "*" && $.trim(actualvalue) !== "") ||
                ($.trim(operator) === "eq" && $.trim(targetvalue) === $.trim(actualvalue)) || //equals operator
                ($.trim(operator) === "neq" && $.trim(targetvalue) !== $.trim(actualvalue)); //not equal operator

            if (needvalidation) {
                return $.validator.methods.required.call(this, value, element, parameters);
            }

            return true;
        });

    /**
    * init code by pazto @2018-04-27
    * topaz@rekadia.co.id
    */
    $.validator.unobtrusive.adapters.add(
        "requiredif",
        ["dependentproperty", "targetvalue", "operator"],
        function (options) {
            options.rules["requiredif"] = {
                dependentproperty: options.params["dependentproperty"],
                targetvalue: options.params["targetvalue"],
                operator: options.params["operator"]
            };
            options.messages["requiredif"] = options.message;
        });
});
