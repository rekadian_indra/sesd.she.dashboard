﻿const FORM_SELECTOR = "form.form-horizontal";
const GRID_COMMAND_TEMPLATE = "<a class=\"k-button-icon #=className#\" #=attr# href=\"javascript:void(0)\"> \
                                    <span data-toggle=\"tooltip\" data-placement=\"auto bottom\" title=\"#=text#\" class=\"#=imageClass#\"></span> \
                              </a>";
const CENTER_HEADER_ATTRIBUTE = { "style": "text-align:center; white-space:unset; vertical-align:middle; " };

var pageableMessages = {
    display: "Data of {0} - {1} from {2} data",
    empty: "No data",
    first: "First page",
    last: "Last page",
    next: "Next page",
    previous: "Previous page",
    page: "Page of",
    of: "from {0}",
    itemsPerPage: "data per page",
    morePages: "Next"
};

var kendoGridFilterable = {
    messages: {
        info: "Show the rows that have value:", // sets the text on top of the filter menu
        filter: "Filter", // sets the text for the "Filter" button
        clear: "Delete", // sets the text for the "Clear" button

        // when filtering boolean numbers
        isTrue: "True", // sets the text for "isTrue" radio button
        isFalse: "False", // sets the text for "isFalse" radio button

        //changes the text of the "And" and "Or" of the filter menu
        and: "And",
        or: "Or"
    },
    operators: {
        //string: {
        //    eq: "Is equal to",
        //    neq: "Is not equal to",
        //    startswith: "Starts with",
        //    contains: "Contains",
        //    endswith: "Ends with"
        //},
        //filter menu for "string" type columns
        string: {
            //eq: "Equal to",
            //neq: "Tidak Equal to",
            //startswith: "Memiliki Awalan",
            contains: "Contains"
            //endswith: "Memiliki Akhiran"
        },
        //filter menu for "number" type columns
        number: {
            eq: "Equal to",
            //neq: "Tidak Equal to",
            //gte: "Lebih Besar Atau Equal to",
            gt: "Greater than",
            //lte: "Lebih Kecil Atau Equal to",
            lt: "Lower than"
        },
        //filter menu for "date" type columns
        date: {
            eq: "Equal to"
            //neq: "Tidak Equal to",
            //gte: "Setelah Atau Equal to",
            //gt: "Setelah",
            //lte: "Sebelum Atau Equal to"
            //lt: "Sebelum"
        },
        //filter menu for foreign key values
        enums: {
            eq: "Equal to",
            //neq: "Tidak Equal to"
        }
    },
    extra: false
};

var kendoGridMessages = {
    noRecords: "Data are not found."
};

var kendoGridPageable = {
    //numeric: false,
    //input: true,
    messages: pageableMessages
};


var rangeDateFilterable = {
    ui: function (e) {
        var datePicker = $(e).kendoDatePicker({
            format: "dd/MM/yyyy",
            parseFormats: ["dd/MM/yyyy HH:mm:ss", "dd/MM/yyyy HH.mm.ss"]
        }).data("kendoDatePicker");

        datePicker.element.prop("readonly", "readonly");
    },
    operators: {
        date: {
            gte: "Greater than or equal to",
            lte: "Lower than or equal to"
        }
    },
    extra: true
};

/**
* @param {Function} callback function callback
*/
function deleteAlert(callback = null) {
    if (callback !== null && typeof (callback) !== "function") {
        throw "callback must be function";
    }

    swal(
        {
            title: "Delete Data",
            text: "Are you sure to delete the data?",
            type: "warning",
            cancelButtonText: "Cancel",
            showCancelButton: true,
            confirmButtonClass: "btn btn-primary",
            confirmButtonText: "Ya",
            closeOnConfirm: false
        },
        function () {
            swal({
                title: "Loading",
                text: "Please waiting...",
                imageUrl: APP_NAME + "Content/sweet-alert/ajax-loader.gif",
                closeOnConfirm: false,
                confirmButtonClass: "hidden"
            });

            callback();
            swal("Status", "Data has been succesfully deleted.", "success");
        });
}

/**
 * @param {Function} uiOption function to init ui
 * @returns {kendo.ui.GridColumnFilterable} grid column filterable
 */
var dropDownListFilterable = function (uiOption) {
    if (typeof (uiOption) !== "function") {
        throw "uiOption must be function";
    }

    return {
        ui: uiOption,
        operators: {
            string: {
                eq: "Equal to"
            },
            number: {
                eq: "Equal to"
            },
            enums: {
                eq: "Equal to"
            }
        },
        extra: false
    };
};

$(function () {
    initSortIcon();
    requiredSign();
    initInputTooltip();
    initClientSideValidation();

    $.ajaxSetup({ cache: false }); //disable cache for IE

    $(".menu-nav").tooltip();

    $(".dropdown-submenu a.sub-a").on("click", function (e) {
        $(this).next("ul").toggle();
        e.stopPropagation();
        e.preventDefault();
    });     
});

kendo.culture("id-ID");

//datepicker format
$(".form-control-datepicker").kendoDatePicker({
	format: "dd/MM/yyyy",
	parseFormats: ["dd/MM/yyyy HH:mm:ss", "dd/MM/yyyy HH.mm.ss"]
}).prop("readonly", "readonly");

$(".form-control-datepicker-sql").kendoDatePicker({
	format: "yyyy/MM/dd",
	parseFormats: ["dd/MM/yyyy HH:mm:ss", "dd/MM/yyyy HH.mm.ss"]
}).prop("readonly", "readonly");

$(".form-control-datepicker-sql-nullable").kendoDatePicker({
	format: "yyyy/MM/dd",
	parseFormats: ["dd/MM/yyyy HH:mm:ss", "dd/MM/yyyy HH.mm.ss"]
});

$(".form-control-datetimepicker").kendoDateTimePicker({
    format: "dd/MM/yyyy HH:mm",
    parseFormats: ["dd/MM/yyyy HH:mm:ss", "dd/MM/yyyy HH.mm.ss"],
    interval: 30
}).prop("readonly", "readonly");

$(".form-control-datetimepicker-sql").kendoDatePicker({
    format: "yyyy/MM/dd HH:mm",
    parseFormats: ["dd/MM/yyyy HH:mm:ss", "dd/MM/yyyy HH.mm.ss"]
}).prop("readonly", "readonly");

//numeric textbox
$(".form-control-numeric").kendoNumericTextBox({
    //min: 0,
    //max: 2147483647,
    decimals: 0,
    format: "n0",
    spinners: false
});

$(".form-control-decimal").kendoNumericTextBox({
    //min: -2147483647,
    //max: 2147483647,
    decimals: 2,
    format: "n2",
    spinners: false
});

$(".form-control-decimal-n3").kendoNumericTextBox({
    //max: 2147483647,
    decimals: 3,
    format: "n3",
    spinners: false
});

$(".form-control-decimal-n4").kendoNumericTextBox({
    //max: 2147483647,
    decimals: 4,
    format: "n4",
    spinners: false
});

//kendo textarea
$(".form-control-editor").kendoEditor({
    tools: [
        { name: "insertLineBreak", shift: false },
        { name: "insertParagraph", shift: true }
    ],
    encoded: false,
    paste: function (ev) {
        var pText = "<span>" + ev.html + "</span>";

        ev.html = $(pText).text();
    }
});

//kendo upload
$(".form-control-upload").kendoUpload({
	async: {
		saveUrl: urlAction("", "FileManagement", "Save"),
		removeUrl: urlAction("", "FileManagement", "Remove"),
		batch: true,
		autoUpload: true,
		multiple: true,
	},
	localization: {
		select: "Select File...",
	},
	multiple: true,
	showFileList: true
});
$(".form-control-upload-static").kendoUpload({
	async: {
		saveUrl: urlAction("", "FileManagement", "Save"),
		batch: true,
		autoUpload: true,
		multiple: true,
	},
	localization: {
		select: "Select File...",
	},
	multiple: true,
	showFileList: false
});

/**
 * required sign using asterisk
 * @param {JQuery} form jQuery object
 */
function requiredSign(form = null) {
    if (form === null) {
        form = $(FORM_SELECTOR);
    }

    form.find("input[type=text], input[type=hidden], textarea, select, input[type=password]").each(function () {
        var req = $(this).attr("data-val-required") || $(this).attr("data-val-requiredif");

        if (undefined !== req) {
            var label = $(this).parentsUntil("form").find("label[for=\"" + $(this).attr("id") + "\"]");
            var text = label.text();

            if (text.length > 0) {
                label.append("<span style=\"color:red\" class=\"asterisk\"> *</span>");
            }
        }
    });
}

function initSortIcon() {
    $(window).on("load", function () {
        // Things that need to happen after full load
    
        var headers = $("body").find("table th[role=columnheader]").find("a.k-link");
        headers.each(function (i, el) {
            $(el).append(" <span class=\"k-icon k-i-kpi\"></span>");
        });
        
    });
}



function initInputTooltip() {
    //tooltip for input form
    $("input[type=text][data-val-required]:not([readonly]), textarea[data-val-required]:not([readonly])").tooltip({
        trigger: "focus",
        title: function () {
            var title = "";
            var titleArray = [];

            if ($(this).attr("data-val-required") !== null)
                titleArray[titleArray.length] = $(this).attr("data-val-required");

            if ($(this).attr("data-val-requiredif") !== null)
                titleArray[titleArray.length] = $(this).attr("data-val-requiredif");

            if ($(this).attr("data-val-number") !== null)
                titleArray[titleArray.length] = $(this).attr("data-val-number");

            title = titleArray.join(" ");

            return title;
        },
        container: "body"
    });
}

/**
 * init client side validation
 * @param {JQuery} form jQuery object
 * @param {JQuery} modal jQuery object
 */
function initClientSideValidation(form = null, modal = null) {
    if (modal !== null) {
        form = modal.find(FORM_SELECTOR);
    } else {
        if (form === null) {
            form = $(FORM_SELECTOR);
        }
    }

    if (form.length > 0) {
        var formChange = false;
        var validator = form.data("validator");
        //handle client validation on unsupported component. example : kendo dropdownlist, kendo editor, etc.
        //to use this add class always-validate to component
        if (validator !== undefined && validator !== null) {
            validator.settings.ignore = ":hidden:not(.always-validate)";
        }

        if (modal !== null) {
            //enable or disable button submit on client side validation
            form.find("input, textarea").on("keyup blur", function (e) {
                validateForm(null, null, modal);
            });
        } else {
            //enable or disable button submit on client side validation
            form.find("input, textarea").on("keyup blur", validateForm);

            //confirmation form data not save yet
            form.each(function () {
                if (!$(this).hasClass("not-track-change")) {
                    $(this).find("input, select, textarea").on("change", function () {
                        formChange = true;
                    });
                    $(this).submit(function () {
                        $(this).find("button[type=\"submit\"]").attr("disabled", true);
                        formChange = false;
                    });
                }
            });

            $(window).on("beforeunload", function () {
                if (formChange) {
                    return false;
                }
            });
        }
    }
}

/**
 * client side form validation
 * @param {JQuery.Event} e jQuery event
 * @param {JQuery} form jQuery object
 * @param {JQuery} modal jQuery object
 */
function validateForm(e = null, form = null, modal = null) {
    if (e !== null && form !== null) {
        throw "please select e or form";
    }

    var validator;

    if (modal !== null) {
        form = modal.find(FORM_SELECTOR);

        validator = form.data("validator");

        if (validator !== undefined && validator !== null) {
            if (form.valid()) {
                modal.find("#btnSave").prop("disabled", false);
            } else {
                modal.find("#btnSave").prop("disabled", "disabled");
            }
        }
    } else {
        if (e !== null) {
            form = $(this).closest("form");
        } else {
            if (form === null) {
                form = $(FORM_SELECTOR);
            }
        }

        validator = form.data("validator");

        if (validator !== undefined && validator !== null) {
            if (form.valid()) {
                $("button[type=\"submit\"]").prop("disabled", false);
            } else {
                $("button[type=\"submit\"]").prop("disabled", "disabled");
            }
        }
    }
}

/**
 * @param {string} xmlDate parse xmldate
 * @returns {Date} 2013-12-04T18:10:05.768
 */
function TimeStampToDate(xmlDate) {
    var dt = new Date();
    var dtS = xmlDate.slice(xmlDate.indexOf("T") + 1, xmlDate.indexOf("."));
    var TimeArray = dtS.split(":");
    //
    (TimeArray);
    dt.setHours(TimeArray[0]);
    dt.setMinutes(TimeArray[1]);
    dtS = xmlDate.slice(0, xmlDate.indexOf("T"));
    TimeArray = dtS.split("-");
    //console.log(TimeArray);
    dt.setFullYear(TimeArray[0]);
    dt.setMonth(TimeArray[1] - 1);
    dt.setDate(TimeArray[2]);
    //console.log(dt);
    return dt;
}

/**
 * mengambil tanggal dari datetimeco
 * @param {Date} datetime 12/31/2013 12:00:00 AM
 * @return {string} 12/31/2013
 */
function GetDate(datetime) {
    var parts = datetime.split(" ");
    var date = parts[0];

    return date;
}

/**
 * mengambil jam dari datetime
 * @param {Date} datetime 12/31/2013 12:00:00 AM
 * @return {string} 12:00
 */
function GetTime(datetime) {
    //kamus
    var parts = datetime.split(" ");
    var ampm = parts[2];
    var dateObject = new Date(datetime);

    //hour & minute
    var hour = dateObject.getHours();
    var minute = dateObject.getMinutes();
    if (ampm === "PM")
        hour += 12;
    hour = String("00" + hour).slice(-2);
    minute = String("00" + minute).slice(-2);

    //time string
    var time = hour + ":" + minute;

    return time;
}

/**
 * mengembalikan tanggal ditambah x days
 * @param {Date} original 12/30/2013 12:00:00 AM
 * @param {Number} days 1
 * @return {Date} 12/31/2013 12:00:00 AM
 */
function AddDays(original, days) {
    var newDate = new Date(original.setTime(original.getTime() + days * 86400000));
    return newDate;
}

/**
 * fungsi desimal
 * @param {number} n number
 * @param {string} sep sparator
 * @returns {string} 1.000.000
 */
function ThousandSeparator(n, sep) {
    var sRegExp = new RegExp("(-?[0-9]+)([0-9]{3})"), sValue = n + "";
    if (sep === undefined) { sep = ","; }
    while (sRegExp.test(sValue)) {
        sValue = sValue.replace(sRegExp, "$1" + sep + "$2");
    }
    return sValue;
}

/**
 * Disable spell check kendo editor
 * @param {kendo.ui.Editor} editor kendo editor
 */
function kendoEditorDisableSpellCheck(editor) {
    $(editor.body).attr("spellcheck", false);
    $(editor.body).css("padding", "10px");
}

/**
 * html decode
 * untuk web grid bisa menggunakan atribut encoded di kolom
 * @param {string} text text
 * @returns {jQuery} dom selector
 */
function FormatText(text) {
    var textContainer = $("<div></div>");
    if (text !== null) {
        textContainer.html(text.replace(/\n/g, "<br>"));
    }
    return textContainer.html();
}

/**
 * @param {string} selector dom selector
 * menghapus nilai dari $(selector)
 */
function clearValue(selector) {
    $(selector).val("");
}

/**
 * menampilkan info data kosong
 * @param {JQuery} el: jquery element
 * @param {string} message: pesan
 */
function displayEmptyGrid(el, message) {
    el.removeClass();
    el.attr("style", "");
    el.html(message);
}

/**
 * kendo grid mengambil data dari row
 * @param {string} e element selector
 * @returns {JQuery} jquery element
 */
function getDataRowGrid(e) {
    return $(e.target).closest("tr");
}

/**
 * konfirmasi delete sebelum di redirect
 * response: { Success: t/f, Message }
 * @param {string} url url
 * @param {string} datasource datasource
 * @param {string} otherdatasource datasource
 * @param {Function} callback function callback
 */
function goToDeletePage(url, datasource, otherdatasource = null, callback = null) {
    if (callback !== null && typeof (callback) !== "function") {
        throw "callback must be function";
    }

    swal(
        {
            title: "Delete Data",
            text: "Are you sure you want to delete this data?",
            type: "warning",
            cancelButtonText: "Cancel",
            showCancelButton: true,
            confirmButtonClass: "btn btn-primary",
            confirmButtonText: "Yes",
            closeOnConfirm: false
        },
        function () {
            swal({
                title: "Loading",
                text: "Please wait...",
                imageUrl: APP_NAME + "Content/sweet-alert/ajax-loader.gif",
                closeOnConfirm: false,
                confirmButtonClass: "hidden"
                //imageSize: "80x80"
            });

            $.ajax({
                url: url,
                type: "POST",
                success: function (data) {
                    if (data.Success === true) {
                        if (datasource !== null) {
                            datasource.read();

                            if (otherdatasource !== null) {
                                otherdatasource.read();
                            }
                        }

                        swal("Status", "Data has been successfully deleted.", "success");
                        //callback(data);
                    }
                    else {
                        swal("Error", data.Message, "error");
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    swal("Server Error. Please call the administrator.");
                }
            });
        });
}

//delete via post
function postToDeletePage(url, data, datasource) {
    swal(
        {
            title: "Delete Data",
            text: "Are you sure you want to delete this data?",
            type: "warning",
            cancelButtonText: "Cancel",
            showCancelButton: true,
            confirmButtonClass: "btn btn-primary",
            confirmButtonText: "Yes",
            closeOnConfirm: false
        },
        function () {
            swal({
                title: "Loading",
                text: "Please wait...",
                imageUrl: APP_NAME + "Content/sweet-alert/ajax-loader.gif",
                closeOnConfirm: false,
                confirmButtonClass: "hidden"
            });

            $.ajax({
                url: url,
                type: "POST",
                data: data,
                success: function (response) {
                    if (response.Success === true) {
                        if (datasource !== null) {
                            datasource.read();
                        }
                        swal("Status", "Data has been successfully deleted.", "success");
                    }
                    else {
                        swal(response.Message);
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    swal("Server Error. Please call the administrator");
                }
            });
        }
    );
}

function deleteThenRedirect(url, data, redirectPage) {
    swal(
        {
            title: "Delete Data",
            text: "Are you sure you want to delete this data?",
            type: "warning",
            cancelButtonText: "Cancel",
            showCancelButton: true,
            confirmButtonClass: "btn btn-primary",
            confirmButtonText: "Yes",
            closeOnConfirm: false
        },
        function () {
            swal({
                title: "Loading",
                text: "Please wait...",
                imageUrl: "/Content/sweet-alert/ajax-loader.gif",
                closeOnConfirm: false,
                confirmButtonClass: "hidden"
            });

            $.ajax({
                url: url,
                type: "POST",
                data: data,
                success: function (response) {
                    if (response.Success === true) {
                        window.location.href = redirectPage;
                    }
                    else {
                        swal(response.Message);
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    swal("Server Error. Please call the administrator");
                }
            });
        }
    );
}

/**
 * @param {string} el element selector
 * disable $(el) dengan menambahkan kelas disabled
 */
function disableMe(el) {
    $(el).addClass("disabled");
}

/**
 * @param {string} a string a
 * @param {string} b string b
 * @returns {string} mengecek kalau a == null menampilkan b
 */
function isnull(a, b) {
    b = b || "";
    return a || b;
}

/**
 * @param {string} value value
 * @returns {string} decode hasil penyimpanan dari kendo grid
 */
function htmlDecode(value) {
    var result = "";
    if (value !== null) {
        result = value.replace(/&lt;/g, "<").replace(/&gt;/g, ">");
    }

    return result;
}

/**
 * menghapus isi combobox jika masukan user tidak ada pada pilihan
 * @param {JQuery.Event} e event
 */
function comboBoxOnChange(e) {
    if (this.value() && this.selectedIndex === -1) {   //or use this.dataItem to see if it has an object
        this.text("");
    }
}

/**
 * @param {string} area param area
 * @param {string} controller param controller
 * @param {string} action param action
 * @returns {string} http://APP_ADDRESS/area/controler/action
 */
function urlAction(area, controller, action) {
    var result = "";

    if (area !== "") {
        result = kendo.format("{0}{1}/{2}/{3}", APP_ADDRESS, area, controller, action);
    } else {
        result = kendo.format("{0}{1}/{2}", APP_ADDRESS, controller, action);
    }

    return result;
}

/**
 * @param {string} url param url
 * @param {string} optionType param optionType
 * @param {string} mapField param mapField
 * @param {Array} args param args
 * @returns {kendo.data.DataSource} kendo dataSource
 */
function generateOptionDataSource(url, optionType, mapField, args = null) {
    return ds = new kendo.data.DataSource({
        transport: {
            read: {
                url: url,
                dataType: "json",
                type: "POST"
            },
            parameterMap: function (options, operation) {
                if (operation !== "read" && options.models) {
                    return kendo.stringify(options);
                } else {
                    var i;

                    if (options.filter) {
                        var filters = options.filter.filters;

                        for (i in filters) {
                            var filter = filters[i];

                            if (filter.field === "Text") {
                                //mapField is database field
                                filter.field = mapField;
                            }
                        }
                    }

                    if (options.sort) {
                        var sorts = options.sort;

                        for (i in sorts) {
                            var sort = sorts[i];

                            if (sort.field === "Text") {
                                //mapField is database field
                                sort.field = mapField;
                            }
                        }
                    }

                    options.optionType = optionType;
                    
                    if (args !== null) {
                        options.args = args;
                    }

                    return options;
                }
            }
        },
        schema: {
            model: {
                id: "Value",
                fields: {
                    "Text": { type: "string" },
                    "Value": { type: "string" }
                }
            }
        },
        serverPaging: true,
        serverFiltering: true,
        serverSorting: true,
        sort: { field: "Text", dir: "asc" }
    });
}

/**
* @param { string } url param url
* @param { string } optionType param optionType
* @returns { kendo.data.DataSource } kendo dataSource
*/
function generateEnumDataSource(url, optionType) {
    return ds = new kendo.data.DataSource({
        transport: {
            read: {
                url: url,
                dataType: "json",
                type: "POST"
            },
            parameterMap: function (options, operation) {
                if (operation !== "read" && options.models) {
                    return kendo.stringify(options);
                } else {
                    if (options.filter) {
                        var i;
                        var filters = options.filter.filters;
                        for (i in filters) {
                            var filter = filters[i];
                            if (filter.field === "Value") {
                                filter.field = "Value";
                            }
                        }
                    }
                    if (options.sort) {
                        var sorts = options.sort;
                        for (i in sorts) {
                            var sort = sorts[i];
                            if (sort.field === "Text") {
                                sort.field = "Text";
                            }
                        }
                    }
                    options.optionType = optionType;
                    return options;
                }
            }
        },
        schema: {
            model: {
                id: "Value",
                fields: {
                    "Text": { type: "String" },
                    "Value": { type: "String" }
                }
            }
        },
        serverPaging: true, 
        serverSorting: true,
        sort: { field: "Value", dir: "asc" }
    });
}

/** 
* @param { JQuery } element param element
* @param {string} url param url
* @param {string} optionType param optionType
* @param {string} mapField param mapField
* @param {string} optionLabel param optionLabel
* @param {boolean} filter param optionLabel
* @param {Array} args param args
* @returns {kendo.ui.DropDownList} dropdownlist
*/

function dropdownListGenerator(element, url, optionType, mapField, optionLabel, filter = false, args = null) {
    var dataSource = generateOptionDataSource(
        url,
        optionType,
        mapField,
        args
    );

    var result = $().data("kendoDropDownList");

    if (filter) {
        result = element.kendoDropDownList({
            optionLabel: optionLabel,
            dataTextField: "Text",
            dataValueField: "Value",
            dataSource: dataSource,
            filter: "contains"
        }).data("kendoDropDownList");
    } else {
        result = element.kendoDropDownList({
            optionLabel: optionLabel,
            dataTextField: "Text",
            dataValueField: "Value",
            dataSource: dataSource
        }).data("kendoDropDownList");
    }

    return result;
}

/** 
* @param {string} btnUploadSelector Jquery Selector of Upload button
* @param {string} btnDeleteSelector Jquery Selector of Delete button
* @param {string} inputFileSelector Jquery Selector of Input file
* @param {string} tableSelector Jquery Selector of Table element. Row data must have "Id", "FilePath", and "IsDeleted"
* @param {object} dataSource Kendo grid dataSource object
* @param {object} deleteUrl Url
* @param {object} validationOptions Kendo upload validation options (allowedExtensions, maxFileSize, ...)
* @returns {kendo.ui.Grid} kendoGrid
*/
function kendoMultipleUploadGenerator(btnUploadSelector, btnDeleteSelector, inputFileSelector, tableSelector, dataSource, deleteUrl, validationOptions = null) {
	// process table
	var grid = $(tableSelector).kendoGrid({
		autoBind: false,
		dataSource: dataSource,
		filterable: kendoGridFilterable,
		sortable: true,
		noRecords: true,
		columns: [
			{
				selectable: true,
				width: "32px"
			},
			{
				field: "FilePath",
				title: "File",
				width: "300px",
				headerAttributes: {
					style: "vertical-align: middle; font-weight: bold;"
				},
				filterable: false,
				sortable: false,
				template: "<a href='#=AbsolutePath#' target='_blank'>#=FriendlyName#</a>"
			},
			{
				field: "FileType",
				title: "Type",
				width: "60px",
				headerAttributes: {
					style: "vertical-align: middle; font-weight: bold;"
				},
				filterable: false,
				sortable: false,
				template: getIconFileType,
				attributes: { "class": "icon-type" }
			}
		],
		change: function (e) {
			var $selected = this.tbody.find("tr.k-state-selected[role=\"row\"]");

			if ($selected.exists()) {
				$(btnDeleteSelector).show(300);
			} else {
				$(btnDeleteSelector).hide(300);
			}
		}
	}).data("kendoGrid");

	grid.dataSource.read();

	// process input file
	if (validationOptions === null) {
		validationOptions = {
			maxFileSize: 20971520 //default (20 MB) size must in kibibyte or mebibyte or gibibyte (1 mebibyte = 1048576 byte)
		}
	}

	$(inputFileSelector).kendoUpload({
		async: {
			batch: true,
			saveUrl: urlAction("", "FileManagement", "Save"),
			removeUrl: urlAction("", "FileManagement", "Remove"),
		},
		multiple: true,
		validation: validationOptions,
		upload: function (e) {
			//console.log("upload event: "); console.log(e);
			fileOnUpload(e);
		},
		select: function (e) {
			//console.log("select event: "); console.log(e);
			fileOnSelect(e);
		},
		success: function (e) {
			//console.log("success event: "); console.log(e);
			fileOnSuccess(e);
		}
	}).closest(".k-upload").hide();

	function fileOnUpload(e) {
		swal({
			title: "Loading",
			text: "Please wait...",
			imageUrl: "/Content/sweet-alert/ajax-loader.gif",
			closeOnConfirm: false,
			confirmButtonClass: "hidden"
            });
	}

	function fileOnSelect(e) {
		var errorMessage = "",
			hasErrorSize = false,
			hasErrorExtension = false,
			files = e.files;

		$.each(files, function (i) {
			var errors = this.validationErrors;

			if (errors) {
				$.each(errors, function (j) {
					if (this.toString() === "invalidFileExtension") {
						if (!hasErrorExtension) {
							errorMessage = kendo.format("Allowed file type {0}.", validationOptions.allowedExtensions.join());
							hasErrorExtension = true;
						}
					} else {
						if (!hasErrorSize) {
							errorMessage = kendo.format("Max file size {0}.", validationOptions.maxFileSize);
							hasErrorSize = true;
						}
					}
				});
			}
		});

		errorMessage = errorMessage.trim();

		if (errorMessage !== "") {
			swal("Error", errorMessage, "error");
		}
	}

	function fileOnSuccess(e) {
		if (e.operation === "upload" && e.response.length > 0) {
			$.each(e.response, function (i) {
				this.Id = 0;
				this.IsDeleted = false;
				grid.dataSource.add(this);
			});
		}

		$(".sweet-alert").hide().prev().hide();
	}

	// process button upload & delete
	$(btnUploadSelector).on("click", function (e) {
		//console.log("btn click event: "); console.log(e);
		$(inputFileSelector).trigger("click");
	});

	$(btnDeleteSelector).on("click", function (e) {
		//console.log("btn delete event: "); console.log(e);
		var dataItems = [];
		var $selected = grid.tbody.find("tr.k-state-selected[role=\"row\"]");
		//console.log("selected items: ");console.log($selected);
		$selected.each(function (i) {
			dataItems.push(grid.dataItem(this));
		});
		//console.log("dataItems: "); console.log(dataItems);

		$.each(dataItems, function (i) {
			if ((this.Id === 0) || (this.id === 0)) { // table 
				//console.log("table item: "); console.log(this);
				$.ajax({
					type: "POST",
					url: urlAction("", "FileManagement", "Remove"),
					data: { "fileNames": this.FilePath }
				});
			} else {
				//console.log("db item: "); console.log(this);
				$.ajax({
					type: "POST",
					url: deleteUrl,
					data: { "id": this.Id }
				});
			}
			grid.dataSource.remove(this);
		});

		grid.trigger("change");
	});

	return grid;
}

function getIconFileType(dataItem) {
    var iconUrl = "",
        extension = dataItem.FileExtension;

    switch (extension) {
        case ".pdf":
            iconUrl = "/Content/img/file-types/pdf.png";
            break;
        case ".doc":
            iconUrl = "/Content/img/file-types/doc.png";
            break;
        case ".docx":
            iconUrl = "/Content/img/file-types/docx.png";
            break;
        case ".xls":
            iconUrl = "/Content/img/file-types/xls.png";
            break;
        case ".xlsx":
            iconUrl = "/Content/img/file-types/xlsx.png";
            break;
        case ".ppt":
            iconUrl = "/Content/img/file-types/ppt.png";
            break;
        case ".pptx":
            iconUrl = "/Content/img/file-types/pptx.png";
            break;
        default:
            iconUrl = "/Content/img/file-types/txt.png";
            break;
    }

    return kendo.format("<img src=\"{0}\" width=\"24\" height=\"24\" />", iconUrl);
}

/**
 * @param {kendo.ui.GridDataBoundEvent} e param event
 */
function dataBoundTooltip(e) {
    e.sender.element.find("[data-toggle=\"tooltip\"]").tooltip({ container: "body" });
}

/**
 * @param {kendo.ui.TreeListDataBoundEvent} e param event
 * @param {string} edit title edit
 * @param {string} destroy title delete
 */
function dataBoundTreeListCommand(e, edit, destroy) {
    var treeList = e.sender;

    var $rows = treeList.tbody.find("tr[role=\"row\"]");

    if ($rows.exists()) {
        $rows.each(function (i) {
            $(this).find("[data-command=\"edit_\"]")
                .attr("data-toggle", "tooltip")
                .attr("data-placement", "auto bottom")
                .attr("title", edit)
                .html("<span class=\"fa fa-lg fa-pencil-square\"></span>");

            $(this).find("[data-command=\"delete\"]")
                .attr("data-toggle", "tooltip")
                .attr("data-placement", "auto bottom")
                .attr("title", destroy)
                .html("<span class=\"fa fa-lg fa-trash\"></span>");
        });
    } else {
        treeList.element.find(".k-grid-content").css({
            "display": ""
        });
    }
}

/**
 * @param {kendo.ui.GridFilterMenuInitEvent} e param event
 */
function filterMenuInitRangeDate(e) {
    var dropDown = e.container.find("select:eq(0)").data("kendoDropDownList"),
        start = e.container.find("input:eq(0)").data("kendoDatePicker"),
        end = e.container.find("input:eq(1)").data("kendoDatePicker"),
        dataItem = dropDown.dataSource.at(1);

    //custom first dropdown
    dropDown.dataSource.remove(dataItem);
    
    //custom logic dropdown
    dropDown = e.container.find("select:eq(1)").data("kendoDropDownList");
    dataItem = dropDown.dataSource.at(1);
    dropDown.dataSource.remove(dataItem);

    //custom second dropdown
    dropDown = e.container.find("select:eq(2)").data("kendoDropDownList");
    dataItem = dropDown.dataSource.at(0);
    dropDown.dataSource.remove(dataItem);

    //add on change event to start
    start.bind("change", function (e) {
        var startDate = start.value(),
            endDate = end.value();

        if (startDate) {
            startDate = new Date(startDate);
            startDate.setDate(startDate.getDate());
            end.min(startDate);
        } else if (endDate) {
            start.max(new Date(endDate));
        } else {
            endDate = new Date();
            start.max(endDate);
            end.min(endDate);
        }
    });

    //add on change event to end
    end.bind("change", function (e) {
        var endDate = end.value(),
            startDate = start.value();

        if (endDate) {
            endDate = new Date(endDate);
            endDate.setDate(endDate.getDate());
            start.max(endDate);
        } else if (startDate) {
            end.min(new Date(startDate));
        } else {
            endDate = new Date();
            start.max(endDate);
            end.min(endDate);
        }
    });
}

/**
 * @param {kendo.ui.GridFilterMenuOpenEvent} e param event
 */
function filterMenuOpenRangeDate(e) {
    var dropDown = e.container.find("select:eq(0)").data("kendoDropDownList"),
        start = e.container.find("input:eq(0)").data("kendoDatePicker"),
        end = e.container.find("input:eq(1)").data("kendoDatePicker");

    var filter = e.sender.dataSource.filter(),
        filters = filter !== undefined && filter !== null ? filter.filters : [],
        selectedFilters = [];

    //custom first dropdown
    dropDown.value("gte");
    dropDown.trigger("change");

    //custom logic dropdown
    dropDown = e.container.find("select:eq(1)").data("kendoDropDownList");
    dropDown.value("and");
    dropDown.trigger("change");

    //custom second dropdown
    dropDown = e.container.find("select:eq(2)").data("kendoDropDownList");
    dropDown.value("lte");
    dropDown.trigger("change");

    for (var i in filters) {
        filter = filters[i];

        if (filter.field === e.field) {
            selectedFilters.push(filter);
        }
    }

    if (selectedFilters.length === 1) {
        filter = filters[i];

        if (filter.operator === "lte") {
            end.value(new Date(filter.value));
            end.trigger("change");
            start.value(null);
            start.trigger("change");
        }
    }
}

/**
 * @param {kendo.ui.GridFilterMenuInitEvent} e param event
 */
function filterMenuInitNumeric(e) {
    //check ctrl+v, it should number too
    var $inputNumber = e.container.find("input:eq(0)");

    $inputNumber.on("paste", function (e) {
        setTimeout(function () {
            isnum = /^[\d-]+$/.test($inputNumber.val());

            if (!isnum) {
                $inputNumber.val("");
                $inputNumber.css({ "border-color": "#ff0000" });
                setTimeout(function () {
                    $inputNumber.css({ "border-color": "" });
                }, 200);
            }
        }, 0); //just break the callstack to let the event finish               
    });

    // check every onkeydown event, if it's string or not.
    $inputNumber.on("keydown", inputNumStrip);
}

/**
 * An event for the input should not alphabetic, can do Numeric and Minus
 * @param {JQuery.Event} e param event
*/
function inputNumStrip(e) {
    // Allow: backspace, delete, tab, escape, enter, ctrl, alt and -
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 17, 18, 110, 109, 189, 173]) !== -1 ||
        // Allow: Ctrl+A,Ctrl+C,Ctrl+V, Command+A
        ((e.keyCode === 65 || e.keyCode === 86 || e.keyCode === 67) && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: home, end, left, right, down, up
        (e.keyCode >= 35 && e.keyCode <= 40)) {
        // let it happen, don't do anything
        return;
    }

    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
        $(e.target).css({ "border-color": "#ff0000" });
        setTimeout(function () {
            $(e.target).css({ "border-color": "" });
        }, 200);
    }
}

/**
 * @param {kendo.data.ObservableArray} data callback dropdown event
 * @returns {Array} array object grid datasource after clean up unused properties
 */
function cleanupDataSource(data) {
    var result = [];

    for (var i = 0; i < data.length; i++) {
        for (var prop in data[i]) {
            switch (prop) {
                case "defaults":
                case "fields":
                case "idField":
                case "_defaultId":
                    delete data[i][prop];
                    break;
                default:
                    break;
            }
        }

        result.push(data[i]);
    }

    return result;
}

/**
 * @param {object} filter kendo dataSource filter
 */
function cleanupDataSourceFilter(filter) {
    var prop;

    if (filter.logic !== null && filter.filters !== null && filter.filters !== undefined && filter.filters.length > 0) {
        //cleanup property field, operator and value
        for (prop in filter) {
            switch (prop) {
                case "field":
                case "operator":
                case "value":
                    delete filter[prop];
                    break;
                default:
                    break;
            }
        }

        for (var i in filter.filters) {
            var f = filter.filters[i];

            //call recursive to cleanup filter in filters
            cleanupDataSourceFilter(f);
        }
    } else {
        //cleanup property logic and filters
        for (prop in filter) {
            switch (prop) {
                case "logic":
                case "filters":
                    delete filter[prop];
                    break;
                default:
                    break;
            }
        }
    }
}

/**
 * @param {string} url url
 * @param {kendo.data.DataSource} dataSource dataSource which that options to put into session
 * @param {Function} callback function callback
 */
function putDataSourceOption(url, dataSource, callback = null) {
    if (callback !== null && typeof (callback) !== "function") {
        throw "callback must be function";
    }

    var options = {
        Page: dataSource.page(),
        PageSize: dataSource.pageSize(),
        Sorts: dataSource.sort(),
        Filter: dataSource.filter()
    };

    //parse type of date 
    if (options.Filter !== null && options.Filter !== undefined && options.Filter.filters.length > 0) {
        //level 1
        for (var i in options.Filter.filters) {
            var f1 = options.Filter.filters[i];

            if (Object.prototype.toString.call(f1.value) === "[object Date]") {
                f1.value = kendo.toString(f1.value, "yyyy-MM-ddTHH:mm:ss");
            }

            if (f1.logic !== null && f1.logic !== undefined && f1.filters !== null
                && f1.filters !== undefined && f1.filters.length > 0) {
                //level 2
                for (var j in f1.filters) {
                    var f2 = f1.filters[j];

                    if (Object.prototype.toString.call(f2.value) === "[object Date]") {
                        f2.value = kendo.toString(f2.value, "yyyy-MM-ddTHH:mm:ss");
                    }
                }
            }
        }
    }

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=UTF-8",
        url: url,
        data: JSON.stringify(options),
        success: function (e) {
            if (callback !== null) {
                callback();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            swal("Server Error. Call administrator");
        }
    });
}

/**
 * @param {object} source dataSource option from session
 * @param {object} target taget dataSource option
 */
function mapDataSourceOption(source, target) {
    if (source.Page !== null) {
        target.page = source.Page;
    }

    if (source.PageSize !== null) {
        target.pageSize = source.PageSize;
    }

    if (source.Sorts !== null && source.Sorts.length > 0) {
        target.sort = source.Sorts;
    }

    if (source.Filter !== null) {
        cleanupDataSourceFilter(source.Filter);
        target.filter = source.Filter;
    }
}

function keyUpConvertSize(elementName, size, text) {
   
    var t = $(elementName).data("kendoNumericTextBox");

    if (text !== "" && size === "inch") {
        t.value(milimeterToInch(text));
    }
    else if (text !== "" && size === "mm")  {
        t.value(inchToMilimeter(text));
    }
    else if (text !== "" && size === "kpa") {
        t.value(psiToKpa(text));
    }
    else if (text !== "" && size === "psi") {
        t.value(kpaToPsi(text));
    }
    else if (text !== "" && size === "celcius") {
        t.value(fahrenheitToCelcius(text));
    }
    else if (text !== "" && size === "fahrenheit") {
        t.value(celciusToFahrenheit(text));
    }
    else {
        t.value(0);
    }
}

function milimeterToInch(milimeter) {
    var result = parseFloat(milimeter) * parseFloat(0.0393701);
    return result;
}

function inchToMilimeter(inch) {
    var result = parseFloat(inch) * parseFloat(25.4);
    return result;
}

function psiToKpa(psi) {
    var result = parseFloat(psi) * parseFloat(6.894757);
    return result;
}

function kpaToPsi(kpa) {
    var result = parseFloat(kpa) * parseFloat(0.1450377);
    return result;
}

function fahrenheitToCelcius(f) {
    var result = (parseFloat(f) - 32) * 5 / 9;
    return result;
}

function celciusToFahrenheit(c) {
    var result = (parseFloat(c) * 9 / 5) + 32;
    return result;
}

// icon hover
$('#bs-example-navbar-collapse-1 ul li.front:nth-child(1) a').mouseover(function () {
    $(this).find('img').attr('src', APP_NAME + 'Content/img/icon/icon-23.png');
}).mouseout(function () {
    $(this).find('img').attr('src', APP_NAME + 'Content/img/icon/icon-22.png');
});

$('#bs-example-navbar-collapse-1 ul li.front:nth-child(2) a').mouseover(function () {
    $(this).find('img').attr('src', APP_NAME + 'Content/img/icon/icon-25.png');
}).mouseout(function () {
    $(this).find('img').attr('src', APP_NAME + 'Content/img/icon/icon-24.png');
    });

$('#bs-example-navbar-collapse-1 ul li.front:nth-child(3) a').mouseover(function () {
    $(this).find('img').attr('src', APP_NAME + 'Content/img/icon/icon-29.png');
}).mouseout(function () {
    $(this).find('img').attr('src', APP_NAME + 'Content/img/icon/icon-28.png');
});

$('#bs-example-navbar-collapse-1 ul li.front:nth-child(4) a').mouseover(function () {
    $(this).find('img').attr('src', APP_NAME + 'Content/img/icon/icon-27.png');
}).mouseout(function () {
    $(this).find('img').attr('src', APP_NAME + 'Content/img/icon/icon-26.png');
});

$('#bs-example-navbar-collapse-1 ul li.front:nth-child(5) a').mouseover(function () {
    $(this).find('img').attr('src', APP_NAME + 'Content/img/icon/icon-21.png');
}).mouseout(function () {
    $(this).find('img').attr('src', APP_NAME + 'Content/img/icon/icon-20.png');
    });

$('#bs-example-navbar-collapse-1 ul li.back:nth-child(1) a').mouseover(function () {
    $(this).find('img').attr('src', APP_NAME + 'Content/img/icon/icon-23.png');
}).mouseout(function () {
    $(this).find('img').attr('src', APP_NAME + 'Content/img/icon/icon-22.png');
});

$('#bs-example-navbar-collapse-1 ul li.back:nth-child(2) a').mouseover(function () {
    $(this).find('img').attr('src', APP_NAME + 'Content/img/icon/icon-15.png');
}).mouseout(function () {
    $(this).find('img').attr('src', APP_NAME + 'Content/img/icon/icon-14.png');
    });

$('#bs-example-navbar-collapse-1 ul li.back:nth-child(3) a').mouseover(function () {
    $(this).find('img').attr('src', APP_NAME + 'Content/img/icon/icon-19.png');
}).mouseout(function () {
    $(this).find('img').attr('src', APP_NAME + 'Content/img/icon/icon-18.png');
});

$('#bs-example-navbar-collapse-1 ul li.back:nth-child(4) a').mouseover(function () {
    $(this).find('img').attr('src', APP_NAME + 'Content/img/icon/icon-11.png');
}).mouseout(function () {
    $(this).find('img').attr('src', APP_NAME + 'Content/img/icon/icon-10.png');
});

$('#bs-example-navbar-collapse-1 ul li.back:nth-child(5) a').mouseover(function () {
    $(this).find('img').attr('src', APP_NAME + 'Content/img/icon/icon-17.png');
}).mouseout(function () {
    $(this).find('img').attr('src', APP_NAME + 'Content/img/icon/icon-16.png');
    });

// navigation side menu
$('#side-menu li.profile:nth-child(1) a').mouseover(function () {
    $(this).find('img').attr('src', APP_NAME + 'Content/img/icon/icon-03.png');
}).mouseout(function () {
    $(this).find('img').attr('src', APP_NAME + 'Content/img/icon/icon-02.png');
    });
$('#side-menu li.profile:nth-child(2) a').mouseover(function () {
    $(this).find('img').attr('src', APP_NAME + 'Content/img/icon/icon-07.png');
}).mouseout(function () {
    $(this).find('img').attr('src', APP_NAME + 'Content/img/icon/icon-06.png');
    });
$('#side-menu li.profile:nth-child(3) a').mouseover(function () {
    $(this).find('img').attr('src', APP_NAME + 'Content/img/icon/icon-05.png');
}).mouseout(function () {
    $(this).find('img').attr('src', APP_NAME + 'Content/img/icon/icon-04.png');
    });
$('#side-menu li.profile:nth-child(4) a').mouseover(function () {
    $(this).find('img').attr('src', APP_NAME + 'Content/img/icon/icon-09.png');
}).mouseout(function () {
    $(this).find('img').attr('src', APP_NAME + 'Content/img/icon/icon-08.png');
    });

$('#side-menu li.finance:nth-child(1) a').mouseover(function () {
    $(this).find('img').attr('src', APP_NAME + 'Content/img/icon/icon-32.png');
}).mouseout(function () {
    $(this).find('img').attr('src', APP_NAME + 'Content/img/icon/icon-31.png');
});
$('#side-menu li.finance:nth-child(2) a').mouseover(function () {
    $(this).find('img').attr('src', APP_NAME + 'Content/img/icon/icon-09.png');
}).mouseout(function () {
    $(this).find('img').attr('src', APP_NAME + 'Content/img/icon/icon-08.png');
    });

$('#side-menu li.analysis:nth-child(1) a').mouseover(function () {
    $(this).find('img').attr('src', APP_NAME + 'Content/img/icon/icon-05.png');
}).mouseout(function () {
    $(this).find('img').attr('src', APP_NAME + 'Content/img/icon/icon-04.png');
    });
$('#side-menu li.analysis:nth-child(2) a').mouseover(function () {
    $(this).find('img').attr('src', APP_NAME + 'Content/img/icon/icon-07.png');
}).mouseout(function () {
    $(this).find('img').attr('src', APP_NAME + 'Content/img/icon/icon-06.png');
    });
$('#side-menu li.analysis:nth-child(3) a').mouseover(function () {
    $(this).find('img').attr('src', APP_NAME + 'Content/img/icon/icon-32.png');
}).mouseout(function () {
    $(this).find('img').attr('src', APP_NAME + 'Content/img/icon/icon-31.png');
    });

$('#side-menu li.upload:nth-child(1) a').mouseover(function () {
    $(this).find('img').attr('src', APP_NAME + 'Content/img/icon/icon-09.png');
}).mouseout(function () {
    $(this).find('img').attr('src', APP_NAME + 'Content/img/icon/icon-08.png');
});
$('#side-menu li.upload:nth-child(2) a').mouseover(function () {
    $(this).find('img').attr('src', APP_NAME + 'Content/img/icon/icon-21.png');
}).mouseout(function () {
    $(this).find('img').attr('src', APP_NAME + 'Content/img/icon/icon-20.png');
});
$('#side-menu li.upload:nth-child(3) a').mouseover(function () {
    $(this).find('img').attr('src', APP_NAME + 'Content/img/icon/icon-03.png');
}).mouseout(function () {
    $(this).find('img').attr('src', APP_NAME + 'Content/img/icon/icon-02.png');
    });

$('.box-button div.box-button-image:nth-child(1) a').mouseover(function () {
    $(this).find('img').attr('src', APP_NAME + 'Content/img/icon/icon-41.png');
}).mouseout(function () {
    $(this).find('img').attr('src', APP_NAME + 'Content/img/icon/icon-38.png');
    });
$('.box-button div.box-button-image:nth-child(2) a').mouseover(function () {
    $(this).find('img').attr('src', APP_NAME + 'Content/img/icon/icon-42.png');
}).mouseout(function () {
    $(this).find('img').attr('src', APP_NAME + 'Content/img/icon/icon-39.png');
    });
$('.box-button div.box-button-image:nth-child(3) a').mouseover(function () {
    $(this).find('img').attr('src', APP_NAME + 'Content/img/icon/icon-43.png');
}).mouseout(function () {
    $(this).find('img').attr('src', APP_NAME + 'Content/img/icon/icon-40.png');
});

/**
 * @param {kendo.ui.GridFilterMenuInitEvent} e param event
 */
function filterMenuInitRangeNumeric(e) {
    var dropDown = e.container.find("select:eq(0)").data("kendoDropDownList"),
        start = e.container.find("input:eq(1)").data("kendoNumericTextBox"),
        end = e.container.find("input:eq(3)").data("kendoNumericTextBox"),
        dataItem = dropDown.dataSource.at(1);

    //custom first dropdown
    dropDown.dataSource.remove(dataItem);

    //custom logic dropdown
    dropDown = e.container.find("select:eq(1)").data("kendoDropDownList");
    dataItem = dropDown.dataSource.at(1);
    dropDown.dataSource.remove(dataItem);

    //custom second dropdown
    dropDown = e.container.find("select:eq(2)").data("kendoDropDownList");
    dataItem = dropDown.dataSource.at(0);
    dropDown.dataSource.remove(dataItem);

    //add on change event to start
    start.bind("change", function (e) {
        start.max(end.value());
        end.min(start.value());
    });

    //add on change event to end
    end.bind("change", function (e) {
        start.max(end.value());
        end.min(start.value());
    });
}

/**
 * @param {kendo.ui.GridFilterMenuOpenEvent} e param event
 */
function filterMenuOpenRangeNumeric(e) {
    var dropDown = e.container.find("select:eq(0)").data("kendoDropDownList"),
        start = e.container.find("input:eq(0)").data("kendoNumericTextBox"),
        end = e.container.find("input:eq(1)").data("kendoNumericTextBox");

    var filter = e.sender.dataSource.filter(),
        filters = filter !== undefined && filter !== null ? filter.filters : [],
        selectedFilters = [];

    //custom first dropdown
    dropDown.value("gte");
    dropDown.trigger("change");

    //custom logic dropdown
    dropDown = e.container.find("select:eq(1)").data("kendoDropDownList");
    dropDown.value("and");
    dropDown.trigger("change");

    //custom second dropdown
    dropDown = e.container.find("select:eq(2)").data("kendoDropDownList");
    dropDown.value("lte");
    dropDown.trigger("change");

    for (var i in filters) {
        filter = filters[i];

        if (filter.field === e.field) {
            selectedFilters.push(filter);
        }
    }

    if (selectedFilters.length === 1) {
        filter = filters[i];

        if (filter.operator === "lte") {
            end.value(filter.value());
            end.trigger("change");
            start.value(null);
            start.trigger("change");
        }
    }
}