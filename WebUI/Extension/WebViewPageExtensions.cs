﻿using Newtonsoft.Json;
using System;
using System.Web;
using System.Web.Mvc;
using WebUI.Infrastructure;

namespace WebUI.Extension
{
    public abstract class WebViewPageExtensions : WebViewPage
    {
        public virtual new Principal User
        {
            get
            {
                return HttpContext.Current.User as Principal; ;
            }
        }

        public string DataSourceOption
        {
            get
            {
                return Context.Session[AppSession.DataSourceOption] != null ?
                       Context.Session[AppSession.DataSourceOption].ToString() : "null";
            }
        }

        #region Default Method

        public string SerializeObject<TObject>(TObject @object)
        {
            if (@object == null)
                throw new ArgumentNullException();

            return JsonConvert.SerializeObject(@object);
        }

        public TObject DeserializeObject<TObject>(string json)
        {
            if (string.IsNullOrEmpty(json))
                throw new ArgumentNullException();

            return JsonConvert.DeserializeObject<TObject>(json);
        }

        #endregion
    }

    public abstract class WebViewPageExtensions<T> : WebViewPage<T>
    {
        public virtual new Principal User
        {
            get
            {
                return HttpContext.Current.User as Principal;
            }
        }

        public string DataSourceOption
        {
            get
            {
                return Context.Session[AppSession.DataSourceOption] != null ?
                       Context.Session[AppSession.DataSourceOption].ToString() : "null";
            }
        }

        #region Default Method

        public string SerializeObject<TObject>(TObject @object)
        {
            if (@object == null)
                throw new ArgumentNullException();

            return JsonConvert.SerializeObject(@object);
        }

        public TObject DeserializeObject<TObject>(string json)
        {
            if (string.IsNullOrEmpty(json))
                throw new ArgumentNullException();

            return JsonConvert.DeserializeObject<TObject>(json);
        }

        #endregion
    }
}