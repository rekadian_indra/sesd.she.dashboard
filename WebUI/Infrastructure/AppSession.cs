﻿namespace WebUI.Infrastructure
{
    public static class AppSession
    {
        public const string UserLogin = "USER_LOGIN";
        public const string DataSourceOption = "DATA_SOURCE_OPTION";
    }
}