﻿namespace WebUI.Infrastructure
{
    public static class KeySite
    {
        //default key
        public const string Dashboard = "Dashboard";
        public const string DashboardUserManagement = "DashboardUserManagement";

        //app key
        public const string IndexUser = "IndexUser";
        public const string AssignRevokeRoles = "AssignRevokeRoles";

        public const string IndexRegion = "IndexRegion";
        public const string CreateRegion = "CreateRegion";
        public const string EditRegion = "EditRegion";

        public const string IndexSite = "IndexSite";
        public const string CreateSite = "CreateSite";
        public const string EditSite = "EditSite";

        public const string IndexOrganization = "IndexOrganization";
        public const string CreateOrganization = "CreateOrganization";
        public const string EditOrganization = "EditOrganization";

        public const string IndexRole = "IndexRole";
        public const string CreateRole = "CreateRole";
        public const string EditRole = "EditRole";
        public const string AssignRevokeModules = "AssignRevokeModules";

        public const string IndexCompany = "IndexCompany";
        public const string CreateCompany = "CreateCompany";
        public const string EditCompany = "EditCompany";

        public const string IndexCompanyType = "IndexCompanyType";
        public const string CreateCompanyType = "CreateCompanyType";
        public const string EditCompanyType = "EditCompanyType";

        public const string IndexVehicleType = "IndexVehicleType";
        public const string CreateVehicleType = "CreateVehicleType";
        public const string EditVehicleType = "EditVehicleType";

        public const string IndexVehicleBrand = "IndexVehicleBrand";
        public const string CreateVehicleBrand = "CreateVehicleBrand";
        public const string EditVehicleBrand = "EditVehicleBrand";

        public const string IndexVehicleOwnership = "IndexVehicleOwnership";
        public const string CreateVehicleOwnership = "CreateVehicleOwnership";
        public const string EditVehicleOwnership = "EditVehicleOwnership";

        public const string IndexVehicle = "IndexVehicle";
        public const string CreateVehicle = "CreateVehicle";
        public const string EditVehicle = "EditVehicle";

        public const string IndexLocation = "IndexLocation";
        public const string CreateLocation = "CreateLocation";
        public const string EditLocation = "EditLocation";
    }
}