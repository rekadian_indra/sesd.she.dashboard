﻿namespace WebUI.Infrastructure
{
    public static class ActionSite
    {
        //reuseable action
        public const string Index = "Index";
        public const string Login = "Login";
        public const string Create = "Create";
        public const string Edit = "Edit";
        public const string Delete = "Delete";
        public const string Detail = "Detail";
        public const string Binding = "Binding";
        public const string BindingOption = "BindingOption";
        public const string PutOption = "PutOption";

        //default action
        public const string AssignRoles = "AssignRoles";
        public const string RevokeRoles = "RevokeRoles";
        public const string BindingRoles = "BindingRoles";
        public const string UserNameExists = "UserNameExists";
        public const string EmailExists = "EmailExists";
        public const string ModuleNameExists = "ModuleNameExists";
        public const string RoleNameExists = "RoleNameExists";
        public const string AssignRevokeModules = "AssignRevokeModules";
        public const string BindingActionsInModules = "BindingActionsInModules";
        public const string BindingModules = "BindingModules";
        public const string AssignRevokeRoles = "AssignRevokeRoles";

        //app action
        public const string NameExists = "NameExists";
        public const string LicensePlateHeaderExists = "LicensePlateHeaderExists";
        public const string LicensePlateNumberExists = "LicensePlateNumberExists";
        public const string LicensePlateTailExists = "LicensePlateTailExists";
        public const string BindingOwnhershipOption = "BindingOwnhershipOption";
        public const string BindingCompanyOption = "BindingCompanyOption";
        public const string BindingMstOrganizationInUsersInRoles = "BindingMstOrganizationInUsersInRoles";
    }
}