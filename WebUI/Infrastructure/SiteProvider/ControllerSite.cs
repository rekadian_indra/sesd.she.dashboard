﻿namespace WebUI.Infrastructure
{
    public static class ControllerSite
    {
        //reuseable controller
        public const string Home = "Home";
        public const string Dashboard = "Dashboard";

        //default controller
        public const string Authentication = "Authentication";
        public const string User = "User";
        public const string Role = "Role";
        public const string Module = "Module";
        public const string Mockup = "Mockup";

		//app controller
		public const string Region = "Region";
        public const string Site = "Site";
        public const string Location = "Location";
        public const string Organization = "Organization";
        public const string CompanyType = "CompanyType";
        public const string Company = "Company";
        public const string VehicleType = "VehicleType";
        public const string VehicleOwnership = "VehicleOwnership";
        public const string VehicleBrand = "VehicleBrand";
        public const string Vehicle = "Vehicle";
    }
}