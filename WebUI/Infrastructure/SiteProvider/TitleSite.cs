﻿namespace WebUI.Infrastructure
{
    public static class TitleSite
    {
        //default title
        public const string Dashboard = "Dashboard";
        public const string UserManagement = "User Management";
        public const string Create = "Tambah";
        public const string Edit = "Edit";
        public const string Breadcrumb = "Breadcrumb";
        public const string User = "User";
        public const string Role = "Role";
        public const string AssignRevokeModules = "Assign / Revoke Modules";
        public const string AssignRevokeRoles = "Assign / Revoke Roles";

        //app title
        public const string Region = "Region";
        public const string Site = "Site";
        public const string Organization = "Organization";
        public const string Company = "Company";
        public const string CompanyType = "Company Type";
        public const string VehicleType = "Vehicle Type";
        public const string VehicleBrand = "Vehicle Brand";
        public const string VehicleOwnership = "Vehicle Ownership";
        public const string Vehicle = "Vehicle";
        public const string Location = "Location";
    }
}