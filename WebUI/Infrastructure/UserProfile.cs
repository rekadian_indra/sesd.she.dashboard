﻿using Common.Enums;
using System.Web.Profile;
using System.Web.Security;

namespace WebUI.Infrastructure
{
    public class UserProfile : ProfileBase
    {
        [SettingsAllowAnonymous(false)]
        public string FullName
        {
            get { return base[Field.FullName.ToString()] as string; }
            set { base[Field.FullName.ToString()] = value; }
        }

        public static UserProfile CurrentUser
        {
            get
            {
                if (Membership.GetUser() != null)
                    return Create(Membership.GetUser().UserName) as UserProfile;
                else
                    return null;
            }
        }

        public static UserProfile GetUserProfile(string username)
        {
            return Create(username) as UserProfile;
        }
    }
}