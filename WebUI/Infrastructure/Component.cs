﻿namespace WebUI.Infrastructure
{
    public static class Component
    {
        public const string OptionLabel = "Pilih...";

        public static class Grid
        {
            public static class Command
            {
                //default command
                public const string Add = "Tambah";
                public const string Edit = "Edit";
                public const string Delete = "Hapus";
                public const string Detail = "Detail";
                public const string Role = "Assign / Revoke Role";
                public const string Modules = "Assign / Revoke Modules";

                //app command
            }
        }
    }
}