﻿namespace WebUI.Infrastructure
{
    public static class RouteValue
    {
        public const string Area = "area";
        public const string Controller = "controller";
        public const string Action = "action";
    }
}