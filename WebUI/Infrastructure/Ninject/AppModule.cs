﻿using Business.Abstract;
using Business.Concrete;
using LogAction.Abstract;
using LogAction.Concrete;
using Ninject.Modules;
using WebUI.Infrastructure.Abstract;
using WebUI.Infrastructure.Concrete;

namespace WebUI.Infrastructure
{
    public class AppModule : NinjectModule
    {
        public static NinjectModule[] Modules
        {
            get
            {
                return new[] { new AppModule() };
            }
        }

        public override void Load()
        {
            //TODO: Bind to Concrete Types Here            
            Kernel.Bind<ISiteRepository>().To<EFSiteRepository>();
            Kernel.Bind<IRegionRepository>().To<EFRegionRepository>();
            Kernel.Bind<IOrganizationRepository>().To<EFOrganizationRepository>();
            Kernel.Bind<IMstRegionRepository>().To<EFMstRegionRepository>();
            Kernel.Bind<IMstVehicleRepository>().To<EFMstVehicleRepository>();
            Kernel.Bind<IMstVehicleTypeRepository>().To<EFMstVehicleTypeRepository>();
            Kernel.Bind<IMstVehicleOwnershipRepository>().To<EFMstVehicleOwnershipRepository>();
            Kernel.Bind<IMstVehicleBrandRepository>().To<EFMstVehicleBrandRepository>();
            Kernel.Bind<IMstCompanyRepository>().To<EFMstCompanyRepository>();
            Kernel.Bind<IMstCompanyTypeRepository>().To<EFMstCompanyTypeRepository>();
            Kernel.Bind<ILocationRepository>().To<EFLocationRepository>();

            //user management
            Kernel.Bind<IApplicationRepository>().To<EFApplicationRepository>();
            Kernel.Bind<IUserRepository>().To<EFUserRepository>();
            Kernel.Bind<IRoleRepository>().To<EFRoleRepository>();
            Kernel.Bind<IModuleRepository>().To<EFModuleRepository>();
            Kernel.Bind<IActionRepository>().To<EFActionRepository>();
            Kernel.Bind<IModulesInRoleRepository>().To<EFModulesInRoleRepository>();
            Kernel.Bind<IMembershipRepository>().To<EFMembershipRepository>();
            Kernel.Bind<IUsersInRolesRepository>().To<EFUsersInRolesRepository>();

            //others
            Kernel.Bind<ILogRepository>().To<EFLogRepository>();
            Kernel.Bind<IAuthProvider>().To<DummyAuthProvider>();
        }
    }
}
