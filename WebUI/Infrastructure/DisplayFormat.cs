﻿using Business.Infrastructure;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace WebUI.Infrastructure
{
    public static class DisplayFormat
    {
        public const string FullDateFormat = "d MMMM yyyy"; //1 Januari 2014
        public const string MonthYearDateFormat = "MMMM yyyy"; //Januari 2014
        public const string CompactDateFormat = "M/d/yy"; //5/18/14
        public const string JavascriptDateFormat = "o"; //Jun 5, 2014 format yang bs diparse oleh javascript Date()
        public const string FullDateTimeFormat = "d MMMM yyyy (HH:mm)"; //1 Januari 2015 (12:58)
        public const string FullDayDateTimeFormat = "dddd, d MMMM yyyy (HH:mm)";
        public const string ShortDateFormat = "d MMM yyyy"; //1 Jan 2017
        public const string ShortDayMonthFormat = "d MMM"; //1 Jan
        public const string ShortDayMonthTimeFormat = "d MMM (HH:mm)"; //1 Jan (12:58)
        public const string ShortDayFormat = "d"; //1
        public const string ShortableDateFormat = "yyyyMMdd"; //20180131
        public const string SqlDateFormat = "yyyy-MM-dd"; //2014-01-31
        public const string SqlDateTimeFormat = "yyyy-MM-dd HH:mm:ss"; //2014-01-31 12:58:00
        public const string Iso8601DateTimeFormat = "yyyy-MM-ddTHH:mm:ss"; //2014-01-31T12:58:00

        public const string JsFullDateFormat = "{0:d MMMM yyyy}"; //1 Januari 2014
        public const string JsCompactDateFormat = "{0:d MMM yyyy}"; //1 Jan 2017
        public const string JsFullDateTimeFormat = "{0:d MMMM yyyy (HH:mm)}"; //1 Januari 2014 (12:58)
        public const string JsCompactDateTimeFormat = "{0:d MMM yyyy (HH:mm)}"; //1 Jan 2017 (12:58)
        public const string JsTimeFormat = "{0:HH:mm}"; //12:58
        public const string JsSqlDateFormat = "{0:yyyy-MM-dd}"; //2014-01-31
        public const string JsSqlDateTimeFormat = "{0:yyyy-MM-dd HH:mm:ss}"; //2014-01-31 12:58:00
        public const string JsIso8601DateTimeFormat = "{0:yyyy-MM-ddTHH:mm:ss}"; //2014-01-31T12:58:00

        /// <summary>
        /// Produces optional, URL-friendly version of a title, "like-this-one". 
        /// hand-tuned for speed, reflects performance refactoring contributed
        /// by John Gietzen (user otac0n) 
        /// </summary>
        public static string URLFriendly(string title)
        {
            if (title == null) return "";

            const int maxlen = 80;
            int len = title.Length;
            bool prevdash = false;
            var sb = new StringBuilder(len);
            char c;

            for (int i = 0; i < len; i++)
            {
                c = title[i];
                if ((c >= 'a' && c <= 'z') || (c >= '0' && c <= '9'))
                {
                    sb.Append(c);
                    prevdash = false;
                }
                else if (c >= 'A' && c <= 'Z')
                {
                    // tricky way to convert to lowercase
                    sb.Append((char)(c | 32));
                    prevdash = false;
                }
                else if (c == ' ' || c == ',' || c == '.' || c == '/' ||
                    c == '\\' || c == '-' || c == '_' || c == '=')
                {
                    if (!prevdash && sb.Length > 0)
                    {
                        sb.Append('-');
                        prevdash = true;
                    }
                }
                else if (c >= 128)
                {
                    int prevlen = sb.Length;
                    sb.Append(RemapInternationalCharToAscii(c));
                    if (prevlen != sb.Length) prevdash = false;
                }
                if (i == maxlen) break;
            }

            if (prevdash)
                return sb.ToString().Substring(0, sb.Length - 1);
            else
                return sb.ToString();
        }

        public static string RemapInternationalCharToAscii(char c)
        {
            string s = c.ToString().ToLowerInvariant();
            if ("àåáâäãåą".Contains(s))
            {
                return "a";
            }
            else if ("èéêëę".Contains(s))
            {
                return "e";
            }
            else if ("ìíîïı".Contains(s))
            {
                return "i";
            }
            else if ("òóôõöøőð".Contains(s))
            {
                return "o";
            }
            else if ("ùúûüŭů".Contains(s))
            {
                return "u";
            }
            else if ("çćčĉ".Contains(s))
            {
                return "c";
            }
            else if ("żźž".Contains(s))
            {
                return "z";
            }
            else if ("śşšŝ".Contains(s))
            {
                return "s";
            }
            else if ("ñń".Contains(s))
            {
                return "n";
            }
            else if ("ýÿ".Contains(s))
            {
                return "y";
            }
            else if ("ğĝ".Contains(s))
            {
                return "g";
            }
            else if (c == 'ř')
            {
                return "r";
            }
            else if (c == 'ł')
            {
                return "l";
            }
            else if (c == 'đ')
            {
                return "d";
            }
            else if (c == 'ß')
            {
                return "ss";
            }
            else if (c == 'Þ')
            {
                return "th";
            }
            else if (c == 'ĥ')
            {
                return "h";
            }
            else if (c == 'ĵ')
            {
                return "j";
            }
            else
            {
                return "";
            }
        }

        /// <summary>
        /// format filepath: ~/.../.../.../sample.ext
        /// </summary>
        /// <param name="fullPath"></param>
        /// <returns>sample.ext</returns>
        public static string GetFileNameFromFilePath(string fullPath)
        {
            string filename = "";
            string[] split = fullPath.Split('/');
            //split = split.Last().Split('.');
            //filename = split.FirstOrDefault();
            filename = split.Last();

            return filename;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="uniqueName">23b9b72b-9c81-4949-a51c-ceaae4bb4e73_getting-started.pdf</param>
        /// <returns>getting-started.pdf</returns>
        public static string GetFileNameFromUniqueFileName(string uniqueName)
        {
            string filename = "";
            List<string> split = uniqueName.Split('_').ToList();
            split.RemoveAt(0);
            filename = string.Join("", split);

            return filename;
        }

        public static List<SelectListItem> EnumToSelectList<T>(bool order = true)
        {
            Type type = typeof(T);
            IEnumerable<T> enums = EnumExtension.EnumToList<T>();
            List<SelectListItem> result = new List<SelectListItem>();

            foreach (T e in enums)
            {
                Enum en = (Enum)Enum.Parse(type, e.ToString());

                result.Add(new SelectListItem
                {
                    Text = en.ToDescription(),
                    Value = e.ToString()
                });
            }

            if (order)
                result = result.OrderBy(m => m.Text).ToList();

            return result;
        }

        public static void EnumSelectListFilterSortProccess(QueryRequestParameter param, ref IEnumerable<SelectListItem> options)
        {
            if (options == null || param == null)
                throw new ArgumentNullException();

            if (param.Filter != null && param.Filter.Filters.Any())
            {
                FilterQuery filter = param.Filter.Filters.FirstOrDefault();

                if (filter.Field.Value.Equals(Field.Text))
                {
                    FilterOperator @operator = filter.Operator.Value;
                    string value = filter.Value.ToString();

                    switch (@operator)
                    {
                        default:
                        case FilterOperator.Contains:
                            options = options.Where(m => m.Text.ToLower().Contains(value.ToLower()));

                            break;
                        case FilterOperator.StartsWith:
                            options = options.Where(m => m.Text.ToLower().StartsWith(value.ToLower()));

                            break;
                        case FilterOperator.EndsWith:
                            options = options.Where(m => m.Text.ToLower().EndsWith(value.ToLower()));

                            break;
                    }
                }
            }

            if (param.Sorts != null && param.Sorts.Any())
            {
                SortQuery sort = param.Sorts.FirstOrDefault();

                if (sort.Field.Value.Equals(Field.Text))
                {
                    SortOrder direction = sort.Direction.Value;

                    switch (direction)
                    {
                        default:
                        case SortOrder.Ascending:
                            options = options.OrderBy(m => m.Text);

                            break;
                        case SortOrder.Descending:
                            options = options.OrderByDescending(m => m.Text);

                            break;

                    }
                }
            }
        }

        public static NumberFormatInfo NumberFormat()
        {
            var format = (NumberFormatInfo)CultureInfo.InvariantCulture.NumberFormat.Clone();
            format.NumberGroupSeparator = ".";
            format.NumberDecimalSeparator = ",";
            return format;
        }

        public static string NumberFormat<T>(T s)
        {
            return string.Format("{0:n}", s);
        }

        public static string NumberFormatNoDecimal<T>(T s)
        {
            return string.Format("{0:n0}", s);
        }
    }
}
