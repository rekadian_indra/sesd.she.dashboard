﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.DirectoryServices;

namespace WebUI.Infrastructure
{
    public class ActiveDirectoryUtil
    {
        public bool IsAuthenticated { get; set; }
        public string UserName { get; set; }
        public List<string> Roles { get; set; }
        //public string FullName { get; set; }
        public string Email { get; set; }

        public ActiveDirectoryUtil()
        {
            Roles = new List<string>();
        }

        public void Login(string username, string password, bool rememberMe)
        {
            IsAuthenticated = false;

            string domain = ConfigurationManager.AppSettings["LDAPHost"].ToString();

            DirectoryEntry entry = new DirectoryEntry(domain, username, password, AuthenticationTypes.Secure);
            try
            {
                DirectorySearcher search = new DirectorySearcher(entry);
                string filter = string.Format("(&(ObjectClass={0})(sAMAccountName={1}))", "person", username);
                string[] properties = new string[] { "fullname", "cn", "mail" };
                //search.Filter = "(sAMAccountName=" + username + ")";

                //Select property in the server to identify roles
                search.SearchScope = SearchScope.Subtree;
                search.ReferralChasing = ReferralChasingOption.All;
                search.PropertiesToLoad.AddRange(properties);
                //search.PropertiesToLoad.Add("*");
                search.Filter = filter;

                //search.PropertiesToLoad.Add("cn");
                //search.PropertiesToLoad.Add("mail");
                SearchResult result = search.FindOne();

                if (result != null)
                {
                    Roles.Add((string)result.Properties["cn"][0]);
                    if (result.Properties["mail"] != null && result.Properties["mail"].Count > 0)
                        Email = (string)result.Properties["mail"][0];
                    else
                        Email = "-";
                    IsAuthenticated = true;
                }

            }
            catch (Exception ex)
            {
                IsAuthenticated = false;
            }

        }

        public static List<ActiveDirectoryUtil> GetUsers(string key)
        {
            List<ActiveDirectoryUtil> result = new List<ActiveDirectoryUtil>();
            string domain = ConfigurationManager.AppSettings["LDAPHost"].ToString();
            string username = ConfigurationManager.AppSettings["Username"].ToString();
            string password = ConfigurationManager.AppSettings["Password"].ToString();
            string email;

            DirectoryEntry entry = new DirectoryEntry(domain, username, password);
            string filter = "(&(sAMAccountName=*" + key + "*))";

            string[] strCats = { "mail", "sAMAccountName", "givenName" };
            List<string> items = new List<string>();
            DirectorySearcher dirComp = new DirectorySearcher(entry, filter, strCats, SearchScope.Subtree);
            //DirectorySearcher dirComp = new DirectorySearcher(entry);
            SearchResultCollection results = dirComp.FindAll();
            foreach (SearchResult data in results)
            {
                ActiveDirectoryUtil adUtil = new ActiveDirectoryUtil();
                if (data.Properties["mail"] != null && data.Properties["mail"].Count > 0)
                {
                    email = data.Properties["mail"][0].ToString();
                    adUtil.Email = string.IsNullOrEmpty(email) ? "-" : email;
                }
                if (data.Properties["sAMAccountName"] != null && data.Properties["sAMAccountName"].Count > 0)
                    adUtil.UserName = data.Properties["sAMAccountName"][0].ToString();

                //if (data.Properties["givenName"] != null && data.Properties["givenName"].Count > 0)
                //    adUtil.FullName = data.Properties["givenName"][0].ToString();

                result.Add(adUtil);
            }

            return result;
        }
    }
}