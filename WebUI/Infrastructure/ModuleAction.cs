﻿using Business.Abstract;
using Business.Concrete;
using Business.Entities;
using Business.Infrastructure;
using Common.Enums;
using System.Collections.Generic;
using System.Linq;

namespace WebUI.Infrastructure
{
    public class ModuleAction
    {
        public string ModuleName { get; set; }
        public string ActionName { get; set; }

        public ModuleAction()
        {
        }

        public ModuleAction(string moduleName, string actionName = null)
        {
            ModuleName = moduleName;
            ActionName = actionName;
        }

        public static List<ModuleAction> GetModuleActionForUser(string username)
        {
            //lib
            User user;
            IEnumerator<Role> roleEnumerator;
            IUserRepository repoUser = new EFUserRepository();
            FilterQuery filter = new FilterQuery(Field.UserName, FilterOperator.Equals, username);
            List<ModuleAction> result = new List<ModuleAction>();

            //algorithm
            user = repoUser.Find(null, filter);
            //roleEnumerator = user.Roles.GetEnumerator();
            roleEnumerator = user.UsersInRoles.Select(a => a.Role).GetEnumerator();

            while (roleEnumerator.MoveNext())
            {
                Role role = roleEnumerator.Current;
                ICollection<ModulesInRole> modulesInRoles = role.ModulesInRoles;
                IEnumerator<ModulesInRole> moduleInRoleEnumerator = modulesInRoles.GetEnumerator();

                while (moduleInRoleEnumerator.MoveNext())
                {
                    ModulesInRole modulesInRole = moduleInRoleEnumerator.Current;
                    string moduleName = modulesInRole.Module.ModuleName;
                    IEnumerable<ModuleAction> moduleActions = modulesInRole.Actions
                        .Select(p => new ModuleAction(moduleName, p.ActionName));

                    if (moduleActions.Any())
                        result.AddRange(moduleActions);
                    else
                        result.Add(new ModuleAction(moduleName));
                }
            }

            return result;
        }
    }
}