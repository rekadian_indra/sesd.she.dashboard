﻿namespace WebUI.Infrastructure
{
    public class ApiCredential
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}