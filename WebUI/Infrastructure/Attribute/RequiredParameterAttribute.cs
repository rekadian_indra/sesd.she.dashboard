﻿using Common.Enums;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;

/**
 * code by pazto (topaz@rekadia.co.id)
 * @2018-05-09
*/
public class RequiredParameterAttribute : ActionMethodSelectorAttribute
{

    public string[] ParameterNames { get; private set; }

    /// <summary>
    /// Set it to true to include GET (QueryStirng) parameters, false to exclude them:
    /// default is true.
    /// </summary>
    public bool IncludeGet { get; set; }

    /// <summary>
    /// Set it to true to include POST (Form) parameters, false to exclude them:
    /// default is true.
    /// </summary>
    public bool IncludePost { get; set; }

    /// <summary>
    /// Set it to true to include parameters from Cookies, false to exclude them:
    /// default is false.
    /// </summary>
    public bool IncludeCookies { get; set; }

    /// <summary>
    /// Use MatchMode.All to invalidate the method unless all the given parameters are set (default).
    /// Use MatchMode.Any to invalidate the method unless any of the given parameters is set.
    /// Use MatchMode.None to invalidate the method unless none of the given parameters is set.
    /// </summary>
    public MatchMode Mode { get; set; }

    public RequiredParameterAttribute(string parameterName) : this(new[] { parameterName })
    {
    }

    public RequiredParameterAttribute(params string[] parameterNames)
    {
        ParameterNames = parameterNames;
        IncludeGet = true;
        IncludePost = true;
        IncludeCookies = false;
        Mode = MatchMode.All;
    }

    public override bool IsValidForRequest(ControllerContext controllerContext, MethodInfo methodInfo)
    {
        switch (Mode)
        {
            case MatchMode.All:
            default:
                return (
                    (IncludeGet && ParameterNames.All(p => controllerContext.HttpContext.Request.QueryString.AllKeys.Contains(p)))
                    || (IncludePost && ParameterNames.All(p => controllerContext.HttpContext.Request.Form.AllKeys.Contains(p)))
                    || (IncludeCookies && ParameterNames.All(p => controllerContext.HttpContext.Request.Cookies.AllKeys.Contains(p)))
                    );
            case MatchMode.Any:
                return (
                    (IncludeGet && ParameterNames.Any(p => controllerContext.HttpContext.Request.QueryString.AllKeys.Contains(p)))
                    || (IncludePost && ParameterNames.Any(p => controllerContext.HttpContext.Request.Form.AllKeys.Contains(p)))
                    || (IncludeCookies && ParameterNames.Any(p => controllerContext.HttpContext.Request.Cookies.AllKeys.Contains(p)))
                    );
            case MatchMode.None:
                return (
                    (!IncludeGet || !ParameterNames.Any(p => controllerContext.HttpContext.Request.QueryString.AllKeys.Contains(p)))
                    && (!IncludePost || !ParameterNames.Any(p => controllerContext.HttpContext.Request.Form.AllKeys.Contains(p)))
                    && (!IncludeCookies || !ParameterNames.Any(p => controllerContext.HttpContext.Request.Cookies.AllKeys.Contains(p)))
                    );
        }
    }
}
