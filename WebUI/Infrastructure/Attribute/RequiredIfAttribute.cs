﻿using Common.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Reflection;
using System.Web.Mvc;

/**
* init code by pazto @2018-04-27
* topaz@rekadia.co.id
*/
namespace WebUI.Infrastructure
{
    public class RequiredIfAttribute : ValidationAttribute, IClientValidatable
    {
        protected RequiredAttribute _innerAttribute;
        protected FilterOperator _operator;

        public string DependentProperty { get; set; }
        public object TargetValue { get; set; }

        public object Operator
        {
            get
            {
                return _operator;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException();
                else if (value.GetType() != typeof(FilterOperator))
                    throw new InvalidCastException("Type must be UserModule");
                else
                {
                    FilterOperator @operator = (FilterOperator)value;

                    if (@operator != FilterOperator.Equals && @operator != FilterOperator.NotEquals)
                        throw new NotSupportedException($"Operator {@operator} not supported yet");
                    else
                        _operator = @operator;
                }
            }
        }

        public bool AllowEmptyStrings
        {
            get
            {
                return _innerAttribute.AllowEmptyStrings;
            }
            set
            {
                _innerAttribute.AllowEmptyStrings = value;
            }
        }

        public RequiredIfAttribute(string dependentProperty, object targetValue)
        {
            _innerAttribute = new RequiredAttribute();
            _operator = FilterOperator.Equals;
            DependentProperty = dependentProperty;
            TargetValue = targetValue;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            // get a reference to the property this validation depends upon
            bool needValidation = false;
            Type containerType = validationContext.ObjectInstance.GetType();
            PropertyInfo field = containerType.GetProperty(DependentProperty);

            if (field != null)
            {
                // get the value of the dependent property
                object dependentValue = field.GetValue(validationContext.ObjectInstance, null);

                // trim spaces of dependent value
                if (dependentValue != null && dependentValue is string)
                {
                    dependentValue = (dependentValue as string).Trim();

                    if (!AllowEmptyStrings && (dependentValue as string).Length == 0)
                    {
                        dependentValue = null;
                    }
                }

                // compare the value against the target value
                needValidation = (dependentValue == null && TargetValue == null) ||
                    (dependentValue != null && (TargetValue.Equals("*") || _operator == FilterOperator.Equals &&
                    dependentValue.ToString().ToLower().Equals(TargetValue.ToString().ToLower()))) ||
                    (dependentValue != null && (TargetValue.Equals("*") || _operator == FilterOperator.NotEquals &&
                    !dependentValue.ToString().ToLower().Equals(TargetValue.ToString().ToLower())));

                if (needValidation)
                {
                    // match => means we should try validating this field
                    if (!_innerAttribute.IsValid(value))
                        // validation failed - return an error
                        return new ValidationResult(FormatErrorMessage(validationContext.DisplayName),
                            new[] { validationContext.MemberName });
                }
            }

            return ValidationResult.Success;
        }

        public virtual IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            ModelClientValidationRule rule = new ModelClientValidationRule
            {
                ErrorMessage = FormatErrorMessage(metadata.GetDisplayName()),
                ValidationType = "requiredif",
            };

            string depProp = BuildDependentPropertyId(metadata, context as ViewContext);

            // find the value on the control we depend on;
            // if it's a bool, format it javascript style 
            // (the default is True or False!)
            string targetValue = (TargetValue ?? "").ToString().ToLower();

            rule.ValidationParameters.Add("dependentproperty", depProp);
            rule.ValidationParameters.Add("targetvalue", targetValue);
            rule.ValidationParameters.Add("operator", _operator.ToDescription());

            yield return rule;
        }

        private string BuildDependentPropertyId(ModelMetadata metadata, ViewContext viewContext)
        {
            // build the ID of the property
            string depProp = viewContext.ViewData.TemplateInfo.GetFullHtmlFieldId(DependentProperty);
            // unfortunately this will have the name of the current field appended to the beginning,
            // because the TemplateInfo's context has had this fieldname appended to it. Instead, we
            // want to get the context as though it was one level higher (i.e. outside the current property,
            // which is the containing object, and hence the same level as the dependent property.
            string thisField = metadata.PropertyName + "_";

            if (depProp.StartsWith(thisField))
                // strip it off again
                depProp = depProp.Substring(thisField.Length);

            return depProp;
        }
    }
}
