﻿using LogAction.Abstract;
using LogAction.Entities;
using System;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using WebUI.Controllers;

namespace WebUI.Infrastructure
{
    public class LogActionFilterAttribute : ActionFilterAttribute
    {
        /// <summary>
        /// uncomment to use log functionality
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //kamus
            BaseController controller = filterContext.Controller as BaseController;
            ILogRepository logRepo;
            Log log;

            //algoritma
            if (controller == null)
            {
                throw new InvalidOperationException("There is no YourProperty !!!");
            }
            else
            {
                logRepo = controller.RepoLog;

                log = GetLog(filterContext.RouteData, filterContext.HttpContext);
                logRepo.Save(log);
            }
        }

        private Log GetLog(RouteData routeData, HttpContextBase httpContext)
        {
            Log result;
            string application = "ProjectTemplate";
            string username = httpContext.User.Identity.Name;
            string controllerName = routeData.Values["controller"].ToString();
            string actionName = routeData.Values["action"].ToString();
            //string areaName = routeData.Values["area"].ToString();

            string ipAddress = httpContext.Request.UserHostAddress;
            string url = httpContext.Request.Url.PathAndQuery;
            string requestBody = "";

            using (StreamReader reader = new StreamReader(httpContext.Request.InputStream))
            {
                try
                {
                    httpContext.Request.InputStream.Position = 0;
                    requestBody = reader.ReadToEnd();
                }
                catch
                {
                    requestBody = string.Empty;
                    //log errors
                }
                finally
                {
                    httpContext.Request.InputStream.Position = 0;
                }

                if (requestBody.Contains("password"))
                {
                    requestBody = requestBody.Substring(0, requestBody.IndexOf("password"));
                }
            }

            result = new Log
            {
                Action = url,
                Application = application,
                Data = requestBody,
                Ip = ipAddress,
                TimeStamp = DateTime.Now,
                User = username
            };

            return result;
        }
    }
}