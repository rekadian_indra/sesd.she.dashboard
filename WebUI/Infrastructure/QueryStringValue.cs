﻿namespace WebUI.Infrastructure
{
    public static class QueryStringValue
    {
        public const string Args = "args";
        public const string Id = "id";
        public const string Url = "url";
        public const string ReturnUrl = "returnUrl";

        public const string CompanyType = "companyType";
        public const string VehicleType = "vehicleType";
        public const string Site = "site";
    }
}