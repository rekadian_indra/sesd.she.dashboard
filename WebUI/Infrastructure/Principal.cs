﻿using Common.Enums;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web.Security;
using WebUI.Models;

namespace WebUI.Infrastructure
{
    public class Principal : IPrincipal
    {
        public IIdentity Identity { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public List<string> Roles { get; set; }
        public List<ModuleAction> Modules { get; set; }

        private Principal()
        {
            Roles = new List<string>();
            Modules = new List<ModuleAction>();
        }

        public Principal(IIdentity identity) : this()
        {
            Identity = identity;
        }

        public Principal(Param param) : this(param.Identity)
        {
            UserId = param.User.ProviderUserKey.ToString();
            UserName = param.User.UserName;
            Email = param.User.Email;

            Roles.AddRange(param.Roles);
            Modules.AddRange(param.Modules);
        }

        public bool IsInRole(string role)
        {
            return IsInRole(role.ToEnum<UserRole>());
        }

        public bool IsInRole(UserRole? role)
        {
            return Identity != null && Identity.IsAuthenticated && role != null 
                && System.Web.Security.Roles.IsUserInRole(Identity.Name, role.ToDescription());
        }

        public bool HasAccess(params UserRole?[] roles)
        {
            //lib
            bool hasAccess = false;

            //algorithm
            if (roles != null && roles.Any())
            {
                hasAccess = Roles.Any(m => roles.Contains(m.ToEnum<UserRole>()));
            }

            return Identity != null && Identity.IsAuthenticated && hasAccess;
        }

        public bool HasAccess(params UserModule?[] modules)
        {
            //lib
            bool hasAccess = false;

            //algorithm
            if (modules != null && modules.Any())
            {
                hasAccess = Modules.Any(m => modules.Contains(m.ModuleName.ToEnum<UserModule>()));
            }

            return Identity != null && Identity.IsAuthenticated && hasAccess;
        }

        public class Param
        {
            public IIdentity Identity { get; set; }
            public MembershipUser User { get; set; }
            public List<string> Roles { get; set; }
            public List<ModuleAction> Modules { get; set; }
        }
    }
}