﻿using Business.Abstract;
using Business.Entities;
using Business.Entities.Views;
using Business.Extension;
using Business.Infrastructure;
using Common.Enums;
using LogAction.Abstract;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Controllers
{
    //[LogActionFilter]
    public abstract class BaseController : BaseController<object>
    {
    }

    //[LogActionFilter]
    public abstract class BaseController<TFormStub> : Controller
        where TFormStub : class
    {
        #region Global variables

        #endregion

        #region Properties

        public ILogRepository RepoLog { get; set; }
        public ISiteRepository RepoSite { get; set; }
        public IRegionRepository RepoRegion { get; set; }
        public IOrganizationRepository RepoOrganization { get; set; }
        public IUserRepository RepoUser { get; set; }
        public IRoleRepository RepoRole { get; set; }
        public IMembershipRepository RepoMembership { get; set; }
        public IMstRegionRepository RepoMstRegion { get; set; }
        public IModuleRepository RepoModule { get; set; }
        public IActionRepository RepoAction { get; set; }
        public IModulesInRoleRepository RepoModulesInRole { get; set; }

        public IApplicationRepository RepoApplication { get; set; }

        public IMstVehicleRepository RepoMasterVehicle { get; set; }
        public IMstVehicleTypeRepository RepoMasterVehicleType { get; set; }
        public IMstVehicleOwnershipRepository RepoMasterVehicleOwnership { get; set; }
        public IMstVehicleBrandRepository RepoMasterVehicleBrand { get; set; }
        public IMstCompanyRepository RepoMasterCompany { get; set; }
        public IMstCompanyTypeRepository RepoMasterCompanyType { get; set; }
        public IUsersInRolesRepository RepoUsersInRoles { get; set; }
        public ILocationRepository RepoLocation { get; set; }


        protected string AppAddress
        {
            get
            {
                string result = Request.ApplicationPath == "/" ?
                    string.Format("{0}://{1}/", Request.Url.Scheme, Request.Url.Authority) :
                    string.Format("{0}://{1}{2}/", Request.Url.Scheme, Request.Url.Authority, Request.ApplicationPath);
                return result;
            }
        }

        protected virtual new Principal User
        {
            get
            {
                return HttpContext.User as Principal;
            }
        }

        #endregion

        #region Constructor

        public BaseController()
        {
        }

        #endregion

        #region Main action

        public virtual async Task<ActionResult> Index(object args = null)
        {
            //remove dataSource options from session when current controller not equal session controller
            if (HttpContext.Session[AppSession.DataSourceOption] != null)
            {
                string currentController = RouteData.Values[RouteValue.Controller].ToString();
                string json = HttpContext.Session[AppSession.DataSourceOption].ToString();
                DataSourceOptionsStub model = JsonConvert.DeserializeObject<DataSourceOptionsStub>(json);

                if (model.Controller != currentController)
                {
                    ClearOption();
                }
            }

            await Task.Delay(0);

            if (args != null)
            {
                return View(args);
            }
            else
            {
                return View();
            }
        }

        public virtual async Task<ActionResult> Create()
        {
            await Task.Delay(0);
            throw new NotImplementedException();
        }

        [HttpPost]
        public virtual async Task<ActionResult> Create(TFormStub model)
        {
            await Task.Delay(0);
            throw new NotImplementedException();
        }

        public virtual async Task<ActionResult> Edit(params object[] id)
        {
            await Task.Delay(0);
            throw new NotImplementedException();
        }

        [HttpPost]
        public virtual async Task<ActionResult> Edit(TFormStub model)
        {
            await Task.Delay(0);
            throw new NotImplementedException();
        }

        [HttpPost]
        public virtual async Task<ActionResult> Delete(params object[] id)
        {
            await Task.Delay(0);
            throw new NotImplementedException();
        }

        public virtual async Task<ActionResult> Detail(params object[] id)
        {
            await Task.Delay(0);
            throw new NotImplementedException();
        }

        /**
         * Action to put dataSource options into session when in query event
        */
        public async Task<ActionResult> PutOption(DataSourceOptionsStub model)
        {
            if (model == null)
                throw new ArgumentNullException();

            if (string.IsNullOrEmpty(model.Controller))
                model.Controller = RouteData.Values[RouteValue.Controller].ToString();

            HttpContext.Session[AppSession.DataSourceOption] = JsonConvert.SerializeObject(model);

            await Task.Delay(0);

            return Json(true);
        }

        #endregion

        #region Binding

        public virtual async Task<string> Binding(params object[] args)
        {
            await Task.Delay(0);
            throw new NotImplementedException();
        }

        public async Task<string> BindingOption(OptionType? optionType, params object[] args)
        {
            //lib
            List<SelectListItem> options = new List<SelectListItem>();
            QueryRequestParameter param = QueryRequestParameter.Current;

            //algorithm
            switch (optionType)
            {
                case OptionType.REGION:
                    options = await RegionOptionAsync(param);
                    break;   
                case OptionType.SITE:
                    options = await SiteOptionAsync(param, args);
                    break;
                case OptionType.ORGANIZATION:
                    options = await OrganizationOptionAsync(param, args);
                    break;
                case OptionType.AD_USERS:
                    options = await ActiveDirectoryUsersOptionAsync(param);
                    break;
                case OptionType.MST_REGION:
                    options = await MstRegionOptionAsync(param);
                    break;
                case OptionType.APPLICATION:
                    options = await ApplicationOptionAsync(param);
                    break;
                case OptionType.COMPANY_TYPE:
                    options = await CompanyTypeOptionAsync(param);
                    break;
                case OptionType.VEHICLE_TYPE:
                    options = await VehicleTypeOptionAsync(param);
                    break;
                case OptionType.VEHICLE_BRAND:
                    options = await VehicleBrandOptionAsync(param);
                    break;
                case OptionType.COMPANY:
                    options = await CompanyOptionAsync(param, args);
                    break;
                case OptionType.VEHICLE_OWNERSHIP:
                    options = await OwnershipOptionAsync(param);
                    break;
                default:
                    return null;
            }


            return JsonConvert.SerializeObject(options);
        }
        
        #endregion

        #region Http404 handling

        protected override void HandleUnknownAction(string actionName)
        {
            // If controller is ErrorController dont 'nest' exceptions
            if (GetType() != typeof(ErrorController))
                InvokeHttp404(HttpContext);
        }

        public ActionResult InvokeHttp404(HttpContextBase httpContext)
        {
            IController errorController = DependencyResolver.Current.GetService<ErrorController>();
            RouteData errorRoute = new RouteData();

            errorRoute.Values.Add("controller", "Error");
            errorRoute.Values.Add("action", "Http404");
            errorRoute.Values.Add("url", httpContext.Request.Url.OriginalString);
            errorController.Execute(new RequestContext(httpContext, errorRoute));

            return new EmptyResult();
        }

        #endregion

        #region Event

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);

            ViewBag.AppAddress = AppAddress;
        }

        #endregion

        #region Helper
        protected virtual void ClearOption()
        {
            HttpContext.Session.Remove(AppSession.DataSourceOption);
        }
        protected virtual async Task<List<SelectListItem>> RegionOptionAsync(QueryRequestParameter param)
        {
            List<MstRegion> dbObjects = null;
            List<SelectListItem> models = new List<SelectListItem>();

            dbObjects = await RepoRegion.FindAllAsync(param.Sorts, param.Filter);

            if (dbObjects != null && dbObjects.Any())
            {
                foreach (MstRegion viewWell in dbObjects)
                {
                    SelectListItem m = new SelectListItem
                    {
                        Value = viewWell.Id.ToString(),
                        Text = viewWell.Name
                    };
                    models.Add(m);
                }
            }

            return models;
        }
        protected virtual async Task<List<SelectListItem>> SiteOptionAsync(QueryRequestParameter param, params object[] args)
        {
            //lib
            List<SelectListItem> options = new List<SelectListItem>();
            List<MstSite> dbObjects;
            param.Sorts.Add(new SortQuery(Field.Name, SortOrder.Ascending));

            //algorithm
            if (args.HasValue())
            {
                int? regionId = !string.IsNullOrEmpty(args.ElementAt(0).ToString()) ?
                              int.Parse(args.ElementAt(0).ToString()) : (int?)null;

                if (regionId != null)
                {
                    param.InstanceFilter();
                    param.Filter.AddFilter(Field.MstRegionId, FilterOperator.Equals, regionId);
                    dbObjects = await RepoSite.FindAllAsync(param.Sorts, param.Filter);

                    foreach (var s in dbObjects)
                    {
                        SelectListItem o = new SelectListItem
                        {
                            Text = s.Name,
                            Value = s.Id.ToString()
                        };

                        options.Add(o);
                    }
                }
            }
            else
            {
                dbObjects = await RepoSite.FindAllAsync(param.Sorts, param.Filter);

                foreach (var s in dbObjects)
                {
                    SelectListItem o = new SelectListItem
                    {
                        Text = s.Name,
                        Value = s.Id.ToString()
                    };

                    options.Add(o);
                }
            }

            return options;
        }
        protected virtual async Task<List<SelectListItem>> OrganizationOptionAsync(QueryRequestParameter param, params object[] args)
        {
            //lib
            List<SelectListItem> options = new List<SelectListItem>();
            List<MstOrganization> dbObjects;
            param.Sorts.Add(new SortQuery(Field.Name, SortOrder.Ascending));
            dbObjects = await RepoOrganization.FindAllAsync();
            
            //algorithm
            if (args.HasValue())
            {
                int? Id = !string.IsNullOrEmpty(args.ElementAt(0).ToString()) ?
                              int.Parse(args.ElementAt(0).ToString()) : (int?)null;

                if (Id != null)
                {
                    dbObjects = dbObjects.Where(o => o.MstSiteId == null || o.MstSiteId == Id).ToList();
                }
            }
            else
            {
                dbObjects = dbObjects.Where(o => o.MstSiteId == null).ToList();
            }

            if (dbObjects.Count > 0)
            {
                foreach (var s in dbObjects)
                {
                    SelectListItem o = new SelectListItem
                    {
                        Text = s.Name,
                        Value = s.Id.ToString()
                    };

                    options.Add(o);
                }
            }
            

            return options;
        }

        protected virtual async Task<List<SelectListItem>> ActiveDirectoryUsersOptionAsync(QueryRequestParameter param)
        {
            //lib
            List<SelectListItem> options = new List<SelectListItem>();
            FilterQuery filter = param.Filter != null && param.Filter.Filters.Any(n => n.Field == Field.UserName) ? param.Filter.Filters.FirstOrDefault(n => n.Field == Field.UserName) : null;
            string key = filter != null ? filter.Value.ToString() : string.Empty;
            IEnumerable<ActiveDirectoryUtil> listUsers = string.IsNullOrEmpty(key) ? new List<ActiveDirectoryUtil>() : ActiveDirectoryUtil.GetUsers(key);
            List<User> dbObjects = await RepoUser.FindAllAsync();

            listUsers = listUsers.Where(m => !dbObjects.Any(n => n.UserName.ToLower() == m.UserName.ToLower()));

            //algorithm
            foreach (ActiveDirectoryUtil u in listUsers)
            {
                SelectListItem o = new SelectListItem
                {
                    Text = u.UserName,
                    Value = u.Email
                };

                options.Add(o);
            }

            return options;
        }

        protected virtual async Task<List<SelectListItem>> MstRegionOptionAsync(QueryRequestParameter param)
        {
            //lib
            List<SelectListItem> options = new List<SelectListItem>();
            List<MstRegion> dbObjects = await RepoMstRegion.FindAllAsync(param.Sorts, param.Filter);
            foreach (MstRegion region in dbObjects)
            {
                SelectListItem o = new SelectListItem
                {
                    Value = region.Id.ToString(),
                    Text = region.Name
                };
                options.Add(o);
            }
            return options;
        }

        protected virtual async Task<List<SelectListItem>> ApplicationOptionAsync(QueryRequestParameter param)
        {
            //lib
            List<SelectListItem> options = new List<SelectListItem>();
            List<Application> dbObjects = await RepoApplication.FindAllAsync(param.Sorts, param.Filter);
            foreach (Application a in dbObjects)
            {
                SelectListItem o = new SelectListItem
                {
                    Value = a.ApplicationId.ToString(),
                    Text = a.ApplicationName
                };
                options.Add(o);
            }
            return options;
        }

        protected virtual async Task<List<SelectListItem>> CompanyTypeOptionAsync(QueryRequestParameter param)
        {
            List<MstCompanyType> dbObjects = null;
            List<SelectListItem> options = new List<SelectListItem>();

            param.Sorts.Add(new SortQuery(Field.Name, SortOrder.Ascending));

            dbObjects = await RepoMasterCompanyType.FindAllAsync(param.Sorts, param.Filter);

            if (dbObjects != null && dbObjects.Any())
            {
                foreach (MstCompanyType o in dbObjects)
                {
                    SelectListItem m = new SelectListItem
                    {
                        Value = o.Id.ToString(),
                        Text = o.Name
                    };
                    options.Add(m);
                }
            }

            return options;
        }

        protected virtual async Task<List<SelectListItem>> VehicleTypeOptionAsync(QueryRequestParameter param)
        {
            List<MstVehicleType> dbObjects = null;
            List<SelectListItem> options = new List<SelectListItem>();

            param.Sorts.Add(new SortQuery(Field.Name, SortOrder.Ascending));

            dbObjects = await RepoMasterVehicleType.FindAllAsync(param.Sorts, param.Filter);

            if (dbObjects != null && dbObjects.Any())
            {
                foreach (MstVehicleType o in dbObjects)
                {
                    SelectListItem m = new SelectListItem
                    {
                        Value = o.Id.ToString(),
                        Text = o.Name
                    };
                    options.Add(m);
                }
            }

            return options;
        }

        protected virtual async Task<List<SelectListItem>> VehicleBrandOptionAsync(QueryRequestParameter param)
        {
            List<MstVehicleBrand> dbObjects = null;
            List<SelectListItem> options = new List<SelectListItem>();

            param.Sorts.Add(new SortQuery(Field.Name, SortOrder.Ascending));

            dbObjects = await RepoMasterVehicleBrand.FindAllAsync(param.Sorts, param.Filter);

            if (dbObjects != null && dbObjects.Any())
            {
                foreach (MstVehicleBrand o in dbObjects)
                {
                    SelectListItem m = new SelectListItem
                    {
                        Value = o.Id.ToString(),
                        Text = o.Name
                    };
                    options.Add(m);
                }
            }

            return options;
        }

        protected virtual async Task<List<SelectListItem>> CompanyOptionAsync(QueryRequestParameter param, params object[] args)
        {
            List<MstCompany> dbObjects = null;
            List<SelectListItem> options = new List<SelectListItem>();
            param.Sorts.Add(new SortQuery(Field.Name, SortOrder.Ascending));

            if (args != null && args.Any())
            {
                int? companyTypeId = !string.IsNullOrEmpty(args.ElementAt(0).ToString()) ?
                     int.Parse(args.ElementAt(0).ToString()) : (int?)null;

                if (companyTypeId != null)
                {
                    param.InstanceFilter();
                    param.Filter.AddFilter(Field.MstCompanyTypeId, FilterOperator.Equals, companyTypeId);

                    dbObjects = await RepoMasterCompany.FindAllAsync(param.Sorts, param.Filter);

                    foreach (MstCompany o in dbObjects)
                    {
                        SelectListItem m = new SelectListItem
                        {
                            Value = o.Id.ToString(),
                            Text = o.Name
                        };
                        options.Add(m);
                    }
                }
            }
            else
            {
                dbObjects = await RepoMasterCompany.FindAllAsync(param.Sorts);

                foreach (MstCompany o in dbObjects)
                {
                    SelectListItem m = new SelectListItem
                    {
                        Value = o.Id.ToString(),
                        Text = o.Name
                    };
                    options.Add(m);
                }
            }

            return options;
        }

        protected virtual async Task<List<SelectListItem>> OwnershipOptionAsync(QueryRequestParameter param)
        {
            List<MstVehicleOwnership> dbObjects = null;
            List<SelectListItem> options = new List<SelectListItem>();

            param.Sorts.Add(new SortQuery(Field.Name, SortOrder.Ascending));

            dbObjects = await RepoMasterVehicleOwnership.FindAllAsync(param.Sorts, param.Filter);

            if (dbObjects != null && dbObjects.Any())
            {
                foreach (MstVehicleOwnership o in dbObjects)
                {
                    SelectListItem m = new SelectListItem
                    {
                        Value = o.Id.ToString(),
                        Text = o.Name
                    };
                    options.Add(m);
                }
            }

            return options;
        }
        #endregion
    }
}
