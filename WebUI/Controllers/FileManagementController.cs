﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Mvc;
using WebUI.Infrastructure;

namespace WebUI.Controllers
{
    public class FileManagementController : Controller
    {
        /**
         * save file using kendo file uploader
         * menangani kalau nama file sama
         * The Name of the Upload component is "files"
         * currently not support multiple uploads
         * 
         * @return filepath ex "~/Uploads/rekadia.jpg"
         */
        public string Save(IEnumerable<HttpPostedFileBase> files)
        {
            //lib
            string relativePath = "";
            string friendlyFilename = "";

            //algorithm
            if (files != null)
            {
                foreach (var file in files)
                {
                    string serverPath;
                    Guid identifier = Guid.NewGuid();
                    string fileName = Path.GetFileNameWithoutExtension(file.FileName);

                    friendlyFilename = $"{DisplayFormat.URLFriendly(fileName)}{Path.GetExtension(file.FileName)}";
                    fileName = $"{identifier} {friendlyFilename}";
                    relativePath = $"{DirectoryHelper.Upload}{fileName}";

                    //save file
                    serverPath = Path.Combine(Server.MapPath(DirectoryHelper.Upload), fileName);
                    file.SaveAs(serverPath);
                }
            }

            return JsonConvert.SerializeObject(new
            {
                filePath = relativePath,
                fileName = friendlyFilename,
                absolutePath = VirtualPathUtility.ToAbsolute(relativePath)
            });
        }

        /**
         * remove file using kendo file uploader
         */
        public ActionResult Remove(string[] fileNames)
        {
            // The parameter of the Remove action must be called "fileNames"

            if (fileNames != null)
            {
                foreach (string fullName in fileNames)
                {
                    string fileName = Path.GetFileName(fullName);
                    string physicalPath = Path.Combine(Server.MapPath(DirectoryHelper.Upload), fileName);

                    if (System.IO.File.Exists(physicalPath))
                    {
                        // The files are not actually removed in this demo
                        System.IO.File.Delete(physicalPath);
                    }
                }
            }

            // Return an empty string to signify success
            return Content("");
        }
    }
}
