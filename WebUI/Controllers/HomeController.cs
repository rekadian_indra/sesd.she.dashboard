﻿using LogAction.Abstract;
using MvcSiteMapProvider;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace WebUI.Controllers
{
    [Authorize]
    public class HomeController : BaseController
    {
        public HomeController(ILogRepository repoLog)
        {
            RepoLog = repoLog;
        }

        //if using ModelBinder to get user login use SGUser parameter
        //if using Principal dont get user login SGUser parameter
        //public ActionResult Index(SGUser user)
        //{
        //}

        [MvcSiteMapNode(Title = "<span class=\"fa fa-home\"></span>", Key = "Dashboard", ParentKey ="Home")]
        public override async Task<ActionResult> Index(object args = null)
        {
            return await base.Index();
        }
    }
}
