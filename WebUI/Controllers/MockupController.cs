﻿using LogAction.Abstract;
using MvcSiteMapProvider;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace WebUI.Controllers
{
    [Authorize]
    public class MockupController : BaseController
    {
        public MockupController(ILogRepository repoLog)
        {
            RepoLog = repoLog;
        }

        [MvcSiteMapNode(Title = "<span class=\"fa fa-home\"></span>", Key = "Mockup", ParentKey ="Home")]
        public override async Task<ActionResult> Index(object args = null)
        {
            return await base.Index();
		}

		[MvcSiteMapNode(Title = "My Responsibilities", Key = "ViewResponsibilitiesMockup", ParentKey = "Home")]
		public ActionResult ViewResponsibilities()
		{
			return View();
		}

	}
}
