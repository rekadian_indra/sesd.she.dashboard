﻿using SecurityGuard.Interfaces;
using SecurityGuard.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801
    public class MvcApplication : HttpApplication
    {
        private static Dictionary<string, Principal> _principals = new Dictionary<string, Principal>();

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            NinjectMvcContainer.RegisterModules(AppModule.Modules);
            NinjectApiContainer.RegisterModules(AppModule.Modules);

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            ClientDataTypeModelValidatorProvider.ResourceClassKey = "AppGlobalError";
            DefaultModelBinder.ResourceClassKey = "AppGlobalError";
            DataAnnotationsModelValidatorProvider.AddImplicitRequiredAttributeForValueTypes = false;

            //if Principal is set, ModelBinders useless
            //ModelBinders.Binders.Add(typeof(Business.Models.SGUser), new UserLoginModelBinder());
        }

        private void Application_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();

            if (ex is HttpAntiForgeryException)
            {
                HttpContext httpContext = HttpContext.Current;
                string user = User.Identity.Name;

                Response.Clear();
                Server.ClearError(); //make sure you log the exception first
                
                //handle for logout
                if (string.IsNullOrEmpty(user))
                    Response.Redirect(FormsAuthentication.LoginUrl, true);
                else
                {
                    httpContext.Response.Redirect("~/Error/AntiForgery", true);
                }
            }
        }

        protected void Application_PreRequestHandlerExecute(object sender, EventArgs e)
        {
            //init
            LogHelper lh = new LogHelper();
            HttpCookie authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
            
            if (authCookie != null)
            {
                FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);

                if (!authTicket.Expired)
                {
                    InitUser(authTicket);

                    //use this if user management from service
                    //InitServiceUser(authTicket);
                }
                else
                {
                    SignOut();
                    return;
                }
            }
        }

        public static void RemoveUserCache(string key)
        {
            if (string.IsNullOrEmpty(key))
                return;

            _principals.Remove(key);
        }

        private void InitUser(FormsAuthenticationTicket authTicket)
        {
            //lib
            Principal principal;
            Principal.Param param;
            IMembershipService membershipService;
            string[] roles;
            List<ModuleAction> modules;
            KeyValuePair<string, Principal> cache;

            //algorithm
            if (Context.Session != null && Context.Session[AppSession.UserLogin] != null)
            {
                Context.User = Context.Session[AppSession.UserLogin] as Principal;
            }
            else
            {
                cache = _principals.FirstOrDefault(m => m.Key == authTicket.Name);
                
                if (cache.Value != null)
                {
                    //get cache and set HttpContext user
                    Context.User = cache.Value;
                }
                else
                {
                    param = new Principal.Param();
                    membershipService = new MembershipService(Membership.Provider);
                    roles = Roles.GetRolesForUser(authTicket.Name);
                    modules = ModuleAction.GetModuleActionForUser(authTicket.Name);

                    //set param
                    param.Identity = User.Identity;
                    param.User = membershipService.GetUser(authTicket.Name);
                    param.Roles = roles.Any() ? roles.ToList() : new List<string>();
                    param.Modules = modules;

                    //set principal
                    principal = new Principal(param);

                    //set session user
                    if (Context.Session != null)
                        Context.Session[AppSession.UserLogin] = principal;

                    //set principal to memory
                    if (_principals != null && _principals.Any(m => m.Key == authTicket.Name))
                        _principals.Remove(authTicket.Name);

                    _principals.Add(authTicket.Name, principal);

                    //set HttpContext user
                    Context.User = principal;
                }
            }

            //clean up user cache
            CleanUpPricipalAsync();
        }

        private void InitServiceUser(FormsAuthenticationTicket authTicket)
        {
            //if (HttpContext.Current.Session != null)
            //{
            //examples code get user from service
            //try
            //{
            //    //kamus
            //    HttpServiceHelper serviceHelper = new HttpServiceHelper();
            //    string url, responseText;

            //    //algoritma
            //    if (ConfigurationManager.AppSettings["UserManagementURL"] != null)
            //    {
            //        userManagementUrl = ConfigurationManager.AppSettings["UserManagementURL"].ToString();
            //    }
            //    if (ConfigurationManager.AppSettings["ApiKey"] != null)
            //    {
            //        apiKey = ConfigurationManager.AppSettings["ApiKey"].ToString();
            //    }

            //    url = string.Format("{0}/User/GetAccess?applicationId={1}&userName={2}", userManagementUrl, apiKey, authTicket.Name);
            //    responseText = serviceHelper.HttpGet(url);
            //    moduleActionList = serviceHelper.ConvertJsonTextToObject<ServiceResponseStub<ModuleAction>>(responseText);
            //    //lh.Write(url);
            //    //lh.Write(responseText);

            //    if (moduleActionList.status == 200)
            //    {
            //        ApplicationPrincipal newUser = new ApplicationPrincipal(authTicket.Name);
            //        HttpContext.Current.Session["ModuleAction"] = moduleActionList.data;
            //        HttpContext.Current.Session["AuthName"] = authTicket.Name;
            //        newUser.Modules = HttpContext.Current.Session["ModuleAction"] as List<ModuleAction>;
            //        newUser.UserName = authTicket.Name;

            //        url = userManagementUrl + "/User/GetRoles?applicationId=" + apiKey + "&userName=" + authTicket.Name;
            //        responseText = serviceHelper.HttpGet(url);
            //        ServiceResponseStub<string> roles = serviceHelper.ConvertJsonTextToObject<ServiceResponseStub<string>>(responseText);
            //        HttpContext.Current.Session["Roles"] = roles.data;
            //        newUser.Roles = HttpContext.Current.Session["Roles"] as List<string>;

            //        url = userManagementUrl + "/User/GetByUsername?applicationId=" + apiKey + "&userName=" + authTicket.Name;
            //        responseText = serviceHelper.HttpGet(url);
            //        ServiceResponseStub<SGUser> users = serviceHelper.ConvertJsonTextToObject<ServiceResponseStub<SGUser>>(responseText);
            //        if (users.status == 200)
            //        {
            //            Session["fullname"] = users.data.ElementAt(0).fullName;
            //            Session["user"] = users.data.ElementAt(0);
            //        }

            //        HttpContext.Current.User = newUser;
            //    }
            //    else
            //    {
            //        SignOut();
            //    }
            //}
            //catch (WebException err)
            //{
            //    lh.Write(err.Message);
            //    SignOut();
            //}
            //}
        }

        private void CleanUpPrincipal()
        {
            //find user cache where ticket expiration <= now
            IEnumerable<KeyValuePair<string, Principal>> deleteList = _principals
                .Where(m => 
                    m.Key != null && m.Value != null 
                    && ((FormsIdentity)m.Value.Identity).Ticket.Expiration <= DateTime.Now
                );

            foreach (var m in deleteList)
            {
                _principals.Remove(m.Key);
            }
        }

        private Task CleanUpPricipalAsync()
        {
            return Task.Run(() => CleanUpPrincipal());
        }

        private void SignOut()
        {
            if (Context.User != null)
                RemoveUserCache(Context.User.Identity.Name);

            Session.Clear();
            Response.Cookies.Clear();
            FormsAuthentication.SignOut();
            Response.Redirect(FormsAuthentication.LoginUrl, true);
        }
    }
}