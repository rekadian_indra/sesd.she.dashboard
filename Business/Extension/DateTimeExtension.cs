﻿using Business.Infrastructure;
using System;
using System.Globalization;

namespace Business.Extension
{
    public static class DateTimeExtension
    {
        public static DateTime ToLocalDateTime(this DateTime dateTime)
        {
            DateTimeOffset dto = new DateTimeOffset(DateTime.SpecifyKind(dateTime, DateTimeKind.Utc));
            DateTime? dt = new DateTime?(dto.LocalDateTime);

            return dt.Value;
        }

        public static DateTime ToUtcDateTime(this DateTime dateTime)
        {
            DateTimeOffset dto = new DateTimeOffset(DateTime.SpecifyKind(dateTime, DateTimeKind.Local));
            DateTime? dt = new DateTime?(dto.UtcDateTime);

            return dt.Value;
        }
    }
}
