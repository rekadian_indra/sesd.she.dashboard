﻿using Business.Infrastructure;
using System;
using System.Globalization;

/**
 * code by pazto (topaz@rekadia.co.id)
 * @2018-05-09
*/
namespace Business.Extension
{
    public static class ObjectExtension
    {
        public static int ToInteger(this object args)
        {
            return int.Parse(args.ToString());
        }

        public static long ToLong(this object args)
        {
            return long.Parse(args.ToString());
        }

        public static short ToShort(this object args)
        {
            return short.Parse(args.ToString());
        }

        public static double ToDouble(this object args)
        {
            return double.Parse(args.ToString());
        }

        public static float ToFloat(this object args)
        {
            return float.Parse(args.ToString());
        }

        public static byte ToByte(this object args)
        {
            return byte.Parse(args.ToString());
        }

        public static bool ToBoolean(this object args)
        {
            return bool.Parse(args.ToString());
        }

        public static Guid ToGuid(this object args)
        {
            return Guid.Parse(args.ToString());
        }

        public static DateTime ToDateTime(this object args)
        {
            try
            {
                DateTime dt;
                string s = args.ToString();

                if (s.Length >= 24)
                    dt = DateTime.ParseExact(s.Substring(0, 24), DateTimeFormat.CompactDayDateTimeFormat, CultureInfo.InvariantCulture);
                else
                    dt = DateTime.Parse(s);

                return dt;
            }
            catch
            {
                throw new ArgumentException("Invalid DateTime format");
            }
        }
    }
}
