﻿using System.Collections.Generic;

/**
 * code by pazto (topaz@rekadia.co.id)
 * @2018-05-09
*/
namespace System.Linq
{
    public static class EnumerableExtension
    {
        //
        // Summary:
        //     Determines whether a sequence contains any elements.
        //
        // Parameters:
        //   source:
        //     The System.Collections.Generic.IEnumerable`1 to check for emptiness.
        //
        // Type parameters:
        //   TSource:
        //     The type of the elements of source.
        //
        // Returns:
        //     true if the source sequence contains any elements; otherwise, false.
        //
        // Exceptions:
        //   T:System.ArgumentNullException:
        //     source is null.
        public static bool HasValue<T>(this IEnumerable<T> source)
        {
            return source != null && source.Any();
        }

        //
        // Summary:
        //     Determines whether any element of a sequence satisfies a condition.
        //
        // Parameters:
        //   source:
        //     An System.Collections.Generic.IEnumerable`1 whose elements to apply the predicate
        //     to.
        //
        //   predicate:
        //     A function to test each element for a condition.
        //
        // Type parameters:
        //   TSource:
        //     The type of the elements of source.
        //
        // Returns:
        //     true if any elements in the source sequence pass the test in the specified predicate;
        //     otherwise, false.
        //
        // Exceptions:
        //   T:System.ArgumentNullException:
        //     source or predicate is null.
        public static bool HasValue<T>(this IEnumerable<T> source, Func<T, bool> predicate)
        {
            return source != null && source.Any(predicate);
        }

        public static T NextOf<T>(this IList<T> list, T item)
        {
            var indexOf = list.IndexOf(item);

            return list[indexOf == list.Count - 1 ? 0 : indexOf + 1];
        }
    }
}
