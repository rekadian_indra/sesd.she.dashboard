﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;

namespace Business.Infrastructure
{
    public class ObjectMapper
    {
        private delegate void CopyPublicPropertiesDelegate<T, U>(T source, U target);

        private readonly Dictionary<string, object> _del;

        public ObjectMapper()
        {
            _del = new Dictionary<string, object>();
        }

        /// <summary>
        /// Melakukan pemetaan objek antara dua kelas jika tipe dan nama fieldnya identik. Jika terdapat field yang tidak identik, maka field tersebut tidak akan dipetakan.
        /// </summary>
        /// <typeparam name="T">Tipe yang mengenkapsulasi source.</typeparam>
        /// <typeparam name="U">Tipe yang mengenkapsulasi target.</typeparam>
        /// <param name="source">Object source.</param>
        /// <param name="target">Object target.</param>
        public static void MapObject<T, U>(object source, object target)
            where T : class
            where U : class
        {
            var instance = new ObjectMapper();
            instance.MapTypes<T, U>();
            instance.Copy((T)source, (U)target);
        }

        private void MapTypes<T, U>()
        {
            var key = GetMapKey<T, U>();
            if (_del.ContainsKey(key))
                return;

            var source = typeof(T);
            var target = typeof(U);

            var args = new[] { source, target };
            var mod = GetType().Module;

            var dm = new DynamicMethod(key, null, args, mod);
            var il = dm.GetILGenerator();
            var maps = GetMatchingProperties<T, U>();

            foreach (var map in maps)
            {
                il.Emit(OpCodes.Ldarg_1);
                il.Emit(OpCodes.Ldarg_0);
                il.EmitCall(OpCodes.Callvirt,
                            map.SourceProperty.GetGetMethod(), null);
                il.EmitCall(OpCodes.Callvirt,
                            map.TargetProperty.GetSetMethod(), null);
            }
            il.Emit(OpCodes.Ret);
            var del = dm.CreateDelegate(
                      typeof(CopyPublicPropertiesDelegate<T, U>));
            _del.Add(key, del);
        }

        private void Copy<T, U>(T source, U target)
        {
            var key = GetMapKey<T, U>();
            var del = (CopyPublicPropertiesDelegate<T, U>)_del[key];
            del.Invoke(source, target);
        }

        private IList<PropertyMap> GetMatchingProperties<T, U>()
        {
            var sourceProperties = typeof(T).GetProperties();
            var targetProperties = typeof(U).GetProperties();

            var properties = (from s in sourceProperties
                              from t in targetProperties
                              where s.Name == t.Name &&
                                    s.CanRead &&
                                    t.CanWrite &&
                                    s.PropertyType == t.PropertyType
                              select new PropertyMap
                              {
                                  SourceProperty = s,
                                  TargetProperty = t
                              }).ToList();
            return properties;
        }

        private string GetMapKey<T, U>()
        {
            var className = "Copy_";
            className += typeof(T).FullName.Replace(".", "_");
            className += "_";
            className += typeof(U).FullName.Replace(".", "_");

            return className;
        }

        private class PropertyMap
        {
            public PropertyInfo SourceProperty { get; set; }
            public PropertyInfo TargetProperty { get; set; }
        }
    }
}
