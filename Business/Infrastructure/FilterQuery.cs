﻿using Common.Enums;
using System.Collections.Generic;
using System.Linq;

namespace Business.Infrastructure
{
    public class FilterQuery
    {
        public FilterOperator? Operator { get; set; }
        public Field? Field { get; set; }
        public FieldHelper RelationField { get; set; }
        public object Value { get; set; }
        public List<FilterQuery> Filters { get; private set; }
        public FilterLogic? Logic { get; set; }

        public FilterQuery(FilterLogic? logic = null, bool isParent = true)
        {
            Filters = new List<FilterQuery>();

            if (logic == null)
            {
                if (isParent)
                    Logic = FilterLogic.And;
            }
            else
                Logic = logic;
        }

        public FilterQuery(Field? field, FilterOperator? @operator, object value = null, FilterLogic? logic = null) : this(logic)
        {
            AddFilter(field, @operator, value);
        }

        public FilterQuery(FieldHelper relationField, FilterOperator? @operator, object value = null, FilterLogic? logic = null) : this(logic)
        {
            AddFilter(relationField, @operator, value);
        }

        public void AddFilter(Field? field, FilterOperator? @operator, object value = null)
        {
            FilterQuery filter = new FilterQuery(null, false);

            filter.Field = field;
            filter.Operator = @operator;

            if (value != null)
                filter.Value = value;

            Filters.Add(filter);
        }

        public void AddFilter(FieldHelper relationField, FilterOperator? @operator, object value = null)
        {
            FilterQuery filter = new FilterQuery(null, false);

            filter.RelationField = relationField;
            filter.Operator = @operator;

            if (value != null)
                filter.Value = value;

            Filters.Add(filter);
        }

        public void AddFilter(FilterQuery filter)
        {
            Filters.Add(filter);
        }

        public static FilterOperator? ParseOperator(string @operator)
        {
            switch (@operator)
            {
                //equal ==
                case "eq":
                case "==":
                case "isequalto":
                case "equals":
                case "equalto":
                case "equal":
                    return FilterOperator.Equals;
                //not equal !=
                case "ne":
                case "neq":
                case "!=":
                case "isnotequalto":
                case "notequals":
                case "notequalto":
                case "notequal":
                    return FilterOperator.NotEquals;
                // Greater
                case "gt":
                case ">":
                case "isgreaterthan":
                case "greaterthan":
                case "greater":
                    return FilterOperator.Greater;
                // Greater or equal
                case "ge":
                case "gte":
                case ">=":
                case "isgreaterthanorequalto":
                case "greaterthanequal":
                    return FilterOperator.GreaterThanOrEquals;
                // Less
                case "lt":
                case "<":
                case "islessthan":
                case "lessthan":
                case "less":
                    return FilterOperator.LessThan;
                // Less or equal
                case "le":
                case "lte":
                case "<=":
                case "islessthanorequalto":
                case "lessthanequal":
                    return FilterOperator.LessThanOrEquals;
                case "startswith":
                    return FilterOperator.StartsWith;
                case "endswith":
                    return FilterOperator.EndsWith;
                //string.Contains()
                case "contains":
                    return FilterOperator.Contains;
                case "doesnotcontain":
                    return FilterOperator.NotContains;
                case "isnull":
                    return FilterOperator.IsNull;
                case "isnotnull":
                    return FilterOperator.IsNotNull;
                default:
                    return null;
            }
        }

        public static FilterLogic? ParseLogic(string logic)
        {
            switch (logic)
            {
                //and &&
                case "and":
                case "&&":
                    return FilterLogic.And;
                //or ||
                case "or":
                case "||":
                    return FilterLogic.Or;
                default:
                    return null;
            }
        }

        /**
         * mengubah atribut field di dalam FilterInfo
         * field yang tadinya bernilai == original, diubah menjadi modification
         */
        public void ReplaceField(Field original, Field modification)
        {
            if (Field == original)
                Field = modification;

            if (Filters != null)
            {
                int i = 0;

                while (i < Filters.Count())
                {
                    Filters[i].ReplaceField(original, modification);
                    ++i;
                }
            }
        }

        /**
         * mengubah atribut field helper di dalam FilterInfo
         * field yang tadinya bernilai == original, diubah menjadi modification
         */
        public void ReplaceField(FieldHelper original, FieldHelper modification)
        {
            if (RelationField.Is(original))
                RelationField = modification;

            if (Filters != null)
            {
                int i = 0;

                while (i < Filters.Count())
                {
                    Filters[i].ReplaceField(original, modification);
                    ++i;
                }
            }
        }

        /**
         * menghapus filter dengan field tertentu
         * @return null kalau tidak ditemukan, atau object FilterQuery dari field tsb
         */
        public FilterQuery RemoveFilter(Field field)
        {
            //lib
            FilterQuery filter = null;

            //algorithm
            if (Filters != null)
            {
                filter = Filters.FirstOrDefault(m => m.Field != null && m.Field == field);

                if (filter != null)
                {
                    Filters.Remove(filter);
                }
            }

            return filter;
        }

        /**
         * menghapus filter dengan relation field tertentu
         * @return null kalau tidak ditemukan, atau object FilterQuery dari relation field tsb
         */
        public FilterQuery RemoveFilter(FieldHelper relationField)
        {
            //lib
            FilterQuery filter = null;

            //algorithm
            if (Filters != null)
            {
                filter = Filters.FirstOrDefault(m => m.RelationField != null && m.RelationField.Is(relationField));

                if (filter != null)
                {
                    Filters.Remove(filter);
                }
            }

            return filter;
        }

        public FilterQuery Clone()
        {
            FilterQuery clone = new FilterQuery
            {
                Field = Field,
                Operator = Operator,
                Value = Value,
                RelationField = RelationField,
                Logic = Logic
            };

            if (Filters.Any())
            {
                foreach (FilterQuery f in Filters)
                {
                    clone.Filters.Add(f.Clone());
                }
            }

            return clone;
        }

        /**
         * mengubah sentence case menjadi underscore
         * mis: ContractorId -> contractor_id
         */
        //public void FormatFieldToUnderscore()
        //{
        //dikomen karena tipe data Field sudah bukan string lg
        //if (Field != null)
        //    Field = System.Text.RegularExpressions.Regex.Replace(Field, @"(\p{Ll})(\p{Lu})", "$1_$2");

        //if (Filters != null)
        //{
        //    int i = 0;
        //    while (i < Filters.Count())
        //    {
        //        Filters[i].FormatFieldToUnderscore();
        //        ++i;
        //    }
        //}
        //}
    }
}
