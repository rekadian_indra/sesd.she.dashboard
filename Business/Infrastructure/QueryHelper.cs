﻿using Business.Extension;
using Business.Linq;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Business.Infrastructure
{
    public static class QueryHelper
    {
        private static FilterLogic? MainLogic { get; set; }

        public static void FilterProcess<T>(FilterQuery filter, ref IQueryable<T> queryable)
        {
            ThrowIfFilterNotValid(filter);

            //lib
            string whereClause = string.Empty;
            List<object> parameters = new List<object>();

            //algorithm
            CreateWhereClause<T>(filter, ref parameters, ref whereClause);

            if (whereClause.Length > 2)
            {
                for (int i = 0; i < parameters.Count(); i++)
                {
                    if (parameters[i].GetType() == typeof(string))
                        parameters[i] = ((string)parameters[i]).ToLower();
                }

                queryable = queryable.Where(whereClause, parameters.ToArray());
            }
        }

        public static void SortProcess<T>(List<SortQuery> sorts, ref IQueryable<T> queryable)
        {
            string sortClause;
            List<string> sortClauses = new List<string>();

            foreach (SortQuery s in sorts)
            {
                ThrowIfSortNotValid(s);

                if (s.Field != null)
                    sortClauses.Add($"{s.Field} {ToLinqOrder(s.Direction)}");
                else
                    sortClauses.Add($"{s.RelationField.Parse} {ToLinqOrder(s.Direction)}");
            }

            sortClause = string.Join(", ", sortClauses);
            queryable = queryable.OrderBy(sortClause);
        }

        private static void CreateWhereClause<T>(
            FilterQuery filter,
            ref List<object> parameters,
            ref string whereClause,
            bool level2 = false
        )
        {
            //lib
            List<FilterQuery> filters = filter.Filters;

            //algorithm
            if (filters != null && filters.Any())
            {
                if (!level2 && MainLogic == null)
                    MainLogic = filter.Logic;

                if (level2)
                {
                    whereClause += whereClause.EndsWith(")") ? $"{ToLinqLogic(MainLogic)}(" : "(";
                }

                for (int i = 0; i < filters.Count; i++)
                {
                    FilterQuery fq = filters[i];

                    if (fq.Filters == null || !fq.Filters.Any())
                    {
                        if (!level2 && whereClause.EndsWith(")"))
                            whereClause += ToLinqLogic(filter.Logic);

                        whereClause += $"{BuildWhereClause<T>(fq, parameters)}{ToLinqLogic(filter.Logic)}";

                        if (i == (filters.Count - 1))
                        {
                            CleanUp(ref whereClause);

                            if (level2)
                                whereClause += ")";
                        }
                    }
                    else
                    {
                        if (level2)
                            throw new ArgumentException("FilterQuery cannot more than two levels.");

                        CreateWhereClause<T>(fq, ref parameters, ref whereClause, true);
                    }
                }
            }
        }

        private static string BuildWhereClause<T>(FilterQuery filter, List<object> parameters)
        {
            ThrowIfFilterNotValid(filter);

            //lib
            string field;
            int paramIndex = parameters.Count;
            PropertyInfo property = null;
            Type entityType = (typeof(T));
            Type valueType = filter.Value.GetType();

            //algorithm
            if (filter.RelationField != null)
            {
                string[] fields = filter.RelationField.Parse.Split('.');

                foreach (string s in fields)
                {
                    property = entityType.GetProperty(s);
                    entityType = property.PropertyType;
                }

                field = filter.RelationField.Parse;
            }
            else
            {
                property = entityType.GetProperty(filter.Field.ToString());
                field = filter.Field.ToString();
            }

            switch (filter.Operator)
            {
                case FilterOperator.Equals:
                case FilterOperator.NotEquals:
                case FilterOperator.GreaterThanOrEquals:
                case FilterOperator.Greater:
                case FilterOperator.LessThanOrEquals:
                case FilterOperator.LessThan:
                    if (property != null)
                    {
                        if ((typeof(DateTime).IsAssignableFrom(property.PropertyType)) || (typeof(DateTime?).IsAssignableFrom(property.PropertyType)))
                        {
                            DateTime dt;

                            if (valueType == typeof(DateTime) || (valueType == typeof(DateTime?) && filter.Value != null))
                            {
                                dt = ((DateTime)filter.Value);
                            }
                            else
                            {
                                dt = filter.Value.ToDateTime();
                            }

                            parameters.Add(dt.Date);

                            return $"DbFunctions.TruncateTime({field}){ToLinqOperator(filter.Operator)}@{paramIndex}";
                        }

                        if (typeof(int).IsAssignableFrom(property.PropertyType) || typeof(int?).IsAssignableFrom(property.PropertyType))
                        {
                            if (valueType == typeof(int) || valueType == typeof(int?))
                                parameters.Add(filter.Value);
                            else
                                parameters.Add(filter.Value.ToInteger());

                            return $"{field}{ToLinqOperator(filter.Operator)}@{paramIndex}";
                        }

                        if (typeof(long).IsAssignableFrom(property.PropertyType) || typeof(long?).IsAssignableFrom(property.PropertyType))
                        {
                            if (valueType == typeof(long) || valueType == typeof(long?))
                                parameters.Add(filter.Value);
                            else
                                parameters.Add(filter.Value.ToLong());

                            return $"{field}{ToLinqOperator(filter.Operator)}@{paramIndex}";
                        }

                        if (typeof(short).IsAssignableFrom(property.PropertyType) || typeof(short?).IsAssignableFrom(property.PropertyType))
                        {
                            if (valueType == typeof(short) || valueType == typeof(short?))
                                parameters.Add(filter.Value);
                            else
                                parameters.Add(filter.Value.ToShort());

                            return $"{field}{ToLinqOperator(filter.Operator)}@{paramIndex}";
                        }

                        if (typeof(double).IsAssignableFrom(property.PropertyType) || typeof(double?).IsAssignableFrom(property.PropertyType))
                        {
                            if (valueType == typeof(double) || valueType == typeof(double?))
                                parameters.Add(filter.Value);
                            else
                                parameters.Add(filter.Value.ToDouble());

                            return $"{field}{ToLinqOperator(filter.Operator)}@{paramIndex}";
                        }

                        if (typeof(float).IsAssignableFrom(property.PropertyType) || typeof(float?).IsAssignableFrom(property.PropertyType))
                        {
                            if (valueType == typeof(float) || valueType == typeof(float?))
                                parameters.Add(filter.Value);
                            else
                                parameters.Add(filter.Value.ToFloat());

                            return $"{field}{ToLinqOperator(filter.Operator)}@{paramIndex}";
                        }

                        if (typeof(byte).IsAssignableFrom(property.PropertyType) || typeof(byte?).IsAssignableFrom(property.PropertyType))
                        {
                            if (valueType == typeof(byte) || valueType == typeof(byte?))
                                parameters.Add(filter.Value);
                            else
                                parameters.Add(filter.Value.ToByte());

                            return $"{field}{ToLinqOperator(filter.Operator)}@{paramIndex}";
                        }

                        if (typeof(bool).IsAssignableFrom(property.PropertyType) || typeof(bool?).IsAssignableFrom(property.PropertyType))
                        {
                            if (valueType == typeof(bool) || valueType == typeof(bool?))
                                parameters.Add(filter.Value);
                            else
                                parameters.Add(filter.Value.ToBoolean());

                            return $"{field}{ToLinqOperator(filter.Operator)}@{paramIndex}";
                        }

                        if (typeof(Guid).IsAssignableFrom(property.PropertyType) || typeof(Guid?).IsAssignableFrom(property.PropertyType))
                        {
                            if (valueType == typeof(Guid) || valueType == typeof(Guid?))
                                parameters.Add(filter.Value);
                            else
                                parameters.Add(filter.Value.ToGuid());

                            return $"{field}{ToLinqOperator(filter.Operator)}@{paramIndex}";
                        }

                        if (typeof(DateTimeOffset).IsAssignableFrom(property.PropertyType) || typeof(DateTimeOffset?).IsAssignableFrom(property.PropertyType))
                        {
                            DateTime dt;
                            DateTimeOffset dto;

                            if (valueType == typeof(DateTimeOffset) || (valueType == typeof(DateTimeOffset?) && filter.Value != null))
                            {
                                dto = (DateTimeOffset)filter.Value;
                                dto.ToUniversalTime();
                            }
                            else
                            {
                                if (valueType == typeof(DateTime) || valueType == typeof(DateTime) && filter.Value != null)
                                {
                                    dt = (DateTime)filter.Value;
                                }
                                else
                                {
                                    dt = filter.Value.ToDateTime();
                                }

                                dt = DateTime.SpecifyKind(dt, DateTimeKind.Local);
                                dto = new DateTimeOffset(dt);
                                dto.ToUniversalTime();

                            }

                            parameters.Add(dto.Date);
                            return $"DbFunctions.TruncateTime({field.ToString()}){ToLinqOperator(filter.Operator)}@{paramIndex}";
                        }
                    }

                    parameters.Add(filter.Value.ToString());
                    return $"{field}.ToLower(){ToLinqOperator(filter.Operator)}@{paramIndex}";
                case FilterOperator.StartsWith:
                    parameters.Add(filter.Value.ToString());
                    return $"{field}.StartsWith(@{paramIndex})";
                case FilterOperator.EndsWith:
                    parameters.Add(filter.Value.ToString());
                    return $"{field}.EndsWith(@{paramIndex})";
                case FilterOperator.Contains:
                    parameters.Add(filter.Value.ToString());
                    return $"{field}.ToLower().Contains(@{paramIndex})";
                case FilterOperator.NotContains:
                    parameters.Add(filter.Value.ToString());
                    return $"!{field}.Contains(@{paramIndex})";
                case FilterOperator.IsNull:
                    return $"{field} == null";
                case FilterOperator.IsNotNull:
                    return $"{field} != null";
                default:
                    throw new ArgumentException("This operator is not yet supported for this Grid", filter.Operator.ToString());
            }
        }

        private static string CleanUp(ref string whereClause)
        {
            whereClause = whereClause.TrimEnd();

            if (whereClause.EndsWith("&&") || whereClause.EndsWith("||"))
                whereClause = whereClause.Remove(whereClause.Length - 3);

            return whereClause;
        }

        private static string ToLinqOperator(FilterOperator? @operator)
        {
            switch (@operator)
            {
                case FilterOperator.Equals: return " == ";
                case FilterOperator.NotEquals: return " != ";
                case FilterOperator.GreaterThanOrEquals: return " >= ";
                case FilterOperator.Greater: return " > ";
                case FilterOperator.LessThanOrEquals: return " <= ";
                case FilterOperator.LessThan: return " < ";
                default: return null;
            }
        }

        private static string ToLinqLogic(FilterLogic? logic)
        {
            switch (logic)
            {
                case FilterLogic.Or: return " || ";
                case FilterLogic.And: return " && ";
                default: return null;
            }
        }

        private static string ToLinqOrder(SortOrder? order)
        {
            switch (order)
            {
                case SortOrder.Ascending: return "asc";
                case SortOrder.Descending: return "desc";
                default: return null;
            }
        }

        private static void ThrowIfFilterNotValid(FilterQuery filter)
        {
            if (filter.Field != null && filter.RelationField != null)
                throw new ArgumentException("Choose either between Field or RelationField");
        }

        private static void ThrowIfSortNotValid(SortQuery sort)
        {
            if (sort.Field != null && sort.RelationField != null)
                throw new ArgumentException("Choose either between Field or RelationField");
        }
    }
}
