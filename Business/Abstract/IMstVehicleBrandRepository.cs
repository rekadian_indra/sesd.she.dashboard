﻿using Business.Entities;

namespace Business.Abstract
{
    public interface IMstVehicleBrandRepository : IRepository<MstVehicleBrand>
    {
    }
}
