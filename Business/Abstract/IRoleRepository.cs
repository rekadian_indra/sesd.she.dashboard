﻿using Business.Entities;

namespace Business.Abstract
{
    public interface IRoleRepository : IRepository<Role>
    {
        Role FindByName(string roleName);
        void AddModuleAndAction(string[] modules,string role);
    }
}
