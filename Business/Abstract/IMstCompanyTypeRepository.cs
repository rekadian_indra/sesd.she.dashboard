﻿using Business.Entities;

namespace Business.Abstract
{
    public interface IMstCompanyTypeRepository : IRepository<MstCompanyType>
    {
    }
}
