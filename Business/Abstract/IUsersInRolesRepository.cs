﻿using Business.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.Abstract
{
    public interface IUsersInRolesRepository : IRepository<UsersInRole>
    {
    }
}
