﻿using Business.Entities;

namespace Business.Abstract
{
    public interface IMstCompanyRepository : IRepository<MstCompany>
    {
    }
}
