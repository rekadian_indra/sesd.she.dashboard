﻿using Business.Entities;

namespace Business.Abstract
{
    public interface IMstVehicleOwnershipRepository : IRepository<MstVehicleOwnership>
    {
    }
}
