﻿using Business.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.Abstract
{
    public interface IUserRepository : IRepository<User>
    {
        void RemoveProfile(User dbItem);
        //void AssignRole(User user, List<Role> roles);
        //void RevokeRole(User user, List<Role> roles);

        Task RemoveProfileAsync(User dbItem);
        //Task AssignRoleAsync(User user, List<Role> roles);
        //Task RevokeRoleAsync(User user, List<Role> roles);
    }
}
