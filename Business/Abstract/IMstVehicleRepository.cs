﻿using Business.Entities;

namespace Business.Abstract
{
    public interface IMstVehicleRepository : IRepository<MstVehicle>
    {
    }
}
