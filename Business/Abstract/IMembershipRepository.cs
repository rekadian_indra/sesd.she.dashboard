﻿using Business.Entities;

namespace Business.Abstract
{
    public interface IMembershipRepository : IRepository<Membership>
    {

    }
}
