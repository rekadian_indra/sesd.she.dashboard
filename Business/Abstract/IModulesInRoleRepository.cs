﻿using Business.Entities;
using System;
using System.Collections.Generic;

namespace Business.Abstract
{
    public interface IModulesInRoleRepository : IRepository<ModulesInRole>
    {
        ModulesInRole FindByRoleAndModule(Guid roleId, Guid moduleId);
        void RemoveAction(Guid moduleId,Guid actionId);
        void DeleteByModule(Guid moduleId);
        void DeleteByRole(Guid roleId);

        void AssignChoosenActions(ModulesInRole modulesInRole, List<Guid> actionIds);
        void RevokeChoosenActions(ModulesInRole modulesInRole, List<Guid> actionIds);
    }
}
