﻿using Business.Entities;

namespace Business.Abstract
{
    public interface ISiteRepository : IRepository<MstSite>
    {
    }
}
