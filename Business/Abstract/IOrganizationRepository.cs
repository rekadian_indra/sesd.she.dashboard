﻿using Business.Entities;

namespace Business.Abstract
{
    public interface IOrganizationRepository : IRepository<MstOrganization>
    {
    }
}
