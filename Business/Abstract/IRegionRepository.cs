﻿using Business.Entities;

namespace Business.Abstract
{
    public interface IRegionRepository : IRepository<MstRegion>
    {
    }
}
