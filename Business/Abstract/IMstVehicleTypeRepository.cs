﻿using Business.Entities;

namespace Business.Abstract
{
    public interface IMstVehicleTypeRepository : IRepository<MstVehicleType>
    {
    }
}
