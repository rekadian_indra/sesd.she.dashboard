﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Business.Entities
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class AppEntities : DbContext
    {
        public AppEntities()
            : base("name=AppEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<MstCompany> MstCompanies { get; set; }
        public virtual DbSet<MstCompanyType> MstCompanyTypes { get; set; }
        public virtual DbSet<MstDriver> MstDrivers { get; set; }
        public virtual DbSet<MstLocation> MstLocations { get; set; }
        public virtual DbSet<MstOrganization> MstOrganizations { get; set; }
        public virtual DbSet<MstRegion> MstRegions { get; set; }
        public virtual DbSet<MstSite> MstSites { get; set; }
        public virtual DbSet<MstVehicle> MstVehicles { get; set; }
        public virtual DbSet<MstVehicleBrand> MstVehicleBrands { get; set; }
        public virtual DbSet<MstVehicleOwnership> MstVehicleOwnerships { get; set; }
        public virtual DbSet<MstVehicleType> MstVehicleTypes { get; set; }
    }
}
