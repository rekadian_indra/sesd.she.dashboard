﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Business.Entities.Views
{
    public class ViewCrew : IView
    {
        public string CrewId { get; set; }
        [Key] //Must have one key for default sorting or first property to be set key
        public string CrewName { get; set; }
        public string Status { get; set; }
        public DateTime RegisterDate { get; set; }
        public int? CompanyId { get; set; }
        public string CompanyName { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDateTimeUtc { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDateTimeUtc { get; set; }
    }
}
