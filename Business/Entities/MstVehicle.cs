//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Business.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class MstVehicle
    {
        public int Id { get; set; }
        public Nullable<int> MstVehicleTypeId { get; set; }
        public string LicensePlateHeader { get; set; }
        public string LicensePlateNumber { get; set; }
        public string LicensePlateTail { get; set; }
        public Nullable<int> MstVehicleOwnershipId { get; set; }
        public Nullable<int> OwnerMstCompanyId { get; set; }
        public Nullable<int> MstVehicleBrandId { get; set; }
        public Nullable<int> VehicleYear { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDateTimeUtc { get; set; }
        public string ModifiedBy { get; set; }
        public System.DateTime ModifiedDateTimeUtc { get; set; }
        public bool IsDeleted { get; set; }
    
        public virtual MstCompany MstCompany { get; set; }
        public virtual MstVehicleBrand MstVehicleBrand { get; set; }
        public virtual MstVehicleOwnership MstVehicleOwnership { get; set; }
        public virtual MstVehicleType MstVehicleType { get; set; }
    }
}
