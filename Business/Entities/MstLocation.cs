//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Business.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class MstLocation
    {
        public int Id { get; set; }
        public int SiteId { get; set; }
        public string Name { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDateTimeUtc { get; set; }
        public string ModifiedBy { get; set; }
        public System.DateTime ModifiedDateTimeUtc { get; set; }
        public bool IsDeleted { get; set; }
    
        public virtual MstSite MstSite { get; set; }
    }
}
