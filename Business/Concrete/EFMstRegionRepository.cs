﻿using Business.Abstract;
using Business.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using Business.Linq;

namespace Business.Concrete
{
    public class EFMstRegionRepository : EFBaseRepository<MstRegion, AppEntities>, IMstRegionRepository
    {
        public EFMstRegionRepository() : base(true)
        {
        }
    }
}
