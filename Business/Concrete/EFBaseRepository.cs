﻿using Business.Abstract;
using Business.Entities;
using Business.Entities.Views;
using Business.Extension;
using Business.Infrastructure;
using Business.Linq;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public abstract class EFBaseRepository<TEntity> : EFBaseRepository<TEntity, AppEntities>, IRepository<TEntity>
        where TEntity : class
    {
        #region Constructors

        protected EFBaseRepository(bool deletedFlag = false) : base(deletedFlag)
        {
            DeletedFlag = deletedFlag;
        }

        #endregion
    }

    public abstract class EFBaseRepository<TEntity, TContext> : IRepository<TEntity>
        where TEntity : class
        where TContext : DbContext, new()
    {
        #region Constans

        protected const int COMMIT_COUNT = 100;

        #endregion

        #region Variables

        private TContext _context;
        private bool _disposed;
        
        #endregion

        #region Properties

        protected TContext Context
        {
            get
            {
                if (_context == null)
                    _context = new TContext();

                return _context;
            }
            set
            {
                _context = value;
            }
        }

        protected DbSet<TEntity> EntitySet
        {
            get
            {
                return Context.Set<TEntity>();
            }
        }

        protected bool IsView
        {
            get
            {
                Type type = typeof(TEntity);
                Type iType = typeof(IView);

                return type.GetInterface(iType.Name) != null;
            }
        }

        protected bool DeletedFlag { get; set; }

        public IQueryable<TEntity> Queryable
        {
            get
            {
                if (IsView)
                    return GenerateView();
                else
                    return EntitySet;
            }
        }

        #endregion

        #region Constructors

        protected EFBaseRepository(bool deletedFlag = false)
        {
            DeletedFlag = deletedFlag;
        }

        #endregion

        #region Synchronous

        public virtual List<TEntity> FindAll(FilterQuery filter)
        {
            return FindAll(null, filter);
        }

        public virtual List<TEntity> FindAll(List<SortQuery> sorts)
        {
            return FindAll(sorts, null);
        }

        public virtual List<TEntity> FindAll(List<SortQuery> sorts, FilterQuery filter)
        {
            return FindAll(null, null, sorts, filter);
        }

        public virtual List<TEntity> FindAll(
            int? skip = null, 
            int? take = null, 
            List<SortQuery> sorts = null, 
            FilterQuery filter = null)
        {
            ThrowIfDisposed();

            //lib
            IQueryable<TEntity> queryable = Queryable;
            FilterQuery copyFilter = null;

            //algorithm
            if (filter != null)
                copyFilter = filter.Clone();

            CustomFilterProcess(copyFilter, ref queryable);
            FilterSortProcess(sorts, copyFilter, ref queryable);
            
            //skip
            if (skip.HasValue)
            {
                queryable = queryable.Skip(skip.Value);
            }

            //take
            if (take.HasValue)
            {
                queryable = queryable.Take(take.Value);
            }

            return queryable.ToList();
        }

        public virtual TEntity Find(FilterQuery filter)
        {
            return Find(null, filter);
        }

        public virtual TEntity Find(List<SortQuery> sorts = null, FilterQuery filter = null)
        {
            ThrowIfDisposed();
            
            //lib
            IQueryable<TEntity> queryable = Queryable;

            //algorithm
            FilterSortProcess(sorts, filter, ref queryable);

            return queryable.FirstOrDefault();
        }

        public virtual TEntity FindByPrimaryKey(params object[] keys)
        {
            ThrowIfDisposed();
            ThrowIfView();

            return EntitySet.Find(keys);
        }
        
        public virtual int Count(FilterQuery filter = null)
        {
            ThrowIfDisposed();

            //lib
            IQueryable<TEntity> queryable = Queryable;
            FilterQuery copyFilter = null;

            //algorithm
            if (filter != null)
                copyFilter = filter.Clone();

            CustomFilterProcess(copyFilter, ref queryable);
            FilterSortProcess(null, copyFilter, ref queryable);

            return queryable.Count();
        }

        public virtual bool Save(TEntity dbItem)
        {
            ThrowIfDisposed();
            ThrowIfView();

            //lib
            TEntity entity = CheckEntity(dbItem);

            //algorithm
            if (entity == null)
            {
                EntitySet.Add(dbItem);
            }
            else
            {
                //if get error Attaching an entity of type failed because another entity
                //of the same type already has the same primary key value, don't set EntityState.Detached.
                //make sure to get object from EF, not create new instance of object before mapping with model.

                Context.Entry(dbItem).State = EntityState.Modified;
            }

            return Context.SaveChanges() > 0;
        }

        public virtual void SaveAll(IEnumerable<TEntity> dbItems, bool autoDetectChanges = true)
        {
            ThrowIfDisposed();
            ThrowIfView();

            //lib
            int counter = 0;

            //algorithm

            //if autoDetectChanges set false entity cannot changes value of the relation,
            //but get max performace to save data
            Context.Configuration.AutoDetectChangesEnabled = autoDetectChanges;

            foreach (var dbItem in dbItems)
            {
                ++counter;

                TEntity entity = CheckEntity(dbItem);

                if (entity == null)
                {
                    EntitySet.Add(dbItem);
                }
                else
                {
                    Context.Entry(entity).State = EntityState.Detached;

                    EntitySet.Attach(dbItem);
                    Context.Entry(dbItem).State = EntityState.Modified;
                }

                if (counter % COMMIT_COUNT == 0)
                {
                    Context.SaveChanges();
                }
            }

            Context.SaveChanges();

            if (!Context.Configuration.AutoDetectChangesEnabled)
                Context.Configuration.AutoDetectChangesEnabled = true;
        }

        public virtual bool Delete(TEntity dbItem)
        {
            ThrowIfDisposed();
            ThrowIfView();

            //lib
            TEntity entity = CheckEntity(dbItem);

            //algorithm
            if (entity != null)
            {
                if (DeletedFlag && typeof(TEntity).GetProperty(Field.IsDeleted.ToString()) != null)
                {
                    entity.SetPropertyValue(Field.IsDeleted.ToString(), true);
                    Context.Entry(entity).State = EntityState.Modified;
                }
                else
                {
                    EntitySet.Remove(entity);
                }
            }

            return Context.SaveChanges() > 0;
        }

        public virtual void DeleteAll(IEnumerable<TEntity> dbItems, bool autoDetectChanges = true)
        {
            ThrowIfDisposed();
            ThrowIfView();

            //lib
            int counter = 0;

            //algorithm

            //if autoDetectChanges set false entity cannot changes value of the relation,
            //but get max performace to save data
            Context.Configuration.AutoDetectChangesEnabled = autoDetectChanges;

            foreach (var dbItem in dbItems)
            {
                TEntity entity = CheckEntity(dbItem);

                ++counter;

                if (entity != null)
                {
                    if (DeletedFlag)
                    {
                        entity.SetPropertyValue(Field.IsDeleted.ToString(), true);
                        Context.Entry(entity).State = EntityState.Modified;
                    }
                    else
                    {
                        EntitySet.Remove(entity);
                    }
                }

                if (counter % COMMIT_COUNT == 0)
                {
                    Context.SaveChanges();
                }
            }

            Context.SaveChanges();

            if (!Context.Configuration.AutoDetectChangesEnabled)
                Context.Configuration.AutoDetectChangesEnabled = true;
        }

        #endregion

        #region Asynchronous

        public virtual Task<List<TEntity>> FindAllAsync(FilterQuery filter)
        {
            return Task.FromResult(FindAll(filter));
        }

        public virtual Task<List<TEntity>> FindAllAsync(List<SortQuery> sorts)
        {
            return Task.FromResult(FindAll(sorts));
        }

        public virtual Task<List<TEntity>> FindAllAsync(List<SortQuery> sorts, FilterQuery filter)
        {
            return Task.FromResult(FindAll(sorts, filter));
        }

        public virtual Task<List<TEntity>> FindAllAsync(
            int? skip = null,
            int? take = null,
            List<SortQuery> sorts = null,
            FilterQuery filter = null)
        {
            return Task.FromResult(FindAll(skip, take, sorts, filter));
        }

        public virtual Task<TEntity> FindAsync(FilterQuery filter)
        {
            return Task.FromResult(Find(filter));
        }

        public virtual Task<TEntity> FindAsync(List<SortQuery> sorts = null, FilterQuery filter = null)
        {
            return Task.FromResult(Find(sorts, filter));
        }

        public virtual Task<TEntity> FindByPrimaryKeyAsync(params object[] keys)
        {
            return Task.FromResult(FindByPrimaryKey(keys));
        }

        public virtual Task<int> CountAsync(FilterQuery filter = null)
        {
            return Task.FromResult(Count(filter));
        }

        public virtual Task<bool> SaveAsync(TEntity dbItem)
        {
            return Task.FromResult(Save(dbItem));
        }

        public virtual Task SaveAllAsync(IEnumerable<TEntity> dbItems, bool autoDetectChanges = true)
        {
            SaveAll(dbItems, autoDetectChanges);

            return Task.FromResult(0);
        }

        public virtual Task<bool> DeleteAsync(TEntity dbItem)
        {
            return Task.FromResult(Delete(dbItem));
        }

        public virtual Task DeleteAllAsync(IEnumerable<TEntity> dbItems, bool autoDetectChanges = true)
        {
            DeleteAll(dbItems, autoDetectChanges);

            return Task.FromResult(0);
        }

        #endregion

        #region Helper

        /// <summary>
        ///     Dispose repository. 
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        ///     Jika dalam posisi disposing, dispose akan memanggil Context. Nilai Context menjadi null.
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing && Context != null)
            {
                Context.Dispose();
            }

            _disposed = true;
            Context = null;
        }

        protected void ThrowIfDisposed()
        {
            if (_disposed)
            {
                throw new ObjectDisposedException(GetType().Name);
            }
        }

        protected virtual void FilterSortProcess<T>(
            List<SortQuery> sorts, 
            FilterQuery filter,
            ref IQueryable<T> queryable)
            where T : class
        {
            //filter data with delete flag
            if (DeletedFlag)
            {
                if (filter == null)
                    filter = new FilterQuery();

                if (typeof(T).GetProperty(Field.IsDeleted.ToString()) != null)
                {
					if (!filter.Filters.Any(x => x.Field == Field.IsDeleted))
					{
                        filter.AddFilter(Field.IsDeleted, FilterOperator.Equals, false);
					}
                }
            }

            //filter entity using queryable
            if (filter != null && (filter.Field != null || (filter.Filters != null && filter.Filters.Any())))
            {
                QueryHelper.FilterProcess(filter, ref queryable);
            }

            //sorting entity using queryable
            if (sorts != null && sorts.Any())
            {
                QueryHelper.SortProcess(sorts, ref queryable);
            }
            else
            {
                //Default sorting must exist or EF error
                string primaryKey;

                if (IsView)
                {
                    primaryKey = EntityExtension.GetKeyFieldView<T>();
                }
                else
                {
                    primaryKey = Context.GetPrimaryKeyField<T>();
                }

                queryable = queryable.OrderBy($"{primaryKey} desc");
            }
        }

        protected virtual T CheckEntity<T>(T dbItem)
            where T : class
        {
            //lib
            bool exist;
            DbSet<T> entitySet = Context.Set<T>();
            int keyCount = Context.CountPrimaryKey<T>();
            IEnumerable<Type> keyTypes = Context.GetPrimaryKeyTypes<T>();
            IEnumerable<object> keyValues = Context.GetPrimaryKeyValues(dbItem);
            T entity = null;

            //algorithm
            if (keyTypes.Any(m => m == typeof(int)))
            {
                exist = true;

                for (int i = 0; i < keyCount && exist; i++)
                {
                    object keyValue = keyValues.ElementAt(i);
                    Type keyType = keyTypes.ElementAt(i);

                    if (keyType == typeof(int))
                    {
                        if ((int)keyValue <= 0)
                        {
                            exist = false;
                        }
                    }
                }

                if (exist)
                {
                    entity = entitySet.Find(keyValues.ToArray());
                }
            }
            else
            {
                entity = entitySet.Find(keyValues.ToArray());
            }

            return entity;
        }

        protected virtual void CustomFilterProcess(FilterQuery filter, ref IQueryable<TEntity> queryable)
        {
        }

        protected virtual IQueryable<TEntity> GenerateView()
        {
            throw new NotImplementedException($"Implement GenerateView method in {GetType().Name} class");
        }

        protected void ThrowIfView()
        {
            if (IsView)
                throw new NotSupportedException("View data cannot modified and find by primary key");
        }

        #endregion
    }
}
