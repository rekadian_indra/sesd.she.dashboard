﻿using Business.Abstract;
using Business.Entities;
using Business.Extension;
using Business.Infrastructure;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class EFUserRepository : EFBaseRepository<User, UmEntities>, IUserRepository
    {
        protected override void CustomFilterProcess(FilterQuery filter, ref IQueryable<User> queryable)
        {
            //lib
            FilterQuery removedFilter;
            DateTime dateTime;

            //algorithm
            if (filter != null && filter.Filters.Any(m => m.Field == Field.CreateDate || m.Field == Field.LastLoginDate))
            {
                //filter CreateDate
                removedFilter = filter.RemoveFilter(Field.CreateDate);

                if (removedFilter.HasValue())
                {
                    dateTime = removedFilter.Value.ToDateTime();
                    queryable = queryable.AsEnumerable()
                        .Where(m => m.Membership.CreateDate.ToLocalDateTime().Date == dateTime.Date)
                        .AsQueryable();
                }

                //filter LastLoginDate
                removedFilter = filter.RemoveFilter(Field.LastLoginDate);

                if (removedFilter.HasValue())
                {
                    dateTime = removedFilter.Value.ToDateTime();
                    queryable = queryable.AsEnumerable()
                        .Where(m => m.Membership.LastLoginDate.ToLocalDateTime().Date == dateTime.Date)
                        .AsQueryable();
                }
            }
        }

        public void RemoveProfile(User dbItem)
        {
            if (dbItem.Profile != null)
            {
                Context.Profiles.Remove(dbItem.Profile);
                Context.SaveChanges();
            }
        }

        //public void AssignRole(User user, List<Role> roles)
        //{
        //    if (user == null)
        //        throw new ArgumentNullException(typeof(User).Name);

        //    if (roles == null)
        //        throw new ArgumentNullException(typeof(Role).Name);

        //    if (roles.Any())
        //    {
        //        foreach (Role m in roles)
        //        {
        //            Role role = Context.Roles.Find(m.RoleId);

        //            if (role != null)
        //            {
        //                user.Roles.Add(role);
        //            }
        //        }

        //        Context.SaveChanges();
        //    }
        //}

        //public void RevokeRole(User user, List<Role> roles)
        //{
        //    if (user == null)
        //        throw new ArgumentNullException(typeof(User).Name);

        //    if (roles == null)
        //        throw new ArgumentNullException(typeof(Role).Name);

        //    if (roles.Any())
        //    {
        //        foreach (Role m in roles)
        //        {
        //            Role role = Context.Roles.Find(m.RoleId);

        //            if (role != null)
        //            {
        //                user.Roles.Remove(role);
        //            }
        //        }

        //        Context.SaveChanges();
        //    }
        //}

        public Task RemoveProfileAsync(User dbItem)
        {
            RemoveProfile(dbItem);

            return Task.FromResult(0);
        }

        //public Task AssignRoleAsync(User user, List<Role> roles)
        //{
        //    AssignRole(user, roles);

        //    return Task.FromResult(0);
        //}

        //public Task RevokeRoleAsync(User user, List<Role> roles)
        //{
        //    RevokeRole(user, roles);

        //    return Task.FromResult(0);
        //}
    }
}
