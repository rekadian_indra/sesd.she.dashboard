﻿using Business.Entities;
using Business.Abstract;

namespace Business.Concrete
{
    public class EFRegionRepository : EFBaseRepository<MstRegion, AppEntities>, IRegionRepository
    {
    }
}
