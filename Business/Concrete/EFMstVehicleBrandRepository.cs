﻿using Business.Abstract;
using Business.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class EFMstVehicleBrandRepository : EFBaseRepository<MstVehicleBrand>, IMstVehicleBrandRepository
    {
        public EFMstVehicleBrandRepository() : base(true)
        {

        }
    }
}
