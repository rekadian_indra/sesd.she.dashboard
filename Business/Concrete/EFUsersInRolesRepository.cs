﻿using Business.Abstract;
using Business.Entities;
using Business.Extension;
using Business.Infrastructure;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class EFUsersInRolesRepository : EFBaseRepository<UsersInRole, UmEntities>, IUsersInRolesRepository
    {
        public EFUsersInRolesRepository() : base()
        {
        }
    }
}
