﻿using Business.Abstract;
using Business.Entities;

namespace Business.Concrete
{
    public class EFMstCompanyTypeRepository : EFBaseRepository<MstCompanyType>, IMstCompanyTypeRepository
    {
        public EFMstCompanyTypeRepository() : base(true)
        {

        }
    }
}
