﻿using Business.Abstract;
using Business.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class EFMstVehicleOwnershipRepository : EFBaseRepository<MstVehicleOwnership>, IMstVehicleOwnershipRepository
    {
        public EFMstVehicleOwnershipRepository() : base(true)
        {

        }
    }
}
