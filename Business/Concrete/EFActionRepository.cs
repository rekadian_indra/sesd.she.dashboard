﻿using Business.Abstract;
using Business.Entities;
using System.Collections.Generic;
using System.Linq;
using Business.Linq;
using System;

namespace Business.Concrete
{
    public class EFActionRepository : EFBaseRepository<Entities.Action, UmEntities>, IActionRepository
    {
        public void Delete(string actionName, bool fk)
        {
            Entities.Action a = EntitySet.Where(x => x.ActionName == actionName).FirstOrDefault();

            if (!fk)
            {
                a.Modules.Clear();
                a.ModulesInRoles.Clear();
            }

            EntitySet.Remove(a);
            Context.SaveChanges();
        }

        public List<Module> GetModulesInAction(string actionName)
        {
            List<Module> result = EntitySet.Where(x => x.ActionName == actionName).First().Modules.ToList();
            for (int i = 0; i < result.Count; ++i)
            {
                result[i].Actions.Clear();
                result[i].ModulesInRoles.Clear();
            }
            return result;
        }
    }
}
