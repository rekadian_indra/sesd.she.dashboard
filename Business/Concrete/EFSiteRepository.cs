﻿using Business.Entities;
using Business.Abstract;

namespace Business.Concrete
{
    public class EFSiteRepository : EFBaseRepository<MstSite, AppEntities>, ISiteRepository
    {
    }
}
