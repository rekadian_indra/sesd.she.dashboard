﻿using Business.Entities;
using Business.Abstract;

namespace Business.Concrete
{
    public class EFOrganizationRepository : EFBaseRepository<MstOrganization, AppEntities>, IOrganizationRepository
    {
    }
}
