﻿using Business.Abstract;
using Business.Entities;
using System;
using System.Linq;
using Business.Linq;
using System.Collections.Generic;

namespace Business.Concrete
{
    public class EFModulesInRoleRepository : EFBaseRepository<ModulesInRole, UmEntities>, IModulesInRoleRepository
    {
        public ModulesInRole FindByRoleAndModule(Guid roleId, Guid moduleId) {
            return EntitySet.Where(x => x.RoleId == roleId).Where(x=>x.ModuleId == moduleId).FirstOrDefault();            
        }

        public void RemoveAction(Guid moduleId, Guid actionId) {
            var modulesInRole = EntitySet.Where(x => x.ModuleId == moduleId).ToList();
            foreach (ModulesInRole mr in modulesInRole) {
                mr.Actions.Remove(Context.Actions.Find(actionId));
            }
            Context.SaveChanges();
        }

        

        /*
         * Delete all ModulesInRole if doesn't have actions
         * 
         * @param moduleId
         */
        public void DeleteByModule(Guid moduleId) {
            var modulesInRole = EntitySet.Where(x => x.ModuleId == moduleId).ToList();
            foreach (ModulesInRole mr in modulesInRole)
            {
                if (mr.Actions.Count <= 0) {
                    Context.ModulesInRoles.Remove(mr);
                }
                
            }
            Context.SaveChanges();
        }

        /*
         * Delete all ModulesInRole by Role
         * 
         * 
         * @param moduleId
         */
        public void DeleteByRole(Guid roleId)
        {
            var modulesInRole = EntitySet.Where(x => x.RoleId == roleId).ToList();
            foreach (ModulesInRole mr in modulesInRole)
            {
                if (mr.Actions.Count > 0)
                {
                    mr.Actions.Clear();
                }
                Context.ModulesInRoles.Remove(mr);

            }
            Context.SaveChanges();
        }

        public void AssignChoosenActions(ModulesInRole modulesInRole, List<Guid> actionIds)
        {
            foreach(Guid g in actionIds)
            {
                modulesInRole.Actions.Add(Context.Actions.Find(g));
            }

            Context.SaveChanges();
        }

        public void RevokeChoosenActions(ModulesInRole modulesInRole, List<Guid> actionIds)
        {
            foreach (Guid g in actionIds)
            {
                modulesInRole.Actions.Remove(Context.Actions.Find(g));
            }

            Context.SaveChanges();
        }


    }
}
