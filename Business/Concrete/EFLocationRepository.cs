﻿using Business.Entities;
using Business.Abstract;

namespace Business.Concrete
{
    public class EFLocationRepository : EFBaseRepository<MstLocation>, ILocationRepository
    {
        public EFLocationRepository() : base(true)
        {
        }
    }
}
